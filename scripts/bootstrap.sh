#!/usr/bin/env bash
#
# Basic configuration, make sure box is up to date and add some generally useful packages like build-essential
# and python

# make sure initially everything up to date
apt -y update
apt -y dist-upgrade

# set time zone and hostname
timedatectl set-timezone America/Chicago
hostnamectl set-hostname cosc2336box

# add some generally useful packages, like build tools, python and wget
apt -y install build-essential csh wget htop sshfs python dos2unix git

# set passwords for the default users
#passwd -d vagrant # this will remove the password
echo -e "ubuntu\nubuntu" | passwd ubuntu
echo -e "vagrant\nvagrant" | password vagrant


# there is a misconfigure with multipathd, get messages like this:
# Aug 11 18:54:08 gaussbox multipathd[575]: sdb: add missing path
# Aug 11 18:54:08 gaussbox multipathd[575]: sdb: failed to get udev uid: Invalid argument
# We can blacklist the VBOX HARDDISK devices to at least keep our syslog messages from filling up
cat >> /etc/multipath.conf <<EOF

# blacklist VBOX HARDDISK devices to avoid log filling with multipathd failures every few seconds 
blacklist {
    device {
        vendor "VBOX"
        product "HARDDISK"
    }
}
EOF

systemctl restart multipath-tools

# make a symbolic link in vagrant user home directory to the class repository
# not needed anymore, we directly mount repo to this location instead in Vagrantfile
#mkdir -p /home/vagrant/repos
#chown vagrant:vagrant /home/vagrant/repos
#su -c "ln -s /vagrant  /home/vagrant/repos/cosc2336-ds-alg" vagrant

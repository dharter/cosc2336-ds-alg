#!/usr/bin/env bash
# install standard ubuntu desktop
apt install -y ubuntu-desktop

# We are seeing a failure on first attempt to install occassionally on windows machines for some reason.
# But if we rerun it seems to get past it.  This is kludgy, but won't hurt if it succeeds the first time
# as nothing will be done for a second attempt to install same packages again, but on failure, it will
# usually fix the issue
apt install -y ubuntu-desktop

# we seem to need to install these by had, especially the dkms kernel video modules, to get the
# gui to come up smoothly.  I think this is only needed somtimes, when guest additions have issues
# being updated?  But again I don't think it hurts anything when they are not actually needed
# because guest additions were up to date correctly.
apt install -y --no-install-recommends virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11 xserver-xorg-video-vesa xserver-xorg-video-vmware

# ensure we will boot to the graphical desktop target
systemctl set-default graphical.target
#systemctl isolate graphical.target

# add gui programs/packages we need
apt install -y evince firefox ubuntu-software

# Set up gdm to perform auto login for the vagrant user
# If installing ubuntu-desktop fails, the custom.conf file will not be there.  In order to allow
# the install of gaussian and other packages to proceed we skip this if the file is not present
if [ -f /etc/gdm3/custom.conf ];
then
    sed -i "s|#  AutomaticLoginEnable = true|AutomaticLoginEnable = true|g" /etc/gdm3/custom.conf
    sed -i "s|#  AutomaticLogin = user1|AutomaticLogin = vagrant|g" /etc/gdm3/custom.conf
fi


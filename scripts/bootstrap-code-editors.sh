#!/usr/bin/env bash
# Install a set of useful code editors / IDE tools for use on Ubuntu desktop

# make sure basic vim is installed
apt install -y vim

# Make sure emacs is installed, and set up for c++ and Python programming environments.  Also
# add in LaTeX and Markdown emacs tools.  We have a basic .emacs file to start out with a somewhat
# useful beginning configuration
apt install -y emacs 
cp /home/vagrant/repos/cosc2336-ds-alg/scripts/emacs-init.el  /home/vagrant/.emacs
chown vagrant:vagrant /home/vagrant/.emacs

# install atom editor for Linux/Ubuntu, this url will always download the latest .deb file to install
#cd /vagrant 
#wget -c https://atom.io/download/deb -O atom-editor.deb
#apt install -y gconf-service gconf-service-backend gconf2-common libgconf-2-4 libpython2-stdlib libpython2.7-minimal libpython2.7-stdlib python-is-python2 python2 python2-minimal python2.7 python2.7-minimal
#dpkg -i atom-editor.deb
echo "Installing atom editor, this may take some time..."
snap install atom --classic

# install atom packages.  Would normally use sudo -u vagrant to run as vagrant user, but that seems
# to not be working for the apm command
su - vagrant -c "apm install intentions linter linter-gcc linter-ui-default atom-beautify busy-signal build build-make output-panel dbg dbg-gdb"

cat <<EOF > /home/vagrant/.atom/config.cson
"*":
  "atom-beautify":
    cpp:
      beautify_on_save: true
      configPath: "/home/vagrant/repos/cosc2336-ds-alg/config/uncrustify.cfg"
  build:
    saveOnBuild: true
  "build-make":
    useMake: true
  core:
    telemetryConsent: "no"
  editor:
    showIndentGuide: true
    softWrapAtPreferredLineLength: true
    tabType: "soft"
  "exception-reporting":
    userId: "ed566a28-a209-40a9-b7c7-a0027afcb0b3"
  "linter-gcc":
    gccIncludePaths: "/home/vagrant/repos/cosc2336-ds-alg/include"
  welcome:
    showOnStartup: false
EOF
chown vagrant:vagrant /home/vagrant/.atom/config.cson

# fix bug in atom install where starting up with some weird files by default
sed -i "s|/snap/bin/atom ATOM_DISABLE_SHELLING_OUT_FOR_ENVIRONMENT=false /usr/bin/atom %F|/snap/bin/atom %F|g" /var/lib/snapd/desktop/applications/atom_atom.desktop


# install sublime editor for Linux/Ubuntu,
echo "Installing sublime editor, this may take some time..."
snap install sublime-text --classic

# install visual code editor/ide for Linux/Ubuntu,
echo "Installing VS Code editor, this may take some time..."
snap install code --classic

# install vscode extensions and configure the workspace, set some custom keybindings
su - vagrant -c "code --install-extension laurenttreguier.uncrustify"
su - vagrant -c "code --install-extension ms-vscode.cpptools"
mkdir -p /home/vagrant/.config/Code/User
cp /home/vagrant/repos/cosc2336-ds-alg/.vscode/keybindings.json /home/vagrant/.config/Code/User/keybindings.json
chown vagrant:vagrant /home/vagrant/.config/Code/User/keybindings.json

# install uncrustify code style checker tools and doxygen, and other tools needed to make project documentation
apt install -y uncrustify doxygen graphviz texlive-base texlive-latex-extra texlive-latex-recommended



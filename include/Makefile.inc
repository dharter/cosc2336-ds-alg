# compiler flags, tools and include variables
GCC_FLAGS=-Wall -Werror -pedantic -O2 -g
GCC=g++
INC=-I../../include

BEAUTIFIER=uncrustify
BEAUTIFIER_FLAGS=-c ../../config/uncrustify.cfg --replace --no-backup

DOC=doxygen
DOC_FLAGS=../../config/Doxyfile

RM=rm -rf

# conditional targets, support make builds on windows or unit/linux/MacOS
ifeq ($(OS), Windows_NT)
	TEST_TARGET=test.exe
	DEBUG_TARGET=debug.exe
else
	TEST_TARGET=test
	DEBUG_TARGET=debug
endif


# objects needed to be linked together for unit test executable
test-objects = $(PROJECT_NAME)-tests.o \
              $(assg-objects)

# objects needed to be linked together for main/debug executable
debug-objects = $(PROJECT_NAME)-main.o \
                $(assg-objects)


## List of all valid targets in this project:
## ------------------------------------------
## all          : by default generate all executables
##                (test and debug)
##
.PHONY : all
all : $(TEST_TARGET) $(DEBUG_TARGET)


## test         : Build and link together unit test executable
##
$(TEST_TARGET) : $(test-objects) $(template-files)
	$(GCC) $(GCC_FLAGS) $(test-objects) -o $@


## debug        : Build and link together debug executable
##
$(DEBUG_TARGET) : $(debug-objects) $(template-files)
	$(GCC) $(GCC_FLAGS) $(debug-objects) -o $@

%.o: %.cpp
	$(GCC) $(GCC_FLAGS) $(INC) -c $< -o $@


## run          : Run the unit tests showing successful tests
##
run : $(TEST_TARGET)
	./$(TEST_TARGET) --use-colour yes


## beautify     : Run the code beautifier by hand if needed
##
.PHONY : beautify
beautify :
	$(BEAUTIFIER) $(BEAUTIFIER_FLAGS) $(sources)


## docs         : Create doygen reference documentation from
##                doc comments.
##
.PHONY : docs
docs :
	@echo "Generating doxygen documentation..."
	@$(DOC) $(DOC_FLAGS) 2>&1 | grep -A 1 warning | egrep -v "assg.*\.md" | grep -v "Searching for include" | sed -e "s|${PWD}/||g"


## submit       : Create a tared and compressed file of
##                assignment files for submission
##
.PHONY : submit
submit :: beautify
	tar cvfz $(PROJECT_NAME).tar.gz $(sources)

## clean        : Remove auto-generated files for a completely
##                clean rebuild
##
.PHONY : clean
clean  :
	$(RM) $(TEST_TARGET) $(DEBUG_TARGET) *.o *.gch
	$(RM) output html latex


## help         : Get all build targets supported by this build.
##
.PHONY : help
help : Makefile ../../include/Makefile.inc
	@sed -n 's/^##//p' $^

---
title: 'Assignment 07: Templates and Operator Overloading'
author: 'COSC 2336: Data Structures and Algorithms'
date: 'Fall 2020'
---

# Objectives
- Practice creating a more realistic abstract data type ADT
- Using operator overloading to do output, insertion and access into a list.
- Use templates so our ADT can hold objects of any type

# Description
In this assignment you will be practicing operator overloading.  I will also,
for extra credit, give an additional task to convert your class into a class
template, so that it will work as a container for any type.

In this assignment you will be expanding on / creating a new version of the
ListType data type we have seen examples of before in class.  Your task is
to create a ListType that holds a list of integers.  You will be asked to
create several member functions, and then to create several overloaded operators
for your list of integers.  Your basic task is to user operator overloading to
support appending and prepending to a list, outputting the list as a string
and to an output stream, accessing the list (using the indexing operator[]),
and concatenating lists together (using operator+).

# Setup

For this assignment you will be given the following files:

| File Name          | Description                            |
|--------------------|----------------------------------------|
| `assg07-tests.cpp` | Unit tests for the quicksort and other |
|                    | functions you are to write.            |
| `ListType.hpp`     | Header file defining ListType class    |
|                    | and where member function prototypes   |
|                    | should be declared.                    |
| `ListType.cpp`     | Implementation file for the            |
|                    | ListType class member functions.       |

Set up a multi-file project to compile the two `.cpp` source files
together and run them as shown for the class.  The Makefile you were
given should be usable to create a build project using the Atom editor
as required in this class.  The general approach you should take for
this assignment, and all assignment is:

1. Set up your project with the given starting templates.  The files
   should compile and run, but either no tests will be run, or tests
   will run but be failing.
2. For this project, start by uncommenting the first `TEST_CASE` in
   the `assg07-tests.cpp` file.  These are the unit tests to test the
   functionality of `getSize()` member function.
3. Add the correct function prototype for the `getSize()` member
   function to the `ListType` class in the `ListType.hpp` header file.
   The prototype consists of the name of the function, its input
   parameters and their types, and the return value of the function.
4. Add a implementation of the `getSize()` member function to the
   `ListType.cpp` implementation file.  The function should
   have the same signature as the prototype you gave in the header
   file. Documentation for the function has not been given for you
   this time, so add documentation of your function first.   Don't
   forget to indicate that this function is a member of the `ListType`
   class.
5. Your code should compile and run now.  Make sure after adding the
   function prototype and stub your code compiles and runs.  However,
   your unit tests might be failing initially.
6. Incrementally implement the functionality of your
   `getSize()` function (if needed).  You should try to add no more than 2
   or 3 lines of code, and then make sure your program still compiles
   and runs.  Start by adding code to get the first failing test to
   pass.  Then once that test passes, move on to the next failing
   tests until you have all tests passing.  If you write something
   that causes a previously passing test to fail, you should stop and
   figure out why, and either fix it so that the original test still
   passes, or remove what you did and try a new approach.
7. Once you have the `getSize()` function implemented and all
   unit tests passing, you should then move on to the other functions
   in the order suggested.  Some member functions use previous ones in
   this assignment, so do them in the order given for you in the tasks
   below.

I have given you a starting template for your ListType that already contains
3 versions of the class constructor.  I have also already provided you
the operator= implementation, to provide the copy operator for your class.
You should first get your class to work as a simple ListType that holds a
list of integers.  If you get your class working for integers and submit it,
you can then turn your class into a template class, so that your list
can work on objects of any type.  I will give up to 10 bonus points for
implementations of working class templates, if you first mostly have your
basic ListType working for simple integers.  

# Tasks

You should set up your project/code as described in the previous
section.  In this section we give some more details on implementing
the member functions for this assignment.  You should perform the following
tasks for this assignment:

1. As mentioned in the starting template I have given you a starting
   class definition, some class constructors and the destructor, and
   the copy `operator=`.  You first need to write two simple getter
   methods in order to access the size and allocSize class member
   values.  These should be called `getSize()` and `getAllocSize()`
   respectively.  These functions should be class const functions (you
   guarantee that calling them will not cause the class to be
   modified.  These functions take no parameters as input.  They both
   return an `int` value, because the `size` and `allocSize` member
   parameters are both integer values.

2. Write a function called `tostring()`.  This function will be a
   `const` class function (the class is not modified when it is called).
   This function takes no parameters as input, and it returns a
   string.  We use this function in our testing, so you need to get it
   correct, but you have implemented versions of this type of function
   in previous assignments.  The function should only create a string
   of the items currently in the list.  So it will return a string
   like "[3, 5, 4, 2]" if those are the 4 items currently in the list.
   See the test code for specifics.

3. Overload the `operator<<()` to provide the ability for the `ListType`
   class to be output to a stream.  This will be a friend function,
   and again it will be pretty similar to several examples we have
   seen of overloading the output stream operator.  You should use the
   `tostring()` method in this function, but it outputs additional
   information, such as the `id`, `size` and `allocSize` of the list to the
   output stream.

4. Create a function named `appendItem()`.  This function takes an int
   value as its only parameter, and it should return a reference
   to itself as its result (e.g. a `ListType&` and return `*this`).  The
   indicated integer value should be appended to the end of your list
   when this function is called.  You need to correctly handle causing
   the size of your memory allocation to grow if needed in this
   function, if your list is currently using all of the allocated
   memory.  Once this is working, overload the `operator&()` operator.
   We will define the & operator to mean list appending.  For example,
   if you do `list & 5` it will cause 5 to be appended to the end of
   the list (assuming list is a variable of `ListType`).  This function
   will simply call `appendItem()` to do the work, the only difficulty
   is getting the syntax correct to declare you are overloading the
   operator.  This is not a friend function, like `operator<<()`.
   Read our textbook about binary operators to see examples of how to
   overload a binary operator like this as a member function of a
   class.  The overloaded `operator&()` should be of the same
   signature as `appendItem()`, e.g. it will take an `int` as its
   parameter, and it will return a `ListType&` (a reference to a
   `ListType`) as its result.

5. Create a function name `prependItem()`.  This works the same as the
   append, but it prepends the indicated integer parameter to the
   front of the list instead of to the end.  However, you still need
   to check and grow your allocated memory before prepending if your
   list is currently full.  Also prepend is a bit more complicated,
   because since we are implementing an array based list, you need to
   shift all of the current items 1 index up in your items before you
   can prepend to the beginning of the list. We will also overload the
   `operator|()` for our class to represent prepending an item.  Thus
   if you do `list | 5` this will cause 5 to be prepended to the
   beginning of the list.

6. Overload the `operator+()` to implement concatenation of two lists.
   This operator is probably the trickiest I have given you to implement.
   This operator should take a `const` reference to another `ListType` as
   its parameter for input.  This is the list on the right hand side
   of the + operation.  This function should return a reference
   to a new `ListType` as its result.  It is important that both the
   input parameter and the return type be both reference parameters
   for this function.  This function should be a `const` function, as
   it does not cause the original list to change.  Instead you should
   dynamically allocate a new `ListType` in this function, fill it
   with the items from the two lists being concatenated, and then
   return it as the result from this overloaded function.  You should
   read our textbook example of overloading the `operator+()` and try
   and follow that pattern for implementing this function.

7. Overload the `operator[]` indexing operator.  This is NOT a `const`
   member function, your list can change as a result of calling this
   function.  This function takes an int value as its input parameter.
   This function should return an int& reference.  Again it is very
   important that this overloaded operator return a reference.  If
   this operator correctly returns an int&, it can actually be used
   as a setter to set/change the values in the list.  This operator
   works to index your `ListType` like an array.  You should perform
   bounds checking in this function.  If the given input index is
   not valid (it is bigger than the end of your list, or it is
   < 0), you should display an error message and simply exit.

If you get all of these 7 steps and member functions working,
you should save/submit your work at that point.  However, I am also
offering the opportunity to earn 10 bonus points on this assignment,
which may be helpful for many of you to make up for some previous
program grades.  As demonstrated in our video for this week, it is
usually better if you want to create a class template to start from a
non-template working version of the class.  As I showed in the video
this week, I usually templatize each member function 1 at a time,
starting with the class definition and the class constructors.  I
will give up to 10 bonus points for a partial or complete
templatized ListType that supports appending, prepending, indexing,
and output to streams using the overloaded operators, but for any
type, not just the int type.

# Example Output
Here is the correct output you should get from your program
if you correctly implement all the class functions and successfully
pass all of the unit tests given for this assignment.  If you
invoke your function with no command line arguments, only failing
tests are usually shown by default.  In the second example, we use the
-s command line option to have the unit test framework show both
successful and failing tests, and thus we get reports of all of the
successfully passing tests as well on the output.


```
$ ./test
===============================================================================
All tests passed (115 assertions in 10 test cases)





$ ./test -s

-------------------------------------------------------------------------------
test is a Catch v2.7.2 host application.
Run with -? for options

-------------------------------------------------------------------------------
<getSize()> test getSize() getter member function
-------------------------------------------------------------------------------
assg07-tests.cpp:40
...............................................................................

assg07-tests.cpp:44: PASSED:
  CHECK( l1.getSize() == 0 )
with expansion:
  0 == 0

... output snipped ...

===============================================================================
All tests passed (115 assertions in 10 test cases)
```

# Assignment Submission

A MyLeoOnline submission folder has been created for this assignment.
There is a target named `submit` that will create a tared and gziped
file named `assg07.tar.gz`. You should do a `make submit` when finished
and upload your resulting gzip file to the MyLeoOnline Submission
folder for this assignment.

```
$ make submit
tar cvfz assg07.tar.gz assg07-tests.cpp assg07-main.cpp
  ListType.hpp ListType.cpp
assg07-tests.cpp
assg07-main.cpp
ListType.hpp
ListType.cpp
```

# Requirements and Grading Rubrics

## Program Execution, Output and Functional Requirements

1. Your program must compile, run and produce some sort of output to
   be graded.  0 if not satisfied.
1. (5 pts.) The getter methods `getSize()` and `getAllocSize()` are
   implemented and working.
1. (10 pts.) `tostring()` works and only creates a string with the items
   of the list and returns it.  `opeator<<()` works, displays the
   additional information on the output stream, and uses `tostring()`
   in its implementation.
1. (15 pts.) Got list appending working correctly.  The `appendItem()`
   member function is implemented correctly, and the `operator&()`
   is overloaded as a member function, and it uses `appendItem()` to
   do the actual work of appending.  Memory is grown if needed by this
   function.
1. (15 pts.) Go list prepending working correctly.  The `prependItem()`
   member function is implemented correctly, and the `operator|()`
   is overloaded as a member function, and it uses `prependItem()` to
   do the actual work of prepending.  Items are shifted up which is
   necessary in the array implemented for prepending.  Memory is
   correctly grown if needed by this function.
1. (25 pts) `operator+()` is correctly overloaded.  The operator
   correctly supports concatenation of two lists.  The operator is
   defined as a class const method.  The operator correctly dynamically
   allocates a new list and puts the items of the two lists into it, and returns
   this newly allocated object as its result.  A reference to the other list
   is given as input, and this function returns a reference to a list as the
   result.
1. (20 pts) `operator[]` is correctly overloaded.  The operator returns an
   int reference as its result.  The operator correctly checks for bounds
   access errors, for indexes to big or less than 0.  The operator correctly
   works as a setter method, so that values can be modified/assigned in the
   list.
1. (5 pts.) All output is correct and matches the correct example output.
1. (5 pts.) Followed class style guidelines, especially those mentioned
   below.
1. (10 bonus pts.) You may templatize your class and submit it (complete or
   partial) for up to 10 bonus points.  Your templatized class must support
   all of the overloaded operations (append, prepend, indexing, output stream),
   and work with any class, like string, double, etc.

## Program Style

Your programs must conform to the style and formatting guidelines
given for this class.  The following is a list of the guidelines that
are required for the assignment to be submitted this week.

1. Most importantly, make sure you figure out how to set your
   indentation settings correctly.  All programs must use 2 spaces for
   all indentation levels, and all indentation levels must be
   correctly indented.  Also all tabs must be removed from files, and
   only 2 spaces used for indentation.
1. A function header must be present for member functions you define.
   You must give a short description of the function, and document
   all of the input parameters to the function, as well as the return
   value and data type of the function if it returns a value for the
   member functions, just like for regular functions.  However, setter
   and getter methods do not require function headers.
1. You should have a document header for your class.  The class header
   document should give a description of the class.  Also you should
   document all private member variables that the class manages in the
   class document header.
1. Do not include any statements (such as `system("pause")` or
   inputting a key from the user to continue) that are meant to keep
   the terminal from going away.  Do not include any code that is
   specific to a single operating system, such as the
   `system("pause")` which is Microsoft Windows specific.

/** @file ListType.cpp
 * @brief Implementation file for Assignment 07 overloading and templates.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 07
 * @date   June 1, 2020
 *
 * Assignment 07 overloading and templates. Implementation of a ListType class
 * container to maintain a list of integers.  Operators are overloaded to
 * support appending, prepending, getting items, etc. This implementation file
 * contains the actual implementation of the functions you write to implement
 * the required member functions for the ListType class.
 */
#include "ListType.hpp"
#include <sstream>
#include <string>


// static member variables have to be initialized like this
int ListType::nextListId = 1;


/** grow list
 * Test to see if adding 1 more item requires list to grow.
 * If so, we grow the list.  This is a private member function,
 * users of the ListType should not and cannot directly call this
 * member function.
 */
void ListType::growListIfNeeded()
{
  // if we have enough room, no need to grow yet
  if (size < allocSize)
  {
    return;
  }

  // otherwise, grow the allocated memory so can accomodate more
  int newAllocSize = allocSize + ALLOCATION_INCREMENT;
  // uncomment following if want to log/debug allocation growth
  //cout << "<ListType::growListIfNeeded()> LOG: grow list current alloc "
  //     << allocSize << " new alloc " << newAllocSize
  //     << endl;

  // first allocate space for new items
  int* newItem = new int[newAllocSize];

  // now copy current item into new item values
  for (int index = 0; index < size; index++)
  {
    newItem[index] = item[index];
  }

  // we are done using the old allocation, free it
  // IMPORTANT: don't leak memory!
  if (item != NULL)
  {
    delete [] item;
  }

  // now start using the new items
  item = newItem;
  allocSize = newAllocSize;

}


/** default constructor
 * Initialize as an empty list.  Initially we have no memory
 * allocated, and the size (and allocation size) are 0.
 */
ListType::ListType()
{
  id = nextListId++;
  size = allocSize = 0;
  item = NULL;
}


/** constructor (empty)
 * Initialize as an empty list with indicated iniaial
 * size of memory allocated.
 *
 * @param allocSize The initialze size for the empty list.
 */
ListType::ListType(int allocSize)
{
  id = nextListId++;
  size = 0;
  this->allocSize = allocSize;
  item = new int[this->allocSize];
}


/** constructor (array
 * Initialize a list using an array of items for the initial values
 * in the list.
 *
 * @param size The number of items in the array given for initialization.
 * @param initItem An array (pointer to base address) of items to initialize
 *   this list with.
 */
ListType::ListType(int size, int* initItem)
{
  id = nextListId++;
  this->size = size;
  this->allocSize = size;
  item = new int[this->size];

  // copy the items into this list
  for (int index = 0; index < this->size; index++)
  {
    item[index] = initItem[index];
  }
}


/** destructor
 * The class destructor.  Be good stewards of memory and make
 * sure that we free up memory allocated to hold our list items
 * by this object when it goes out of scope.  We display some
 * information for debugging/tracking ListType destruction.
 */
ListType::~ListType()
{
  // uncommnt following if you want to log/debug deallocation of
  // ListType objects
  //cout << "ListType: <id=" << id << "> out of scope, size: "
  //     << size
  //     << " allocSize: " << allocSize << endl;

  // be a good memory manager, free up memory we have allocated
  if (item != NULL)
  {
    delete [] item;
  }
}


/** overload operator=
 * Overload the operator= assignment operator.  Whenever one list
 * variable is assigned to another this operator is invoked.
 *
 * @param rhs The list on the right hand side of the assignment, the
 *   contents of which is to be (deep) copied to this list contents.
 *
 * @returns ListType Returns a reference to this list, after contents
 *   have been copied/assigned.
 */
const ListType& ListType::operator=(const ListType& rhs)
{
  // only assign if not doing a self-assignment
  if (this != &rhs)
  {
    // copy the values from rightList into this list
    int newAllocSize = rhs.size;

    // if not enough space, grow our list
    if (this->allocSize < newAllocSize)
    {
      int* newItem = new int[newAllocSize];
      delete [] item;
      item = newItem;
      this->allocSize = newAllocSize;
    }

    // copy the items from right hand side into this list
    for (int index = 0; index < rhs.size; index++)
    {
      this->item[index] = rhs.item[index];
    }

    this->size = rhs.size;
  }

  // return the object assigned
  return *this;
}


// your member functions should go here
// don't forget to add in function documentation
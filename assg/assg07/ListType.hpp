/** @file ListType.hpp
 * @brief Header file for Assignment 07 overloading and templates.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 07
 * @date   June 1, 2020
 *
 * Assignment 07 overloading and templates. Implementation of a ListType class
 * container to maintain a list of integers.  Operators are overloaded to
 * support appending, prepending, getting items, etc. This header file contains
 * the declaration of the ListType class and prototypes for all of its member
 * functions.
 */
#include <iostream>
using namespace std;

#ifndef _LISTTYPE_H_
#define _LISTTYPE_H_

/** ListType abstract data type
 * This list type is "templatized" to support creating concrete lists of
 * any type of object.  This list supports dynamic shrinking and growing
 * of its size as items are appended and removed from the list.
 * In addition, several operators are overloaded for convenience of
 * inserting, accessing and outputting the list to a stream.
 */
class ListType
{
private:
  /// @brief  initial allocSize, unless overridden in construction
  const int ALLOCATION_INCREMENT = 10;
  /// @brief A static class variable used to uniquely assign ids when
  ///   new instances of ListType are created.
  static int nextListId;
  /// @brief a uniqe identifier, each list is assigned its own unique id
  ///   upon creation.
  int id;
  /// @brief The current size (an int) or number of items currently
  ///   contained in the list.
  int size;
  /// @brief The actual amount of memory we currently have allocated.
  int allocSize;
  /// @brief A (pointer to an) array of items of our int type
  ///   we are holding in our list.
  int* item;

  void growListIfNeeded();   // not really needed by users of classes, so hide it

public:
  ListType();  // default constructor
  ListType(int allocSize);  // empty constructor
  ListType(int size, int* items);  // construct from array
  ~ListType();  // class destructor

  // getters
  // add the first two function prototypes that get size and get alloc size here

  // member functions
  // functions used by overloaded operators to do the actual work

  // overloaded operators
  // put overloaded operator prorotypes here.  Don't forget that the
  // operator<< is a friend function
  const ListType& operator=(const ListType& rightList);
};

#endif // _LISTTYPE_H_
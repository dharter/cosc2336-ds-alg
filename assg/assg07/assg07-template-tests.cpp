/** @file assg07-template-tests.cpp
 * @brief Unit tests for Assignment 07 overloading and templates.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 07
 * @date   June 1, 2020
 *
 * Assignment 07 overloading and templates. Implementation of a ListType class
 * container to maintain a list of integers.  Operators are overloaded to
 * support appending, prepending, getting items, etc. This file has catch2 unit
 * tests you need to test and implement the functions for your assignment.
 */
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "ListTypeTemplate.hpp"  // will not compile until you create this
using namespace std;


//---------------------------------------------------------------------------
// Test ListType with int values first, templatized as ListType<int>

// use these various lists in the tests below
ListType<int> l1;   // empty list, size is 0 and allocation size is 0
ListType<int> l2(22);   // empty list but allocSize is 22

int size1 = 8;
int items1[] = {3, 9, 2, 7, 5, 4, 3, 8};
ListType<int> l3(size1, items1);   // list with 8 items in it initially

int size2 = 15;
int items2[] = {-3, 42, 5, -22, 18, 13, 26, 5, 0, -22, 42, 1, 7, 99, -99};
ListType<int> l4(size2, items2);   // list with 8 items in it initially


/** test getSize() member function.  We are also incidnetally testing
 * the ListType<int> constructors as well here.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the getSize() member function.
   TEST_CASE("<getSize()> test getSize() getter member function",
          "[member]")
   {
   // default constructor, size is initially 0
   CHECK( l1.getSize() == 0 );

   // alternate constructor to specify initial allocation size
   CHECK( l2.getSize() == 0 );

   // alternate constructor takes an array, size will be number of items
   // we initialize list with
   CHECK( l3.getSize() == 8 );

   // alternate constructor takes an array, size will be number of items
   // we initialize list with
   CHECK( l4.getSize() == 15 );
   }
 */


/** test getAllocSize() member function.  We are also incidnetally testing
 * the ListType<int> constructors as well here.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the getAllocSize() member function.
   TEST_CASE("<getAllocSize()> test getAllocSize() getter member function",
          "[member]")
   {
   // default constructor, allocation size is initially 0
   CHECK( l1.getAllocSize() == 0 );

   // alternate constructor to specify initial allocation size
   CHECK( l2.getAllocSize() == 22 );

   // alternate constructor takes an array, alloc size will be number of items
   // we initialize list with
   CHECK( l3.getAllocSize() == 8 );

   // alternate constructor takes an array, alloc size will be number of items
   // we initialize list with
   CHECK( l4.getAllocSize() == 15 );
   }
 */


/** test toString() member function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the toString() member function.
   TEST_CASE("<tostring()> test tostring() member function",
          "[member]")
   {
   CHECK( l1.tostring() == "[]" );
   CHECK( l2.tostring() == "[]" );
   CHECK( l3.tostring() == "[3, 9, 2, 7, 5, 4, 3, 8]" );
   CHECK( l4.tostring() == "[-3, 42, 5, -22, 18, 13, 26, 5, 0, -22, 42, 1, 7, 99, -99]" );
   }
 */


/** test operator<<() member function.  Test the overloaded operator<<
 * output stream operator for the ListType<int> class.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator<<() member function.
   TEST_CASE("<operator<<()> test operator<<() overloaded output stream operator",
          "[overload]")
   {
   ostringstream l1String;

   l1String << l1;   // using operator<< to stream into a string
   CHECK( l1String.str() ==
         "ListType <id=1>\n"
         "       size = 0\n"
         "  allocSize = 0\n"
         "      items : []\n"
         );

   ostringstream l2String;
   l2String << l2;
   CHECK( l2String.str() ==
         "ListType <id=2>\n"
         "       size = 0\n"
         "  allocSize = 22\n"
         "      items : []\n"
         );

   ostringstream l3String;
   l3String << l3;
   CHECK( l3String.str() ==
         "ListType <id=3>\n"
         "       size = 8\n"
         "  allocSize = 8\n"
         "      items : [3, 9, 2, 7, 5, 4, 3, 8]\n"
         );

   ostringstream l4String;
   l4String << l4;
   CHECK( l4String.str() ==
         "ListType <id=4>\n"
         "       size = 15\n"
         "  allocSize = 15\n"
         "      items : [-3, 42, 5, -22, 18, 13, 26, 5, 0, -22, 42, 1, 7, 99, -99]\n"
         );
   }
 */


/** test appendItem() member function.  This function does the work for
 * overloading the operator& which we define to append to the list.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the appendItem() member function.
   TEST_CASE("<appendItem()> test appendItem member function",
          "[member]")
   {
   // append to empty list
   l1.appendItem(1);
   CHECK( l1.getSize() == 1 );
   CHECK( l1.getAllocSize() == 10 );
   CHECK( l1.tostring() == "[1]" );

   // append to non empty list
   l3.appendItem(12);
   CHECK( l3.getSize() == 9 );
   CHECK( l3.getAllocSize() == 18 );
   CHECK( l3.tostring() == "[3, 9, 2, 7, 5, 4, 3, 8, 12]" );

   // try append on list that has some excess capacity
   l2.appendItem(16);
   CHECK( l2.getSize() == 1 );
   CHECK( l2.getAllocSize() == 22 );
   CHECK( l2.tostring() == "[16]" );

   // append 2 items on non empty list that has to grow first
   l4.appendItem(-1);
   l4.appendItem(-2);
   CHECK( l4.getSize() == 17 );
   CHECK( l4.getAllocSize() == 25 );
   CHECK( l4.tostring() == "[-3, 42, 5, -22, 18, 13, 26, 5, 0, -22, 42, 1, 7, 99, -99, -1, -2]" );
   }
 */


/** test operator&() overloaded append function.  This function should use
 * the appendItem() member function to do the actual work.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator&() member function.
   TEST_CASE("<operator&()> test overloaded operator& member function",
          "[overload]")
   {
   // apend an item onto the list 1 again
   l1 & 42;
   CHECK( l1.getSize() == 2 );
   CHECK( l1.getAllocSize() == 10 );
   CHECK( l1.tostring() == "[1, 42]" );

   // append 2 items in a row
   l3 & 3;
   l3 & 7;
   CHECK( l3.getSize() == 11 );
   CHECK( l3.getAllocSize() == 18 );
   CHECK( l3.tostring() == "[3, 9, 2, 7, 5, 4, 3, 8, 12, 3, 7]" );

   // do a mix of appends and overloaded operators
   l1.appendItem(4);
   l1 & 3;
   l1 & 7;
   l1.appendItem(0);
   CHECK( l1.getSize() == 6 );
   CHECK( l1.getAllocSize() == 10 );
   CHECK( l1.tostring() == "[1, 42, 4, 3, 7, 0]" );

   // test chain of append expressions
   // this tests that we are correctly defining operator& to return
   // a REFERENCE to a ListType<int> as the result as you were told to do
   // in the assignment description
   l1 & 12 & 14 & 16;
   CHECK( l1.getSize() == 9 );
   CHECK( l1.getAllocSize() == 10 );
   CHECK( l1.tostring() == "[1, 42, 4, 3, 7, 0, 12, 14, 16]" );

   // test append causes allocation to grow correctly
   l1 & 5 & 7;
   CHECK( l1.getSize() == 11 );
   CHECK( l1.getAllocSize() == 20 );
   CHECK( l1.tostring() == "[1, 42, 4, 3, 7, 0, 12, 14, 16, 5, 7]" );
   }
 */


// create a new empty list so we can test prepend to empty
ListType<int> l5;

/** test prependItem() member function.  This function does the work for
 * overloading the operator| which we define to prepend to the front
 * of the list.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the prependItem() member function.
   TEST_CASE("<prependItem()> test prependItem member function",
          "[member]")
   {
   // prepend to the new emtpy list
   l5.prependItem(8);
   CHECK( l5.getSize() == 1 );
   CHECK( l5.getAllocSize() == 10 );
   CHECK( l5.tostring() == "[8]" );

   // prepend to the now non empty list
   l5.prependItem(2);
   CHECK( l5.getSize() == 2 );
   CHECK( l5.getAllocSize() == 10 );
   CHECK( l5.tostring() == "[2, 8]" );

   // prepend to a different list
   l3.prependItem(17);
   CHECK( l3.getSize() == 12 );
   CHECK( l3.getAllocSize() == 18 );
   CHECK( l3.tostring() == "[17, 3, 9, 2, 7, 5, 4, 3, 8, 12, 3, 7]" );

   // keep prepending l3 till its allocation grows
   // add 6 items, should make the list full
   l3.prependItem(6);
   l3.prependItem(5);
   l3.prependItem(4);
   l3.prependItem(3);
   l3.prependItem(2);
   l3.prependItem(1);
   CHECK( l3.getSize() == 18 );
   CHECK( l3.getAllocSize() == 18 );
   CHECK( l3.tostring() == "[1, 2, 3, 4, 5, 6, 17, 3, 9, 2, 7, 5, 4, 3, 8, 12, 3, 7]" );

   // now 1 more item will case it to be reallocated
   l3.prependItem(0);
   CHECK( l3.getSize() == 19 );
   CHECK( l3.getAllocSize() == 28 );
   CHECK( l3.tostring() == "[0, 1, 2, 3, 4, 5, 6, 17, 3, 9, 2, 7, 5, 4, 3, 8, 12, 3, 7]" );
   }
 */


/** test operator|() overloaded prepend function.  This function should use
 * the prependItem() member function to do the actual work.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator|() member function.
   TEST_CASE("<operator|()> test overloaded operator| member function",
          "[overload]")
   {
   // prepend operator on small list
   l5 | -7;
   CHECK( l5.getSize() == 3 );
   CHECK( l5.getAllocSize() == 10 );
   CHECK( l5.tostring() == "[-7, 2, 8]" );

   // add a few more
   l5 | -5;
   l5 | -3;
   l5 | -1;
   CHECK( l5.getSize() == 6 );
   CHECK( l5.getAllocSize() == 10 );
   CHECK( l5.tostring() == "[-1, -3, -5, -7, 2, 8]" );

   // add some to l3, mix prependItem() and operator| calls
   l3.prependItem(-1);
   l3 | -5;
   l3.prependItem(9);
   l3 | 12;
   l3 | 15;
   CHECK( l3.getSize() == 24 );
   CHECK( l3.getAllocSize() == 28 );
   CHECK( l3.tostring() == "[15, 12, 9, -5, -1, 0, 1, 2, 3, 4, 5, 6, 17, 3, 9, 2, 7, 5, 4, 3, 8, 12, 3, 7]" );

   // mix prepends and appends
   l5.prependItem(7);
   l5 & 11;
   l5 | 5;
   l5.appendItem(9);
   l5 | 13;
   l5 | 0;
   l5 & 7;
   l5.appendItem(9);
   CHECK( l5.getSize() == 14 );
   CHECK( l5.getAllocSize() == 20 );
   CHECK( l5.tostring() == "[0, 13, 5, 7, -1, -3, -5, -7, 2, 8, 11, 9, 7, 9]" );


   // try chain of prepend expressions.  This tests that the operator| is
   // correctly returning a reference to a ListType<int> as specified in the
   // assignment.
   l2 | 22 | 24 | 26;;
   CHECK( l2.getSize() == 4 );
   CHECK( l2.getAllocSize() == 22 );
   CHECK( l2.tostring() == "[26, 24, 22, 16]" );

   // prepend and append items in a chain
   // unfortunately because of operator precedence doesn't quite work like
   // I would like it to.  Need to give parens to ensure we don't end up doing
   // some bitwise operations instead of our overloaded append/prepend
   ((((l2 & 5) | 7) & 9) | 11) & 13;
   CHECK( l2.getSize() == 9 );
   CHECK( l2.getAllocSize() == 22 );
   CHECK( l2.tostring() == "[11, 7, 26, 24, 22, 16, 5, 9, 13]" );
   }
 */


/** test operator+() overloaded concatenation function.  .
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator+() member function for concatenation.
   TEST_CASE("<operator+()> test overloaded operator+ concatenation member function",
          "[overload]")
   {
   // test basic concatentation
   ListType<int> l6 = l2 + l3;
   CHECK( l6.getSize() == 33 );
   CHECK( l6.getAllocSize() == 33 );
   CHECK( l6.tostring() == "[11, 7, 26, 24, 22, 16, 5, 9, 13, 15, 12, 9, -5, -1, 0, 1, 2, 3, 4, 5, 6, 17, 3, 9, 2, 7, 5, 4, 3, 8, 12, 3, 7]" );

   // test chained concatentation
   ListType<int> l7 = l1 + l3 + l2;
   CHECK( l7.getSize() == 44 );
   CHECK( l7.getAllocSize() == 44 );
   CHECK( l7.tostring() == "[1, 42, 4, 3, 7, 0, 12, 14, 16, 5, 7, 15, 12, 9, -5, -1, 0, 1, 2, 3, 4, 5, 6, 17, 3, 9, 2, 7, 5, 4, 3, 8, 12, 3, 7, 11, 7, 26, 24, 22, 16, 5, 9, 13]" );

   // test concatentate with empty list
   ListType<int> emptyList;
   ListType<int> l8 = l4 + emptyList;
   // l4 should be still the same as last time we used
   CHECK( l4.getSize() == 17 );
   CHECK( l4.getAllocSize() == 25 );
   CHECK( l4.tostring() == "[-3, 42, 5, -22, 18, 13, 26, 5, 0, -22, 42, 1, 7, 99, -99, -1, -2]" );

   // l8 should look like a copy of l4, except we would only allocate exactly
   // what is needed for the new list that is assigned
   CHECK( l8.getSize() == 17 );
   CHECK( l8.getAllocSize() == 17 );
   CHECK( l8.tostring() == "[-3, 42, 5, -22, 18, 13, 26, 5, 0, -22, 42, 1, 7, 99, -99, -1, -2]" );

   // test concatentate with empty list first
   ListType<int> l9 = emptyList + l5;

   // l5 should still be same as last time we use id
   CHECK( l5.getSize() == 14 );
   CHECK( l5.getAllocSize() == 20 );
   CHECK( l5.tostring() == "[0, 13, 5, 7, -1, -3, -5, -7, 2, 8, 11, 9, 7, 9]" );

   // l9 should look like copy of l5 except allocation will be exactly the
   // size needed
   CHECK( l9.getSize() == 14 );
   CHECK( l9.getAllocSize() == 14 );
   CHECK( l9.tostring() == "[0, 13, 5, 7, -1, -3, -5, -7, 2, 8, 11, 9, 7, 9]" );
   }
 */


/** test operator[]() overloaded indexing/setter function.  .
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator[]() member function for concatenation.
   TEST_CASE("<operator[]()> test overloaded operator[] indexing/setter member function",
          "[overload]")
   {
   // basic tests of indexing
   CHECK( l1[0] ==  1 );
   CHECK( l1[2] ==  4 );
   CHECK( l1[4] ==  7 );

   // TODO: should add an exception to check for bounds errors
   // CHECK_EXCEPTION(li[25], ListTypeBoundsException)
   // CHECK_EXCEPTION(li[-1], ListTypeBoundsException)

   // test iterating over list using indexing
   int size = 5;
   int items[] = {3, 8, 2, 9, 7};
   ListType<int> l10(size, items);

   for (int i = 0; i < l10.getSize(); i++)
   {
    CHECK( l10[i] == items[i] );
   }

   // test if correctly return a reference to an int when indexing. If so should
   // be able to use indexing to assign into list.
   // assign to index 0
   l2[0] = 8;
   CHECK( l2[0] == 8 );
   CHECK( l2.getSize() == 9 );
   CHECK( l2.getAllocSize() == 22 );

   // assign to last valid index
   l2[8] = 42;
   CHECK( l2[8] == 42 );
   CHECK( l2.getSize() == 9 );
   CHECK( l2.getAllocSize() == 22 );

   // assign in middle
   l2[4] = -7;
   CHECK( l2[4] == -7 );
   CHECK( l2.getSize() == 9 );
   CHECK( l2.getAllocSize() == 22 );

   // check all values are as we expect them
   CHECK( l2.tostring() == "[8, 7, 26, 24, -7, 16, 5, 9, 42]" );

   // test again with the new li10 list
   l10[0] = -3;
   l10[2] = -2;
   l10[4] = -7;
   CHECK( l10.getSize() == 5 );
   CHECK( l10.getAllocSize() == 5 );
   CHECK( l10.tostring() == "[-3, 8, -2, 9, -7]" );
   }
 */




//---------------------------------------------------------------------------
// Test ListType with string values now, templatized as ListType<string>

// use these various lists in the tests below
ListType<string> s1;   // empty list, size is 0 and allocation size is 0
ListType<string> s2(22);   // empty list but allocSize is 22

int ssize1 = 8;
string sitems1[] = {"xy", "ab", "qw", "jk", "gh", "lm", "po", "fe"};
ListType<string> s3(ssize1, sitems1);   // list with 8 items in it initially

int ssize2 = 15;
string sitems2[] = {"one", "two", "three", "four", "five", "six", "seven",
                    "eight", "nine", "ten", "eleven", "twelve", "thirteen",
                    "fourteen", "fifteen"};
ListType<string> s4(ssize2, sitems2);   // list with 8 items in it initially


/** test getSize() member function.  We are also incidnetally testing
 * the ListType<int> constructors as well here.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the getSize() member function.
   TEST_CASE("<string getSize()> test getSize() getter member function",
          "[member]")
   {
   // default constructor, size is initially 0
   CHECK( s1.getSize() == 0 );

   // alternate constructor to specify initial allocation size
   CHECK( s2.getSize() == 0 );

   // alternate constructor takes an array, size will be number of items
   // we initialize list with
   CHECK( s3.getSize() == 8 );

   // alternate constructor takes an array, size will be number of items
   // we initialize list with
   CHECK( s4.getSize() == 15 );
   }
 */


/** test getAllocSize() member function.  We are also incidnetally testing
 * the ListType<int> constructors as well here.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the getAllocSize() member function.
   TEST_CASE("<string getAllocSize()> test getAllocSize() getter member function",
          "[member]")
   {
   // default constructor, allocation size is initially 0
   CHECK( s1.getAllocSize() == 0 );

   // alternate constructor to specify initial allocation size
   CHECK( s2.getAllocSize() == 22 );

   // alternate constructor takes an array, alloc size will be number of items
   // we initialize list with
   CHECK( s3.getAllocSize() == 8 );

   // alternate constructor takes an array, alloc size will be number of items
   // we initialize list with
   CHECK( s4.getAllocSize() == 15 );
   }
 */


/** test toString() member function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the toString() member function.
   TEST_CASE("<string tostring()> test tostring() member function",
          "[member]")
   {
   CHECK( s1.tostring() == "[]" );
   CHECK( s2.tostring() == "[]" );
   CHECK( s3.tostring() == "[xy, ab, qw, jk, gh, lm, po, fe]" );
   CHECK( s4.tostring() == "[one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen]" );
   }
 */


/** test operator<<() member function.  Test the overloaded operator<<
 * output stream operator for the ListType<int> class.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator<<() member function.
   TEST_CASE("<string operator<<()> test operator<<() overloaded output stream operator",
          "[overload]")
   {
   ostringstream s1String;

   s1String << s1;   // using operator<< to stream into a string
   CHECK( s1String.str() ==
         "ListType <id=1>\n"
         "       size = 0\n"
         "  allocSize = 0\n"
         "      items : []\n"
         );

   ostringstream s2String;
   s2String << s2;
   CHECK( s2String.str() ==
         "ListType <id=2>\n"
         "       size = 0\n"
         "  allocSize = 22\n"
         "      items : []\n"
         );

   ostringstream s3String;
   s3String << s3;
   CHECK( s3String.str() ==
         "ListType <id=3>\n"
         "       size = 8\n"
         "  allocSize = 8\n"
         "      items : [xy, ab, qw, jk, gh, lm, po, fe]\n"
         );

   ostringstream s4String;
   s4String << s4;
   CHECK( s4String.str() ==
         "ListType <id=4>\n"
         "       size = 15\n"
         "  allocSize = 15\n"
         "      items : [one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen]\n"
         );
   }
 */


/** test appendItem() member function.  This function does the work for
 * overloading the operator& which we define to append to the list.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the appendItem() member function.
   TEST_CASE("<string appendItem()> test appendItem member function",
          "[member]")
   {
   // append to empty list
   s1.appendItem("one");
   CHECK( s1.getSize() == 1 );
   CHECK( s1.getAllocSize() == 10 );
   CHECK( s1.tostring() == "[one]" );

   // append to non empty list
   s3.appendItem("qr");
   CHECK( s3.getSize() == 9 );
   CHECK( s3.getAllocSize() == 18 );
   CHECK( s3.tostring() == "[xy, ab, qw, jk, gh, lm, po, fe, qr]" );

   // try append on list that has some excess capacity
   s2.appendItem("sixteen");
   CHECK( s2.getSize() == 1 );
   CHECK( s2.getAllocSize() == 22 );
   CHECK( s2.tostring() == "[sixteen]" );

   // append 2 items on non empty list that has to grow first
   s4.appendItem("neg one");
   s4.appendItem("neg two");
   CHECK( s4.getSize() == 17 );
   CHECK( s4.getAllocSize() == 25 );
   CHECK( s4.tostring() == "[one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, neg one, neg two]" );
   }
 */


/** test operator&() overloaded append function.  This function should use
 * the appendItem() member function to do the actual work.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator&() member function.
   TEST_CASE("<string operator&()> test overloaded operator& member function",
          "[overload]")
   {
   // apend an item onto the list 1 again
   s1 & "forty-two";
   CHECK( s1.getSize() == 2 );
   CHECK( s1.getAllocSize() == 10 );
   CHECK( s1.tostring() == "[one, forty-two]" );

   // append 2 items in a row
   s3 & "ui";
   s3 & "TAMUC";
   CHECK( s3.getSize() == 11 );
   CHECK( s3.getAllocSize() == 18 );
   CHECK( s3.tostring() == "[xy, ab, qw, jk, gh, lm, po, fe, qr, ui, TAMUC]" );

   // do a mix of appends and overloaded operators
   s1.appendItem("four");
   s1 & "three";
   s1 & "7even";
   s1.appendItem("zero");
   CHECK( s1.getSize() == 6 );
   CHECK( s1.getAllocSize() == 10 );
   CHECK( s1.tostring() == "[one, forty-two, four, three, 7even, zero]" );

   // test chain of append expressions
   // this tests that we are correctly defining operator& to return
   // a REFERENCE to a ListType<int> as the result as you were told to do
   // in the assignment description
   s1 & "twelve" & "fourteen" & "sixteen";
   CHECK( s1.getSize() == 9 );
   CHECK( s1.getAllocSize() == 10 );
   CHECK( s1.tostring() == "[one, forty-two, four, three, 7even, zero, twelve, fourteen, sixteen]" );

   // test append causes allocation to grow correctly
   s1 & "five" & "seven";
   CHECK( s1.getSize() == 11 );
   CHECK( s1.getAllocSize() == 20 );
   CHECK( s1.tostring() == "[one, forty-two, four, three, 7even, zero, twelve, fourteen, sixteen, five, seven]" );
   }
 */


// create a new empty list so we can test prepend to empty
ListType<string> s5;

/** test prependItem() member function.  This function does the work for
 * overloading the operator| which we define to prepend to the front
 * of the list.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the prependItem() member function.
   TEST_CASE("<string prependItem()> test prependItem member function",
          "[member]")
   {
   // prepend to the new emtpy list
   s5.prependItem("eight");
   CHECK( s5.getSize() == 1 );
   CHECK( s5.getAllocSize() == 10 );
   CHECK( s5.tostring() == "[eight]" );

   // prepend to the now non empty list
   s5.prependItem("two");
   CHECK( s5.getSize() == 2 );
   CHECK( s5.getAllocSize() == 10 );
   CHECK( s5.tostring() == "[two, eight]" );

   // prepend to a different list
   s3.prependItem("UTD");
   CHECK( s3.getSize() == 12 );
   CHECK( s3.getAllocSize() == 18 );
   CHECK( s3.tostring() == "[UTD, xy, ab, qw, jk, gh, lm, po, fe, qr, ui, TAMUC]" );

   // keep prepending l3 till its allocation grows
   // add 6 items, should make the list full
   s3.prependItem("f");
   s3.prependItem("e");
   s3.prependItem("d");
   s3.prependItem("c");
   s3.prependItem("b");
   s3.prependItem("a");
   CHECK( s3.getSize() == 18 );
   CHECK( s3.getAllocSize() == 18 );
   CHECK( s3.tostring() == "[a, b, c, d, e, f, UTD, xy, ab, qw, jk, gh, lm, po, fe, qr, ui, TAMUC]" );

   // now 1 more item will case it to be reallocated
   s3.prependItem("?");
   CHECK( s3.getSize() == 19 );
   CHECK( s3.getAllocSize() == 28 );
   CHECK( s3.tostring() == "[?, a, b, c, d, e, f, UTD, xy, ab, qw, jk, gh, lm, po, fe, qr, ui, TAMUC]" );
   }
 */


/** test operator|() overloaded prepend function.  This function should use
 * the prependItem() member function to do the actual work.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator|() member function.
   TEST_CASE("<string operator|()> test overloaded operator| member function",
          "[overload]")
   {
   // prepend operator on small list
   s5 | "neg 7even";
   CHECK( s5.getSize() == 3 );
   CHECK( s5.getAllocSize() == 10 );
   CHECK( s5.tostring() == "[neg 7even, two, eight]" );

   // add a few more
   s5 | "neg five";
   s5 | "neg three";
   s5 | "neg one";
   CHECK( s5.getSize() == 6 );
   CHECK( s5.getAllocSize() == 10 );
   CHECK( s5.tostring() == "[neg one, neg three, neg five, neg 7even, two, eight]" );

   // add some to l3, mix prependItem() and operator| calls
   s3.prependItem("hg");
   s3 | "jg";
   s3.prependItem("lp");
   s3 | "ce";
   s3 | "yt";
   CHECK( s3.getSize() == 24 );
   CHECK( s3.getAllocSize() == 28 );
   CHECK( s3.tostring() == "[yt, ce, lp, jg, hg, ?, a, b, c, d, e, f, UTD, xy, ab, qw, jk, gh, lm, po, fe, qr, ui, TAMUC]" );

   // mix prepends and appends
   s5.prependItem("seven");
   s5 & "eleven";
   s5 | "five";
   s5.appendItem("nine");
   s5 | "thirteen";
   s5 | "zero";
   s5 & "seven";
   s5.appendItem("nine");
   CHECK( s5.getSize() == 14 );
   CHECK( s5.getAllocSize() == 20 );
   CHECK( s5.tostring() == "[zero, thirteen, five, seven, neg one, neg three, neg five, neg 7even, two, eight, eleven, nine, seven, nine]" );


   // try chain of prepend expressions.  This tests that the operator| is
   // correctly returning a reference to a ListType<int> as specified in the
   // assignment.
   s2 | "twenty-two" | "twenty-four" | "twenty-six";
   CHECK( s2.getSize() == 4 );
   CHECK( s2.getAllocSize() == 22 );
   CHECK( s2.tostring() == "[twenty-six, twenty-four, twenty-two, sixteen]" );

   // prepend and append items in a chain
   // unfortunately because of operator precedence doesn't quite work like
   // I would like it to.  Need to give parens to ensure we don't end up doing
   // some bitwise operations instead of our overloaded append/prepend
   ((((s2 & "five") | "seven") & "nine") | "eleven") & "thirteen";
   CHECK( s2.getSize() == 9 );
   CHECK( s2.getAllocSize() == 22 );
   CHECK( s2.tostring() == "[eleven, seven, twenty-six, twenty-four, twenty-two, sixteen, five, nine, thirteen]" );
   }
 */


/** test operator+() overloaded concatenation function.  .
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator+() member function for concatenation.
   TEST_CASE("<string operator+()> test overloaded operator+ concatenation member function",
          "[overload]")
   {
   // test basic concatentation
   ListType<string> s6 = s2 + s3;
   CHECK( s6.getSize() == 33 );
   CHECK( s6.getAllocSize() == 33 );
   CHECK( s6.tostring() == "[eleven, seven, twenty-six, twenty-four, twenty-two, sixteen, five, nine, thirteen, yt, ce, lp, jg, hg, ?, a, b, c, d, e, f, UTD, xy, ab, qw, jk, gh, lm, po, fe, qr, ui, TAMUC]" );

   // test chained concatentation
   ListType<string> s7 = s1 + s3 + s2;
   CHECK( s7.getSize() == 44 );
   CHECK( s7.getAllocSize() == 44 );
   CHECK( s7.tostring() == "[one, forty-two, four, three, 7even, zero, twelve, fourteen, sixteen, five, seven, yt, ce, lp, jg, hg, ?, a, b, c, d, e, f, UTD, xy, ab, qw, jk, gh, lm, po, fe, qr, ui, TAMUC, eleven, seven, twenty-six, twenty-four, twenty-two, sixteen, five, nine, thirteen]" );

   // test concatentate with empty list
   ListType<string> emptyList;
   ListType<string> s8 = s4 + emptyList;
   // s4 should be still the same as last time we used
   CHECK( s4.getSize() == 17 );
   CHECK( s4.getAllocSize() == 25 );
   CHECK( s4.tostring() == "[one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, neg one, neg two]" );

   // s8 should look like a copy of s4, except we would only allocate exactly
   // what is needed for the new list that is assigned
   CHECK( s8.getSize() == 17 );
   CHECK( s8.getAllocSize() == 17 );
   CHECK( s8.tostring() == "[one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, neg one, neg two]" );

   // test concatentate with empty list first
   ListType<string> s9 = emptyList + s5;

   // s5 should still be same as last time we use id
   CHECK( s5.getSize() == 14 );
   CHECK( s5.getAllocSize() == 20 );
   CHECK( s5.tostring() == "[zero, thirteen, five, seven, neg one, neg three, neg five, neg 7even, two, eight, eleven, nine, seven, nine]" );

   // s9 should look like copy of s5 except allocation will be exactly the
   // size needed
   CHECK( s9.getSize() == 14 );
   CHECK( s9.getAllocSize() == 14 );
   CHECK( s9.tostring() == "[zero, thirteen, five, seven, neg one, neg three, neg five, neg 7even, two, eight, eleven, nine, seven, nine]" );
   }
 */


/** test operator[]() overloaded indexing/setter function.  .
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the overloaded operator[]() member function for concatenation.
   TEST_CASE("<string operator[]()> test overloaded operator[] indexing/setter member function",
          "[overload]")
   {
   // basic tests of indexing
   CHECK( s1[0] ==  "one" );
   CHECK( s1[2] ==  "four" );
   CHECK( s1[4] ==  "7even" );

   // TODO: should add an exception to check for bounds errors
   // CHECK_EXCEPTION(li[25], ListTypeBoundsException)
   // CHECK_EXCEPTION(li[-1], ListTypeBoundsException)

   // test iterating over list using indexing
   int size = 5;
   string items[] = {"three", "eight", "two", "nine", "seven"};
   ListType<string> s10(size, items);

   for (int i = 0; i < s10.getSize(); i++)
   {
    CHECK( s10[i] == items[i] );
   }

   // test if correctly return a reference to an int when indexing. If so should
   // be able to use indexing to assign into list.
   // assign to index 0
   s2[0] = "eight";
   CHECK( s2[0] == "eight" );
   CHECK( s2.getSize() == 9 );
   CHECK( s2.getAllocSize() == 22 );

   // assign to last valid index
   s2[8] = "forty-two";
   CHECK( s2[8] == "forty-two" );
   CHECK( s2.getSize() == 9 );
   CHECK( s2.getAllocSize() == 22 );

   // assign in middle
   s2[4] = "neg seven";
   CHECK( s2[4] == "neg seven" );
   CHECK( s2.getSize() == 9 );
   CHECK( s2.getAllocSize() == 22 );

   // check all values are as we expect them
   CHECK( s2.tostring() == "[eight, seven, twenty-six, twenty-four, neg seven, sixteen, five, nine, forty-two]" );

   // test again with the new li10 list
   s10[0] = "neg three";
   s10[2] = "neg 2";
   s10[4] = "neg seven";
   CHECK( s10.getSize() == 5 );
   CHECK( s10.getAllocSize() == 5 );
   CHECK( s10.tostring() == "[neg three, eight, neg 2, nine, neg seven]" );
   }
 */
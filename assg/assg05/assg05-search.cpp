/** @file assg05-search.hpp
 * @brief Implementation file for Assignment 05, recursive implementations of
 *   search algorithms.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 05
 * @date   August 1, 2020
 *
 * Assignment 05, recursive implementations of search algorithms.  This is
 * the file where you should implement the functions linearSearchRecursive()
 * and binarySearchRecursive() as described in the assignment.
 */
#include <iostream>
#include <string>
#include "assg05-search.hpp"

using namespace std;
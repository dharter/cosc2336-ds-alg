/** @file assg05-tests.cpp
 * @brief Unit tests for Assignment 05, recursive implementations
 *   of search algorithms.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Fall 2020
 * @note   ide  : VS Code 1.48 / build package / GNU gcc tools
 * @note   assg : Assignment 05
 * @date   August 1, 2020
 *
 * Assignment 05, recursive implementations of search algorithms  This is
 * the file containing the unit tests for this assignment.  It uses the catch2
 * unit test framework.  Tests are given (commented out) for the member functions
 * you need to implement for the two classes for this assignment.
 */
#include <string>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "assg05-search.hpp"

using namespace std;


/// @brief The size of the list of testing strings we use.
const int SIZE = 10;
/// @brief A list of strings we can use to test our searches.
string list[SIZE] = {
  "Walter", "Jessie", "Badger", "Saul", "Pete",
  "Hank", "Gus", "Skyler", "Mike", "Tuco"
};


/** Tests of the recursive linear search function.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Linear Search> ", "[function]")
   {
   SECTION("test unsuccessful search")
   {
    CHECK(sequentialSearchRecursive(list, 0, SIZE - 1, "Jane") == NOT_FOUND);
    CHECK(sequentialSearchRecursive(list, 0, SIZE - 1, "Huell") == NOT_FOUND);
   }

   SECTION("test successful search of values in the middle")
   {
    CHECK(sequentialSearchRecursive(list, 0, SIZE - 1, "Badger") == 2);
    CHECK(sequentialSearchRecursive(list, 0, SIZE - 1, "Gus") == 6);
    CHECK(sequentialSearchRecursive(list, 0, SIZE - 1, "Mike") == 8);
   }

   SECTION("test successful search of end point values, especially when it is the last value can be a problem")
   {
    CHECK(sequentialSearchRecursive(list, 0, SIZE - 1, "Walter") == 0);
    CHECK(sequentialSearchRecursive(list, 0, SIZE - 1, "Tuco") == 9);
   }
   }
 */


// sort our list in preparation for binary search
/// @brief A sorted list of strings we can use to test our searches.
string sortedList[SIZE] = {
  "Badger", "Gus", "Hank", "Jessie", "Mike",
  "Pete", "Saul", "Skyler", "Tuco", "Walter",
};

/** Tests of the recursive binary search function.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Binary Search> ", "[function]")
   {
   SECTION("test unsuccessful search")
   {
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Jane") == NOT_FOUND);
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Huell") == NOT_FOUND);
   }

   SECTION("test successful search of values in the middle")
   {
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Jessie") == 3);
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Gus") == 1);
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Saul") == 6);
   }

   SECTION("test successful search of end point values, especially when it is the last value can be a problem")
   {
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Badger") == 0);
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Walter") == 9);
   }

   SECTION("test values right on the calculations of the mid points")
   {
    // (9 + 0) / 2 = 4.5 = 4
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Mike") == 4);

    // (3 + 0) / 2 = 1.5 = 1
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Gus") == 1);

    // (5 + 9) / 2 = 7
    CHECK(binarySearchRecursive(sortedList, 0, SIZE - 1, "Skyler") == 7);
   }
   }
 */
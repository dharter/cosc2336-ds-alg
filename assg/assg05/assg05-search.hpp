/** @file assg05-search.hpp
 * @brief Header file for Assignment 05, recursive implementations of
 *   search algorithms.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 05
 * @date   August 1, 2020
 *
 * Assignment 05, recursive implementations of search algorithms.  This is
 * the header file for the assignment 05 functions.  The function prototypes for
 * the functions you create should be defined in this file.
 */
#include <string>

using namespace std;

/// @brief NOT_FOUND is a flag that is returned from search functions to
///   indicate an unsuccessful search.  Since the return value for a search
///   is the index where the item is located, we use a negative number -1
///   as a failure flag since it is not a valid array index.
const int NOT_FOUND = -1;


// function prototypes for assignment 05
// you should put in the function prototypes for the two functions
// you implement in the .cpp file here so they can be included where needed
---
title: 'Assignment 05: Searching using Recursion'
author: 'COSC 2336: Data Structures and Algorithms'
date: 'Fall 2020'
---


# Objectives
- Some more practice and review of writing recursive functions.
- Write alternative implementations of sequential and binary search.
- Practice with arrays into functions some more.


# Description

This week we looked at iterative versions of a sequential search and
a binary search.  By iterative we mean that we used a loop for both of
these functions to search for the location of an item in a list.

We can rewrite these search functions using recursive algorithms.
While this is not common for linear search, binary search is very
often written and presented as a recursive algorithm instead of as
a loop to perform the search as we discussed and used in our textbook
and class materials this week.

First for a linear sequential search, we can define the base and general cases of the
recursion like this.  We start with a `list` of items to be searched, and the
`searchItem` we are searching for in the list.  We initially define
`begin` and `end` indexes with are the indexes into the `list` of the start and end
of the unsearched portions of the list.  So for a sequential search we can
define the base cases like this:

```
// successful base case, if item is at the beginning of the list return its index
if list[begin] == searchItem
    return begin
	
// unsuccessful search base case, if begin index has crossed the end, nothing left to search
if begin > end
    return NOT_FOUND
```

Basically just check if the item we are looking for is the first item.  But also it is possible
for someone to ask you to search for a list where `begin` is past the index of `end`.  `Begin` and
`end` define the unsearched portion of the list.  So when `begin` becomes greater than `end`,
there is nothing left in the `list` to search.

If neither of the base cases are true, we want to continue the sequential search
recursively.  We do this by calling the search again, but since we just checked 
in `begin` and the item wasn't there, we wanted to search the list from `begin +1`
to the `end`:

```
// general recursive case, search list after begin if neither base case was true
return sequentialSearchRecursive(list, begin+1, end, searchItem)
```

These three statements will define a recursive sequential search that can search
a list sequentially without using any explicit loops but instead using
tail recursion to search for the item.

Likewise, we can create a recursive version of the binary search algorithm we
looked at this week as well.  Binary search is more naturally implemented using
recursion, and you will often see recursive versions of a binary search
being used.  A binary search is a type of divide-and-conquer algorithm, which
work well as recursive algorithms.  As we will talk about when we discuss the
complexity of algorithms, because we can eliminate $\frac{1}{2}$ of the list
each time we pick an endpoint to consider, the algorithm can search even
very large lists in very short amounts of time.

The recursive definition of a binary search looks similar to the recursive
definition we just gave for the sequential search.  We have the same
parameters for the search, a `list` to be searched and the `searchItem`
we are looking for.  And we specify the `begin` and `end` indexes of the
portion of the list we have not yet searched.  We begin as we did for the
iterative solution by calculating the midpoint index, the value in the
middle of the `begin` and `end` unsearched portions of the list.  Given
mid, the base cases for the binary search are:

```
// successful base case, if item happens to be at the midpoint we found it
if list[mid] == searchItem
    return mid
	
// unsuccessful search base case, if begin index has crossed the end, nothing
// left to search
if begin > end
    return NOT_FOUND
```

So the base cases are very similar to the sequential search, just we are testing the
midpoint value.  Also recall, of course, for the binary search to work the list
has to be sorted before calling the binary search function.  We are assuming this in order for
the next general recursive case to work corectly.  We then have 2 recursive cases,
the case where the item we want is less than the mid point, so we have to search in
the lower portion of the list, or the case where it is greater so we search the
upper portion of the list:

```
// general recursive case, when searchItem is less than item at midpoint
// do a recursive search of lower portion of list
if searchItem < list[mid]
    return binarySearchRecursive(list, begin, mid-1, searchItem)
	
// other general recursive case, when searchItem is greater than item at midpoint
// do a recursive search of upper portion of list
if searchItem > list[mid]
    return binarySearchRecursive(list, mid+1, end, searchItem)
```

In this assignment you will implement recursive versions of sequential and
binary search using these described base and general recursive cases.

# Setup

For this assignment you will be given the following files:

| File Name                 | Description                         |
|---------------------------|-------------------------------------|
| `assg05-tests.cpp`        | Unit tests for the two functions    |
|                           | you are to write.                   |
| `assg05-search.hpp`       | Header file for function prototypes |
|                           | you are to add.                     |
| `assg05-search.cpp`      | Implementation file for the         |
|                           | functions you are to write for      |
|                           | this assignment.                    |

Set up a multi-file project to compile the two `.cpp` source files
together and run them as shown for the class.  The `Makefile` you were
given should be configure to build a project using the VS Code editor
as required in this class.  The general approach you should take for
this assignment, and all assignment is:

1. Set up your project with the given starting templates.  The files
   should compile and run, but either no tests will be run, or tests
   will run but be failing.
2. For this project, start by uncommenting the first `TEST_CASE` in
   the `assg05-tests.cpp` file.  These are the unit tests of the
   `sequentialSearchRecursive()` function you are to write. 
   When you uncomment this first unit test
   case, your program will no longer be able to compile, because you
   have not yet written the `sequentialSearchRecursive()` function.
3. Add the correct function prototype for your `sequentialSearchRecursive()`
   function to the `assg01-search.hpp` header file.  The
   prototype consists of the name of the function, its input
   parameters and their types, and the return value of the function.
4. Add a stub/empty implementation of `sequentialSearchRecursive()` to your own
   `assg05-search.cpp` implementation file.  The function should
   have the same signature as the prototype you gave in the header
   file.  The function should initially just return a result of `NOT_FOUND`
   since this is a value returning function that should return an integer
   index or the `NOT_FOUND` flag to indicate unsuccessful search..
5. Your code should compile and run now.  Make sure after adding the
   function prototype and stub your code compiles and runs.  However,
   some but not all of the unit tests will now be passing, and most
   will be failing.
6. Incrementally implement the functionality of your `sequentialSearchRecursive()`
   function.  You should try to add no more than 2 or 3 lines of code,
   and then make sure you program still compiles and runs. You can start by
   adding in the base cases 1 at a time, then add in the general case.
7. Once you have the `sequentialSearchRecursive()` function implemented and all
   unit tests passing, you should then move on to the
   `binarySearchRecursive()` function.  Your approach should be
   the same.  Start by uncommenting the second `TEST_CASE` and making
   sure code still compiles after adding your function prototype to
   the header file and a stub/empty function to the implementation
   file.  Then incrementally add code to this function, recompiling
   often and paying attention to which unit tests are passing and
   which are failing while implementing the function.

# Tasks
You should set up your project/code as described in the previous
section.  In this section we give some more details on implementing
the two functions you are to write for this assignment.  You should
perform the following tasks for this assignment, once you have your
project set up and building:

1. Implement the function named `sequentialSearchRecursive()`.
   You should have a stub of the function that compiles and runs
   the tests, but fails because it returns `NOT_FOUND` by default.
   This function takes 4 parameters as inputs, and you can see
   from the test cases what the expected function signature needs
   to be.
   Add in the base cases and the recursive case incrementally until
   all of the tests are passing for the first test case.
1. Implement the function named `binarySearchRecursive()`.  You should already
   have a stub of this function that compiles and returns a result of
   `NOT_FOUND` before starting to implement the function.  This function takes
   the same 4 parameters as the first function.  Add in the base cases
   and the two recursive cases this time until all of the tests are passing
   for the second test case.

# Example Output
Here is the correct output you get from your program
if you correctly implement the two functions and successfully
pass all of the unit tests given for this assignment.  If you
invoke your function with no command line arguments, only failing
tests are usually shown by default.  In the second example, we use the
`-s` command line option to have the unit test framework show both
successful and failing tests, and thus we get reports of all of the
successfully passing tests as well on the output.

```
$ make run
./test --use-colour yes
===============================================================================
All tests passed (17 assertions in 2 test cases)

$ ./test -s

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
test is a Catch v2.12.2 host application.
Run with -? for options

-------------------------------------------------------------------------------
<Linear Search> 
  test unsuccessful search
-------------------------------------------------------------------------------
assg05-tests.cpp:40
...............................................................................

assg05-tests.cpp:42: PASSED:
  CHECK( sequentialSearchRecursive(list, 0, SIZE - 1, "Jane") == NOT_FOUND )
with expansion:
  -1 == -1

assg05-tests.cpp:43: PASSED:
  CHECK( sequentialSearchRecursive(list, 0, SIZE - 1, "Huell") == NOT_FOUND )
with expansion:
  -1 == -1

-------------------------------------------------------------------------------
<Linear Search> 
  test successful search of values in the middle
-------------------------------------------------------------------------------
assg05-tests.cpp:46
...............................................................................

assg05-tests.cpp:48: PASSED:
  CHECK( sequentialSearchRecursive(list, 0, SIZE - 1, "Badger") == 2 )
with expansion:
  2 == 2

assg05-tests.cpp:49: PASSED:
  CHECK( sequentialSearchRecursive(list, 0, SIZE - 1, "Gus") == 6 )
with expansion:
  6 == 6

assg05-tests.cpp:50: PASSED:
  CHECK( sequentialSearchRecursive(list, 0, SIZE - 1, "Mike") == 8 )
with expansion:
  8 == 8

-------------------------------------------------------------------------------
<Linear Search> 
  test successful search of end point values, especially when it is the last
  value can be a problem
-------------------------------------------------------------------------------
assg05-tests.cpp:53
...............................................................................

assg05-tests.cpp:55: PASSED:
  CHECK( sequentialSearchRecursive(list, 0, SIZE - 1, "Walter") == 0 )
with expansion:
  0 == 0

assg05-tests.cpp:56: PASSED:
  CHECK( sequentialSearchRecursive(list, 0, SIZE - 1, "Tuco") == 9 )
with expansion:
  9 == 9

-------------------------------------------------------------------------------
<Binary Search> 
  test unsuccessful search
-------------------------------------------------------------------------------
assg05-tests.cpp:73
...............................................................................

assg05-tests.cpp:75: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Jane") == NOT_FOUND )
with expansion:
  -1 == -1

assg05-tests.cpp:76: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Huell") == NOT_FOUND )
with expansion:
  -1 == -1

-------------------------------------------------------------------------------
<Binary Search> 
  test successful search of values in the middle
-------------------------------------------------------------------------------
assg05-tests.cpp:79
...............................................................................

assg05-tests.cpp:81: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Jessie") == 3 )
with expansion:
  3 == 3

assg05-tests.cpp:82: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Gus") == 1 )
with expansion:
  1 == 1

assg05-tests.cpp:83: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Saul") == 6 )
with expansion:
  6 == 6

-------------------------------------------------------------------------------
<Binary Search> 
  test successful search of end point values, especially when it is the last
  value can be a problem
-------------------------------------------------------------------------------
assg05-tests.cpp:86
...............................................................................

assg05-tests.cpp:88: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Badger") == 0 )
with expansion:
  0 == 0

assg05-tests.cpp:89: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Walter") == 9 )
with expansion:
  9 == 9

-------------------------------------------------------------------------------
<Binary Search> 
  test values right on the calculations of the mid points
-------------------------------------------------------------------------------
assg05-tests.cpp:92
...............................................................................

assg05-tests.cpp:95: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Mike") == 4 )
with expansion:
  4 == 4

assg05-tests.cpp:98: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Gus") == 1 )
with expansion:
  1 == 1

assg05-tests.cpp:101: PASSED:
  CHECK( binarySearchRecursive(sortedList, 0, SIZE - 1, "Skyler") == 7 )
with expansion:
  7 == 7

===============================================================================
All tests passed (17 assertions in 2 test cases)
```



# Assignment Submission

A MyLeoOnline submission folder has been created for this assignment.
There is a target named `submit` that will create a tared and gziped
file named `assg05.tar.gz`. You should do a `make submit` when finished
and upload your resulting gzip file to the MyLeoOnline Submission
folder for this assignment.

```
$ make submit
uncrustify -c ../../config/uncrustify.cfg --replace --no-backup assg05-tests.cpp assg05-main.cpp assg05-search.hpp assg05-search.cpp
Parsing: assg05-tests.cpp as language CPP
Parsing: assg05-main.cpp as language CPP
Parsing: assg05-search.hpp as language CPP
Parsing: assg05-search.cpp as language CPP
tar cvfz assg05.tar.gz assg05-tests.cpp assg05-main.cpp assg05-search.hpp assg05-search.cpp
assg05-tests.cpp
assg05-main.cpp
assg05-search.hpp
assg05-search.cpp

```

# Requirements and Grading Rubrics

## Program Execution, Output and Functional Requirements

1. 30 pts. Your program must compile, run and produce some sort of output to
   be graded.  0 if not satisfied.
2. 30 pts. for correctly implementing the `sequentialSearchRecursive()` function.
3. 40 pts. for correctly implementing the `binarySearchRecursive()` function.


## Program Style

Your programs must conform to the style and formatting guidelines
given for this class.  The following is a list of the guidelines that
are required for the assignment to be submitted this week.

1. Most importantly, make sure you figure out how to set your
   indentation settings correctly.  All programs must use 2 spaces for
   all indentation levels, and all indentation levels must be
   correctly indented.  Also all tabs must be removed from files, and
   only 2 spaces used for indentation.
1. A function header must be present for all functions you define.
   You must give a short description of the function, and document
   all of the input parameters to the function, as well as the return
   value and data type of the function if it returns a value.
1. Do not include any statements (such as `system("pause")` or
   inputting a key from the user to continue) that are meant to keep
   the terminal from going away.  Do not include any code that is
   specific to a single operating system, such as the
   `system("pause")` which is Microsoft Windows specific.

/** @file assg12-tests.cpp
 * @brief Unit tests for Assignment 12 hashing and dictionaries
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 12
 * @date   June 1, 2020
 *
 * Assignment 12 hashing and dictionaries.  Implement member functions for a
 * HashDictionary class for an open hashing scheme with a quadratic probe
 * sequence. This file has catch2 unit tests you need to test and implement the
 * functions for your assignment.
 */
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Employee.hpp"
#include "KeyValuePair.hpp"
#include "HashDictionary.hpp"
using namespace std;



/** test Employee basic functionality.  You should not
 * modify these classes.  Just some tests to make sure nothing gets broken
 * while working on HashDictionary functions.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some () function.
 */
TEST_CASE("<basic Employee> test basic Employee functionality",
          "[class]")
{
  Employee e(12345, "Adam Smith", "123 Main Street, Commerce TX", 500.00);
  CHECK( e.getId() == 12345 );
  CHECK( e.getName() == "Adam Smith" );
  CHECK( e.tostring() == "( id: 12345, Adam Smith, 123 Main Street, Commerce TX, 500.00 )\n" );
}


/** test KeyValuePair basic functionality.  You should not
 * modify these classes.  Just some tests to make sure nothing gets broken
 * while working on HashDictionary functions.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some () function.
 */
TEST_CASE("<basic KeyValuePair> test basic KeyValuePair functionality",
          "[class]")
{
  Employee e(12345, "Adam Smith", "123 Main Street, Commerce TX", 500.00);
  KeyValuePair<int, Employee> pair(12345, e);

  CHECK( pair.key() == 12345 );
  CHECK( pair.value().tostring() == "( id: 12345, Adam Smith, 123 Main Street, Commerce TX, 500.00 )\n" );
  pair.setKey(45678);
  CHECK( pair.key() == 45678 );
  CHECK( pair.value().tostring() == "( id: 12345, Adam Smith, 123 Main Street, Commerce TX, 500.00 )\n" );
}


/** test probe() member function
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the probe() member function.
   TEST_CASE("<probe()> test probe member function",
          "[member]")
   {
   // first set up a hash dictionary, test it is empty
   // the EMPTY_EMPLOYEE_ID comes from the Employee.hpp class header, it is a
   // special Id guaranteed to not be used as an Employee id so we can use to
   // indicate empty has table entries.
   const int TABLE_SIZE = 7;
   HashDictionary<int, Employee> dict(TABLE_SIZE, EMPTY_EMPLOYEE_ID);
   CHECK( dict.size() == 0 );

   // test first 8 probe sequences of defined quadratic probe sequence
   // for c1 = 1, c2 = 2, c3 = 2
   int id = 1;
   CHECK( dict.probe(id, 0) == 2 );
   CHECK( dict.probe(id, 1) == 5 );
   CHECK( dict.probe(id, 2) == 10 );
   CHECK( dict.probe(id, 3) == 17 );
   CHECK( dict.probe(id, 4) == 26 );
   CHECK( dict.probe(id, 5) == 37 );
   CHECK( dict.probe(id, 6) == 50 );
   CHECK( dict.probe(id, 7) == 65 );

   // arbitrary large probe
   CHECK( dict.probe(id, 12345) == 152423717 );
   }
 */


/** test hash() member function
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the hash() member function.
   TEST_CASE("<hash()> test hash member function",
          "[member]")
   {
   const int TABLE_SIZE = 7;
   HashDictionary<int, Employee> dict(TABLE_SIZE, EMPTY_EMPLOYEE_ID);
   CHECK( dict.size() == 0 );

   // the following tests assume a standard 32 bit sized int
   // if your system uses 64 bit int need to comment these out and
   // uncomment the 64bit int tests instead
   CHECK( sizeof(int) == 4 ); // 4 bytes or 32 bits

   // test various int hashes.  If you square using key * key (not pow(key,2))
   // you should get these results for a 32 bit int for a table of size 7
   int id;

   // 1 hashes to slot 0
   id = 1;
   CHECK( dict.hash(id) == 0 );

   // 25 hashes to slot 2
   id = 25;
   CHECK( dict.hash(id) == 2 );

   // 3918 hashes to slot 1
   id = 3918;
   CHECK( dict.hash(id) == 1 );

   // 48517 hashes to slot 6
   id = 48517;
   CHECK( dict.hash(id) == 6 );

   // 913478 hashes to slot 5
   id = 913478;
   CHECK( dict.hash(id) == 5 );

   // 8372915 hashes to slot 4
   id = 8372915;
   CHECK( dict.hash(id) == 4 );

   // we will use the following 4 ids to test hash table insertion with
   // probing.  Note the return from the hash() function is only the home
   // or base address.  With quadratic probing even for probe index 0 we
   // need to add something to this returned base address for the probe
   id = 438901234;
   CHECK( dict.hash(id) == 6 );
   id = 192834192;
   CHECK( dict.hash(id) == 1 );
   // notice a collision will occur when adding this id
   id = 998439281;
   CHECK( dict.hash(id) == 6 );
   id = 362817371;
   CHECK( dict.hash(id) == 5 );
   // another collission at hash bucket 5
   id = 272349829;
   CHECK( dict.hash(id) == 5 );
   }
 */


/** test insert() member function
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the hash() member function.
   TEST_CASE("<insert()> test insert member function",
          "[member]")
   {
   const int TABLE_SIZE = 7;
   HashDictionary<int, Employee> dict(TABLE_SIZE, EMPTY_EMPLOYEE_ID);
   CHECK( dict.size() == 0 );

   // insert first Employee
   int id1 = 438901234;
   Employee e1(id1, "Derek Harter", "123 Main St. Commerce TX", 58.23);
   CHECK( dict.probe(id1, 0) == 2 );
   CHECK( dict.hash(id1) == 6 );
   dict.insert(id1, e1);
   // shoudl end up in slot 1 (6 + 2 % 7 == 1)
   CHECK( dict.size() == 1 );
   CHECK( dict[1].key() == id1 );
   CHECK( dict[1].value().getId() == id1 );

   // insert second Employee, no collision occurs
   int id2 = 192834192;
   Employee e2(id2, "Alice White", "384 Bois'darc Campbell TX", 45.45);
   CHECK( dict.probe(id2, 0) == 2 );
   CHECK( dict.hash(id2) == 1 );
   dict.insert(id2, e2);
   // should end up in slot 3 (1 + 2 % 7 == 3)
   CHECK( dict.size() == 2 );
   CHECK( dict[3].key() == id2 );
   CHECK( dict[3].value().getId() == id2 );

   // insert third Employee, collides with first, needs to go to probe
   // sequence index 1
   int id3 = 998439281;
   Employee e3(id3, "Bob Green", "92 Washing Apt 5 Greenville TX", 16.00);
   CHECK( dict.probe(id3, 0) == 2 );
   CHECK( dict.hash(id3) == 6 );
   dict.insert(id3, e3);
   // should colide for probe index 0 at slot 1, so end up with
   // probe index 1 (6 + 5 % 7 == 4)
   CHECK( dict.size() == 3 );
   CHECK( dict[4].key() == id3 );
   CHECK( dict[4].value().getId() == id3 );

   // insert fourth Employee, no collission occurs
   int id4 = 362817371;
   Employee e4(id4, "Carol Black", "8913 FM 24 Cooper TX", 28.50);
   CHECK( dict.probe(id4, 0) == 2 );
   CHECK( dict.hash(id4) == 5 );
   dict.insert(id4, e4);
   // should end up in index 0 (5 + 2 % 7 == 0)
   CHECK( dict.size() == 4 );
   CHECK( dict[0].key() == id4 );
   CHECK( dict[0].value().getId() == id4 );

   // insert fifth Employee, collission at hash 5 to index 0, but
   // next probe index to index 3 is also filled, so have to go to probe
   // index 2
   int id5 = 272349829;
   Employee e5(id5, "Walter White", "83 Rodeo Drive Phoenix AZ", 560.38);
   CHECK( dict.probe(id5, 0) == 2 );
   CHECK( dict.hash(id5) == 5 );
   dict.insert(id5, e5);
   // should end up in probe index 0 goes to slot 0 which is filled
   // probe index 1 goes to (5 + 5 % 7 == 3) which is filled
   // probe index 2 goes to (5 + 10 % 7 == 1) which is filled
   // probe index 3 goes to (5 + 17 % 7 == 1) which is filled
   // probe index 4 goes to (5 + 26 % 7 == 3) which is filled
   // probe index 5 goes to (5 + 37 % 7 == 0) which is filled
   // probe index 6 goes to (5 + 50 % 7 == 6) which is where we finally land
   CHECK( dict.size() == 5 );
   CHECK( dict[6].key() == id5 );
   CHECK( dict[6].value().getId() == id5 );

   CHECK( dict.tostring() ==
         "Slot: 0\n"
         "     Key  : 362817371\n"
         "     Value: ( id: 362817371, Carol Black, 8913 FM 24 Cooper TX, 28.50 )\n"
         "\n"
         "Slot: 1\n"
         "     Key  : 438901234\n"
         "     Value: ( id: 438901234, Derek Harter, 123 Main St. Commerce TX, 58.23 )\n"
         "\n"
         "Slot: 2\n"
         "     Key  : 0\n"
         "     Value: ( id: 0, , , 0.00 )\n"
         "\n"
         "Slot: 3\n"
         "     Key  : 192834192\n"
         "     Value: ( id: 192834192, Alice White, 384 Bois'darc Campbell TX, 45.45 )\n"
         "\n"
         "Slot: 4\n"
         "     Key  : 998439281\n"
         "     Value: ( id: 998439281, Bob Green, 92 Washing Apt 5 Greenville TX, 16.00 )\n"
         "\n"
         "Slot: 5\n"
         "     Key  : 0\n"
         "     Value: ( id: 0, , , 0.00 )\n"
         "\n"
         "Slot: 6\n"
         "     Key  : 272349829\n"
         "     Value: ( id: 272349829, Walter White, 83 Rodeo Drive Phoenix AZ, 560.38 )\n"
         "\n"
         "\n"
         );
   }
 */


/** test find() member function
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the find() member function.
   TEST_CASE("<find()> test insert member function",
          "[member]")
   {
   // create a dictionary to search
   const int TABLE_SIZE = 7;
   HashDictionary<int, Employee> dict(TABLE_SIZE, EMPTY_EMPLOYEE_ID);

   // insert first Employee
   int id1 = 438901234;
   Employee e1(id1, "Derek Harter", "123 Main St. Commerce TX", 58.23);
   dict.insert(id1, e1);

   // insert second Employee, no collision occurs
   int id2 = 192834192;
   Employee e2(id2, "Alice White", "384 Bois'darc Campbell TX", 45.45);
   dict.insert(id2, e2);

   // insert third Employee, collides with first, needs to go to probe
   // sequence index 1
   int id3 = 998439281;
   Employee e3(id3, "Bob Green", "92 Washing Apt 5 Greenville TX", 16.00);
   dict.insert(id3, e3);

   // insert fourth Employee, no collission occurs
   int id4 = 362817371;
   Employee e4(id4, "Carol Black", "8913 FM 24 Cooper TX", 28.50);
   dict.insert(id4, e4);

   // insert fifth Employee, collission at hash 5 to index 0, but
   // next probe index to index 3 is also filled, so have to go to probe
   // index 2
   int id5 = 272349829;
   Employee e5(id5, "Walter White", "83 Rodeo Drive Phoenix AZ", 560.38);
   dict.insert(id5, e5);


   // Test search for each of the keys
   CHECK( dict.find(id1).getId() == id1 );
   CHECK( dict.find(id2).getId() == id2 );
   CHECK( dict.find(id3).getId() == id3 );
   CHECK( dict.find(id4).getId() == id4 );
   CHECK( dict.find(id5).getId() == id5 );

   // test unsuccessful search
   int badid = 239123876;
   CHECK( dict.find(badid).getId() == EMPTY_EMPLOYEE_ID );
   }
 */
/** @file assg12-tests.cpp
 * @brief main/debug executable for Assignment 12 hashing and dictionaries
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 12
 * @date   June 1, 2020
 *
 * Assignment 12 hashing and dictionaries.  Implement member functions for a
 * HashDictionary class for an open hashing scheme with a quadratic probe
 * sequence. This file contains a main() function we can use to build a debug
 * target for debugging.
 */
#include <iostream>
#include "Employee.hpp"
#include "HashDictionary.hpp"
using namespace std;


/** main
 * The main entry point for this program.  Execution of this program
 * will begin with this main function.
 *
 * @param argc The command line argument count which is the number of
 *     command line arguments provided by user when they started
 *     the program.
 * @param argv The command line arguments, an array of character
 *     arrays.
 *
 * @returns An int value indicating program exit status.  Usually 0
 *     is returned to indicate normal exit and a non-zero value
 *     is returned to indicate an error condition.
 */
int main(int argc, char** argv)
{
  // example of invoking some of the functions to debug them

  // return 0 to indicate successful completion of program
  return 0;
}
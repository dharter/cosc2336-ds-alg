/** @file Employee.hpp
 * @brief Header file with definitions of Employee class for
 *   Assignment 12 hashing and dictionaries
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 12
 * @date   June 1, 2020
 *
 * Simple example of an Employee record/class we can use to demonstrate
 * HashDictionary key/value pair management.
 */
#include <string>
#include <iostream>
using namespace std;


#ifndef EMPLOYEE_HPP
#define EMPLOYEE_HPP


/// @brief This should really be a class constant, however this
///   global constant represents a flag that is used to
///   indicate empty slots and/or failed search.
const int EMPTY_EMPLOYEE_ID = 0;

/** Employee
 * A simple Employee class/record to demonstrate/test
 * our hashing dictionary assignment.
 * NOTE: we are using 0 as a flag to represent an unused
 * slot or an invalid/empty employee.  This is used/assumed
 * by our dictionary class to determine if a slot is empty
 * and/or to give a failure result for a failed search.
 */
class Employee
{
private:
  /// @brief A unique Employee identifier for this record
  int id;
  /// @brief A string to hold Employee name.  In a real record we would use
  ///   multiple separate fields for first, last, middle, and even more complex
  ///   if we need to be international and support naming conventions across the
  ///   world.
  string name;
  /// @brief A string to hold the street address.  Again just an example of a
  ///   field in an Employee record.  A street address would need to be much
  ///   more complex.
  string address;
  /// @brief A floating point value holding the employee salary information.
  float salary;

public:
  Employee();
  Employee(int id, string name, string address, float salary);

  int getId() const;
  string getName() const;
  string tostring() const;

  friend ostream& operator<<(ostream& out, Employee& employee);
};

#endif // EMPLOYEE_HPP
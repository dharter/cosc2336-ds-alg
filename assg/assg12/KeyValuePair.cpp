/** @file KeyValuePair.cpp
 * @brief Implementation file with implementations of KeyValuePair class
 *   member functions for Assignment 12 hashing and dictionaries
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 12
 * @date   June 1, 2020
 *
 * Template class for definining Key/Value pairs, suitable for dictionary and
 * hash table implementations. Based on Shaffer KVPair ADT definition, pg. 139
 * Fig 4.31.
 */
#include "KeyValuePair.hpp"


/** constructor
 * Default constructor for a KeyValuePair.
 */
template <class Key, class Value>
KeyValuePair<Key, Value>::KeyValuePair()
{
  // nothing to do for default
}


/** constructor
 * Standard constructor for a KeyValuePair.
 *
 * @param key The key portion that is to be stored in this pair.
 * @param value The value portion that is to be stored in this pair.
 */
template <class Key, class Value>
KeyValuePair<Key, Value>::KeyValuePair(Key key, Value value)
{
  this->myKey = key;
  this->myValue = value;
}


/** key accessor
 * Accessor method to get and return the key for this key/value pair
 *
 * @returns Key Returns an object of template type Key, which is the
 *   key portion of the pair in this container.
 */
template <class Key, class Value>
Key KeyValuePair<Key, Value>::key()
{
  return myKey;
}


/** key setter
 * Accessor method to set the key for this key/value pair
 *
 * @param key The new value to update the key to for this pair.
 */
template <class Key, class Value>
void KeyValuePair<Key, Value>::setKey(Key key)
{
  this->myKey = key;
}


/** value accessor
 * Accessor method to get and return the value for this key/value pair.
 *
 * @returns Value& Returns a reference to the value object in this
 *   key value pair container.
 */
template <class Key, class Value>
Value& KeyValuePair<Key, Value>::value()
{
  return myValue;
}
---
title: 'Assignment 12: Dictinaries and Hashing'
author: 'COSC 2336: Data Structures and Algorithms'
date: 'Fall 2020'
---

# Objectives
- More practice with using class templates
- Learn about implementing and using key/value pair Dictionary abstraction
- Implement and learn about some basic hashing techniques, like mid-square
  hashing and quadratic probing for closed hashing schemes.

# Description
In this assignment you will be implementing some basic mechanisms
of a hash table to implement a Dictionary that uses hashing to store
and search for items in its collection.  You have been given many files
for this assignment.  I have provided an `Employee` class
in `Employee.[hpp|cpp]` and a `KeyValuePair` class in
`KeyValuePair.[hpp|cpp]`.  You will not need to make any changes to
these files or classes, they should work as given for this assignment.

You will be adding and implementing some member functions to the `HashDictionary`
class.  The initial `HashDictionary.[hpp|cpp]` file contains a constructor
and destructor for a `HashDictionary` as well as some other accessors and
operators already implemented that are used for testing.

You will be implementing a closed hash table mechanism using quadratic
probing of the slots.  You will also implement a version of the
mid-square hashing function described in our textbook (Shaffer section
9.4.3 on closed hashing mechanisms).

# Setup

For this assignment you will be given the following files:

| File Name            | Description                                      |
|----------------------|--------------------------------------------------|
| `assg12-tests.cpp`   | Unit tests for the member functions and hashing  |
|                      | functions you are to write.                      |
| `HashDictionary.hpp` | Header file for the HashDictionary class that    |
|                      | that you are to add function prototypes for the  |
|                      | new member functions into.                       |
| `HashDictionary.cpp` | Implementation file, the implementation of the   |
|                      | HashDictionary class member methods for this     |
|                      | assignment go here in this file.                 |
| `KeyValuePair.hpp`   | A key/value storage used by the HashDictionary.  |
|                      | You do not need to modify this file.             |
| `KeyValuePair.cpp`   | Implementation of the key/value storage type     |
|                      | for the HashDictionary.  You do not need to      |
|                      | modify this file.                                |
| `Employee.hpp`       | A test class we will use to test the templatized |
|                      | HashDictionary.  We will be hashing Employee     |
|                      | instances on their employee id.  You do not need |
|                      | to change this file.                             |
| `Employee.cpp`       | Implementation of the Employee class member      |
|                      | functions.  You do not need to modify this file. |

Set up a multi-file project to compile the `.cpp` source files
and run them as shown for the class.  The `Makefile` you were
given should be usable to create a build project using the Atom editor
as required in this class.


The general approach you should take for this assignment, and all
assignment is:

1. Set up your project with the given starting code.  The files
   should compile and run, but either no tests will be run, or tests
   will run but be failing.
2. For this project, start by uncommenting the first `TEST_CASE` in
   the `assg12-tests.cpp` file.  These are the unit tests to test the
   functionality of your `HashDictionary` `probe()` function, the function
   defining a probe sequence for the closed hashing scheme.
3. Add the `probe()` member function prototype to the
   `HashDictionary.hpp` header file.
4. Add a stub for your  `probe()` member function to the
   `HashDictionary.cpp` implementation file.  This function returns an
   integer value, so you can return a value of 0 as an initial stub.
5. Your code should compile and run now.  Make sure after adding the
   stub member method your code compiles and runs.  However,
   your unit tests will be failing initially.
6. Incrementally implement the functionality of your `probe()` member
   function.  You should try to add no more than 2 or 3 lines of code,
   and then make sure your program still compiles and runs.  Start by
   adding code to get the first failing test to pass.  Then once that
   test passes, move on to the next failing tests until you have all
   tests passing.  If you write something that causes a previously
   passing test to fail, you should stop and figure out why, and
   either fix it so that the original test still passes, or remove
   what you did and try a new approach.
7. Once you have the `probe()` member function implemented and all
   unit tests passing, you should then move on to the other functions
   in the order suggested.  Some functions use previous ones in this
   assignment, so do them in the order given for you in the tasks
   below.

# Tasks
You should set up your project/code as described in the previous
section.  In this section we give some more details on implementing
the member functions for this assignment.  You should perform the following
tasks for this assignment:

1. Your first task is to implement methods to define the probe
   sequence for closed hashing.  Add a member function named `probe()`
   to the `HashDictionary` class.  Be aware that the `HashDictionary`
   class is a templatized on `<Key, Value>` templates, thus when you
   implement the class methods you need to templatize the class
   methods correctly.  You can look at the example implementations of
   `size()` and the constructors to remind yourself how to do this
   correctly.

   In any case, `probe()` is a member function that takes two
   parameters, a Key and an integer index value.  We are not using
   secondary hashing (as described in our textbook) so the Key value
   will actually not be used in your function.  However, keep it as a
   parameter as the general abstraction/API for the probe function
   should include it for cases where secondary hashing is used.
   `probe()` should be a const class member function, as calling it
   does not change the dictionary.  Finally `probe()` will return an
   integer as its result.  

   Your `probe()` function should implement a quadratic probing scheme
   as described in our Shaffer textbook section 9.4.3 on pg. 338.  Use
   `c1 = 1, c2 =2, c3 = 2` as the parameters for your quadratic probe
   (the tests of `probe()` assume your probe sequence is using these
   parameter values for the quadratic function).

2. Your second tasks is to implement a hash function for integer like
   keys using the described mid-square hashing function (Shaffer 9.4.1
   Example 9.6 pg. 327).  We will create a slight variation of this
   algorithm for our hashing dictionary.  First of all the `hash()`
   member functions should take a `Key` as its only input parameter,
   and it will then return a regular `int` as its result.  Since this
   is a hash function, the integer value should be in the range `0 -
   tableSize-1`, so don't forget to `mod` by the tableSize before
   returning your hash result.

   `hash()` should work like this.  First of all, you should square
   the key value that is passed in.  Then, assuming we are working
   with a 32 bit int, we want to only keep the middle 16 bits of the
   square of the key to use for our hash.  There are many ways to work
   with and get the bits you need, but most likely you will want to
   use C bitwise operators to do this.  For example, a simple method
   to get the middle 16 bits is to first mask out the upper 8 bits
   using the bitwise & operator (e.g. `key & 0x00FFFFFF`) will mask
   out the high order 8 bits to 0).  Then once you have removed the
   upper most significant 8 bits, you can left shift the key by 8
   bits, thus dropping out the lower least significant 8 bits
   (e.g. `key >> 8`).  Performing a mask of the upper 8 bits and
   shifting out the lower 8 bits will result in you only retaining the
   middle 16 bits.

   If your system is using 64 bit integers rather than 32 bit
   integers, perform the mid-square method but retain the middle 32
   bits of the result.  You can use the `sizeof(int)` method to
   determine how many bytes are in an `int` on your system.  I will
   give a bonus point if you write your `hash()` function to
   correctly work for both 32 and 64 bit values by testing
   `sizeof(int)` and doing the appropriate work to get the middle
   bits. Again after you square the key and get the middle bits, make
   sure you modulo the result to get an actual has index in the
   correct range.

3. The third task is to add the `insert()` method to your `HashDictionary`
   so that you can insert new key/value pairs into the dictionary.
   `insert()` should take a constant `Key` reference and a constant `Value`
   reference as its input parameters (note that both of these parameters should
   be declared as const, and they should both be reference parameters, so use
   the `&` to indicate they are passed by reference).  Your `insert()`
   function does not return a result, so it will be a `void` function.

   The algorithm for insert is described in Shaffer 9.4.3 on pg. 334.  You
   need to call and use the `probe()` and `hash()` function you created
   in the first 2 steps to correctly define/implement your closed hashing
   probe sequence.  The basic algorithm is that you use `hash()` to determine
   the initial home slot, and `probe()` gives an offset you should add.
   Basically you have to search the `hashTable` using the probe sequence until
   you find an empty slot.  Once you find an empty slot, you should create
   a new instance of a `KeyValuePair<Key, Value>` object, that contains the
   `key` and `value` that were provided as input parameters to your
   `insert()` function.  This `KeyValuePair` instance should then be inserted
   into the table at the location where you find the first empty slot on the
   probe sequence.  Also don't forget to update the valueCount parameter of
   the `HashDictionary` class that keeps track of the number of items currently
   in the dictionary.

4. Finally you will also implement the `find()` method to search for a particular
   key in your dictionary.  The `find()` member function takes a single `Key`
   parameter as input (it should be a const Key& reference parameter).  The
   `find()` function will return a Value as a result, which will be the `Value`
   of the record associated with the given `Key` if it was found in the
   dictionary, or an empty `Value()` object if it was not found.

   The `find()` method uses the same probe sequence as `insert()`
   implemented by your `probe()` and `hash()` methods.  So you should
   again search along the probe sequence, until you either find the key
   you were given to search for, or else find an empty slot.  Then
   at the end, if you found the key in the `hashTable` you should return
   the `value` that corresponds to the `key` that was searched for.  If
   the search failed and you found an empty slot on your probe sequence, you
   should instead return an empty `Value()` object, which is used as an
   indicator for a failed search.

# Example Output
Here is the correct output you should get from your program
if you correctly implement all the class functions and successfully
pass all of the unit tests given for this assignment.  If you
invoke your function with no command line arguments, only failing
tests are usually shown by default.  In the second example, we use the
-s command line option to have the unit test framework show both
successful and failing tests, and thus we get reports of all of the
successfully passing tests as well on the output.

```
$ ./test
===============================================================================
All tests passed (63 assertions in 6 test cases)








$ ./test -s

-------------------------------------------------------------------------------
test is a Catch v2.7.2 host application.
Run with -? for options

-------------------------------------------------------------------------------
<basic Employee> test basic Employee functionality
-------------------------------------------------------------------------------
assg12-tests.cpp:31
...............................................................................

assg12-tests.cpp:35: PASSED:
  CHECK( e.getId() == 12345 )
with expansion:
  12345 (0x3039) == 12345 (0x3039)

... output snipped ...

===============================================================================
All tests passed (63 assertions in 6 test cases)
```

# Assignment Submission

A MyLeoOnline submission folder has been created for this assignment.
There is a target named `submit` that will create a tared and gziped
file named `assg02.tar.gz`. You should do a `make submit` when finished
and upload your resulting gzip file to the MyLeoOnline Submission
folder for this assignment.

```
$ make submit
tar cvfz assg12.tar.gz assg12-tests.cpp assg12-main.cpp
  HashDictionary.hpp HashDictionary.cpp KeyValuePair.hpp
  KeyValuePair.cpp Employee.hpp Employee.cpp
assg12-tests.cpp
assg12-main.cpp
HashDictionary.hpp
HashDictionary.cpp
KeyValuePair.hpp
KeyValuePair.cpp
Employee.hpp
Employee.cpp
```


# Requirements and Grading Rubrics

## Program Execution, Output and Functional Requirements

1. Your program must compile, run and produce some sort of output to
   be graded.  0 if not satisfied.
1. (20 pts.) `probe()` member function implemented.  Function is using
   quadratic probing as asked for, with correct values for c1, c2 and c3
   parameters.  Probe sequence appears correct and passes tests.
1. (20 pts.) `hash()` member function implemented correctly.  Function
   implements the mid-square method as described.  Function correctly
   uses only the 16 middle bits if system uses 32 bit integers.
1. (30 pts.) `insert()` member function implemented and working.
   Function appears to be correctly generating probe sequence using
   the `probe()` and `hash()` functions.  Items are correctly
   inserted into expected location in the hash table.
1. (30 pts.) `find()` member function implemented and working.
   Function appears to be also correctly using the probe sequence
   in the same was as `insert()`.  Function passes the expected
   tests.

## Program Style

Your programs must conform to the style and formatting guidelines
given for this class.  The following is a list of the guidelines that
are required for the assignment to be submitted this week.

1. Most importantly, make sure you figure out how to set your
   indentation settings correctly.  All programs must use 2 spaces for
   all indentation levels, and all indentation levels must be
   correctly indented.  Also all tabs must be removed from files, and
   only 2 spaces used for indentation.
1. A function header must be present for member functions you define.
   You must give a short description of the function, and document
   all of the input parameters to the function, as well as the return
   value and data type of the function if it returns a value for the
   member functions, just like for regular functions.  However, setter
   and getter methods do not require function headers.
1. You should have a document header for your class.  The class header
   document should give a description of the class.  Also you should
   document all private member variables that the class manages in the
   class document header.
1. Do not include any statements (such as `system("pause")` or
   inputting a key from the user to continue) that are meant to keep
   the terminal from going away.  Do not include any code that is
   specific to a single operating system, such as the
   `system("pause")` which is Microsoft Windows specific.

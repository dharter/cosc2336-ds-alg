/** @file HashDictionary.hpp
 * @brief Header file with definitions of HashDictionary class for
 *   Assignment 12 hashing and dictionaries
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 12
 * @date   June 1, 2020
 *
 * Assignment 12 hashing and dictionaries.  Implement member functions for a
 * HashDictionary class for an open hashing scheme with a quadratic probe
 * sequence. This header file contains the declaration of the HashDictionary
 * class ADT data structure.  You need to add the member function prototypes to
 * this file for this assignment.
 */
#include <iostream>
#include "KeyValuePair.hpp"
using namespace std;


#ifndef HASHDICTIONARY_HPP
#define HASHDICTIONARY_HPP


/** HashDictionary
 * An implementation of a dictionary that uses a hash table to insert, search
 * and delete a set of KeyValuePair items.  In the assignment, we will be
 * implementing a closed hashing table with quadratic probing.  The hash function
 * will implement a version of the mid-square hashing function described in
 * our Shaffer textbook.
 */
template <class Key, class Value>
class HashDictionary
{
protected:
  /// @brief  An array of KeyValuePair items, the hash table this
  ///   class/container is managing.
  KeyValuePair<Key, Value>* hashTable;
  /// @brief  The actual size of the hashTable array
  int tableSize;
  /// @brief  The number of KeyValuePair items that are currently being
  ///   managed and are contained in the hashTable
  int valueCount;
  /// @brief A special user-supplied key that can be used to indicate
  ///   empty slots.  Since how we determine what is a valid/invalid key will
  ///   depend on thekey type, the user must supply this special flag/value
  ///   when setting up the hash dictionary.
  Key EMPTYKEY;

public:
  // constructors and destructors
  HashDictionary(int tableSize, Key emptyKey);
  ~HashDictionary();

  // accessor methods
  int size() const;

  // searching and insertion, add your member prototypes here


  // overload operators (mostly for testing)
  KeyValuePair<Key, Value>& operator[](int index);
  string tostring() const;
  template <typename K, typename V>
  friend ostream& operator<<(ostream& out, const HashDictionary<K, V>& aDict);
};


#include "HashDictionary.cpp"

#endif // HASHDICTIONARY_HPP
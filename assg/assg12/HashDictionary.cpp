/** @file HashDictionary.cpp
 * @brief Implementation file with implementations of HashDictionary class
 *   member functions for Assignment 12 hashing and dictionaries
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 12
 * @date   June 1, 2020
 *
 * Assignment 12 hashing and dictionaries.  Implement member functions for a
 * HashDictionary class for an open hashing scheme with a quadratic probe
 * sequence. This implementation file contains the implementations of the
 * HashDictionary ADT data structure.  You need to add the implementations for
 * the member functions for this assignment to this file.
 */
#include <cassert> // for assert() statements
#include <cmath> // pow() function
#include <sstream>
#include "HashDictionary.hpp"

using namespace std;


/** constructor
 * Standard constructor for the HashDictionary
 *
 * @param tableSize The size of the hash table that should be
 *   generated for internal use by this dictionary for hasing.
 * @param emptyKey A special flag/value that can be used to detect
 *   invalid/unused keys.  We need this so we can indicate which
 *   slots/buckets in our hash table are currently empty, and also
 *   this value is used as a return result when an unsuccessful
 *   search is performed on the dictionary.
 */
template <class Key, class Value>
HashDictionary<Key, Value>::HashDictionary(int tableSize, Key emptyKey)
{
  this->tableSize = tableSize;
  this->EMPTYKEY = emptyKey;
  valueCount = 0;

  // allocate an array/table of the indicated initial size
  hashTable = new KeyValuePair<Key, Value>[tableSize];

  // initialize the hash table so all slots are initially empty
  for (int index = 0; index < tableSize; index++)
  {
    hashTable[index].setKey(EMPTYKEY);
  }
}


/** destructor
 * Standard destructor for the HashDictionary.  Be good memory managers and
 * free up the dynamically allocated array of memory pointed to by hashTable.
 */
template <class Key, class Value>
HashDictionary<Key, Value>::~HashDictionary()
{
  delete[] hashTable;
}


/** size
 * Accessor method to get the current size of this dictionary,
 * e.g. the count of the number of key/value pairs currently being
 * managed in our hash table.
 *
 * @returns in Returns the current number of items being managed by
 *   this dictionary and currently in our hashTable.
 */
template <class Key, class Value>
int HashDictionary<Key, Value>::size() const
{
  return valueCount;
}


/** overload indexing operator[]
 * Overload indexing operator[] to provide direct access
 * to hash table.  This is not normally part of the Dictionary
 * API/abstraction, but included here for testing.
 *
 * @param index An integer index.  The index should be in the range 0 - tablesize-1.
 *
 * @returns KeyValuePair<> Returns a KeyValuePair object if the index into the
 *   internal hash table is a valid index.  This method throws an exception if
 *   the index is not a valid slot of the hash table.
 */
template <class Key, class Value>
KeyValuePair<Key, Value>& HashDictionary<Key, Value>::operator[](int index)
{
  if (index < 0 || index >= tableSize)
  {
    cout << "Error: <HashDictionary::operator[] invalid index: "
         << index << " table size is currently: "
         << tableSize << endl;
    assert(false);
  }

  return hashTable[index];
}


/** HashDictionary to string
 * We normally wouldn't have
 * something like this for a Dictionary or HashTable, but for testing
 * and learning purposes, we want to be able to display the contents of
 * each slot in the hash table of a HashDictionary container.
 *
 * @returns string Returns the constructed string representation of the
 *   current contents of this HashDictionary instance.
 */
template <class Key, class Value>
string HashDictionary<Key, Value>::tostring() const
{
  ostringstream out;

  for (int slot = 0; slot < tableSize; slot++)
  {
    out << "Slot: " << slot << endl;
    out << "     Key  : " << hashTable[slot].key() << endl;
    out << "     Value: " << hashTable[slot].value() << endl;
  }
  out << endl;

  return out.str();
}


/** HashDictionary output stream operator
 * Friend function for HashDictionary.  We normally wouldn't have
 * something like this for a Dictionary or HashTable, but for testing
 * and learning purposes, we want to be able to display the contents of
 * each slot in the hash table of a HashDictionary container.
 *
 * @param out An output stream reference into which we should insert
 *   a representation of the given HashDictionary.
 * @param aDict A HashDictionary object that we want to display/represent
 *   on an output stream.
 *
 * @returns ostream& Returns a reference to the original given output stream,
 *   but now the values representing the dictionary we were given should
 *   have been sent into the output stream.
 */
template <typename K, typename V>
ostream& operator<<(ostream& out, const HashDictionary<K, V>& aDict)
{
  //for (int slot = 0; slot < aDict.tableSize; slot++)
  //{
  //  out << "Slot: " << slot << endl;
  //  out << "     Key  : " << aDict.hashTable[slot].key() << endl;
  //  out << "     Value: " << aDict.hashTable[slot].value() << endl;
  //}
  //out << endl;
  out << aDict.tostring();
  return out;
}
/** @file KeyValuePair.hpp
 * @brief Header file with definitions of KeyValuePair class for
 *   Assignment 12 hashing and dictionaries
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 12
 * @date   June 1, 2020
 *
 * Template class for definining Key/Value pairs, suitable for dictionary and
 * hash table implementations. Based on Shaffer KVPair ADT definition, pg. 139
 * Fig 4.31.
 */
#ifndef KEYVALUEPAIR_HPP
#define KEYVALUEPAIR_HPP


/** KeyValue Pair
 * Definition of basic key/value pair container.  This container of course
 * associates a value (usually a record like a class or struct), with
 * a key (can be anything).
 *
 * We do not use the comparator Strategy pattern as discussed in
 * Shaffer pg. 144 here.  We assume that the Key type has suitably
 * overloaded operators for <, >, ==, <=, >= operations as needed
 * in order to compare and order keys if needed by dictionaries and
 * hash tables using a KeyValuePair.
 */
template <class Key, class Value>
class KeyValuePair
{
private:
  /// @brief The key for a key/value pair item/association.
  Key myKey;
  /// @brief The value for a key/value pair, usually something like a record
  ///   (a class or struct of data we are hashing or keeping in a dictionary).
  Value myValue;

public:
  // constructors
  KeyValuePair();
  KeyValuePair(Key key, Value value);

  // accessors, getters and setters
  Key key();
  void setKey(Key key);
  Value& value();
};


#include "KeyValuePair.cpp"

#endif // KEYVALUEPAIR_HPP
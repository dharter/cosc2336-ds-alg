/** @file Employee.cpp
 * @brief Implementation file with implementations of Employee class
 *   member functions for Assignment 12 hashing and dictionaries
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 12
 * @date   June 1, 2020
 *
 * Simple example of an Employee record/class we can use to demonstrate
 * HashDictionary key/value pair management.
 */
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include "Employee.hpp"
using namespace std;


/** constructor
 * Default constructor for our Employee record/class.  Construct an
 * empty employee record
 */
Employee::Employee()
{
  this->id = EMPTY_EMPLOYEE_ID;
  this->name = "";
  this->address = "";
  this->salary = 0.0;
}


/** constructor
 * Basic constructor for our Employee record/class.  This alternate
 * constuctor uses given parameters to construct a non-empty Employee
 * record.
 *
 * @param id The unique employee id (should check it is not a duplicate
 *   somewhere when creating).
 * @param name The full name of the Employee to create.
 * @param address The street address information for the Employee.
 * @param salary Salary information for the Employee being created.
 */
Employee::Employee(int id, string name, string address, float salary)
{
  this->id = id;
  this->name = name;
  this->address = address;
  this->salary = salary;
}


/** id accessor
 * Accessor method to get the employee id.
 *
 * @returns int Returns the integer employee id value.
 */
int Employee::getId() const
{
  return id;
}


/** name accessor
 * Accessor method to get the employee name.
 *
 * @returns string Returns the string containing the full
 *   employee name for this record.
 */
string Employee::getName() const
{
  return name;
}


/** to string
 * Create and return a string representation of this Employee record.
 *
 * @returns string Returns a string representation of the information in
 *   this Employee record.
 */
string Employee::tostring() const
{
  ostringstream out;

  out << "( id: " << id << ", "
      << name << ", "
      << address << ", "
      << fixed << setprecision(2) << salary << " )" << endl;

  return out.str();
}


/** overload operator<<
 * Friend function to ouput representation of Employee to an
 * output stream.
 *
 * @param out A reference to an output stream to which we should
 *   send the representation of an employee record for display.
 * @param employee The reference to the employee record to be displayed.
 *
 * @returns ostream& Returns a reference to the original output
 *   stream, but now the employee information should have been
 *   inserted into the stream for display.
 */
ostream& operator<<(ostream& out, Employee& employee)
{
  //out << "( id: " << employee.id << ", "
  //    << employee.name << ", "
  //    << employee.address << ", "
  //    << fixed << setprecision(2) << employee.salary << " )" << endl;
  out << employee.tostring();

  return out;
}
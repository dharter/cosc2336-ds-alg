/** @file BinaryTree.hpp
 * @brief Header file with class definition for the BinaryTree class
 *   for Assignment 11 binary trees
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 11
 * @date   June 1, 2020
 *
 * Assignment 11 BinaryTrees.  Implement some missing member functions for a
 * BinaryTree ADT data structure. This header file contains the declaration of
 * the BinaryTree class ADT data structure.  You need to add the member function
 * prototypes to this file for this assignment.
 */
#include <string>

using namespace std;


#ifndef BINARYTREE_HPP
#define BINARYTREE_HPP


/** Binary Tree Node
 * A binary tree node, based on Shaffer binary tree node ADT, pg. 156.,
 * implementation pg. 161.  The node class is not the tree.  A binary
 * search tree consists of a structure/colleciton of binary tree nodes,
 * arranged of course as a binary tree.  A binary tree nodes purpose is to
 * store the key/value of a single item being managed, and to keep links
 * to left and right children.
 *
 * We assume both key and value are the same single item here.  This
 * version is not templatized, we create nodes that hold simple int
 * values, but we could parameritize this to hold arbitrary value
 * types.
 */
struct BinaryTreeNode
{
  /// @brief item The item held by this binary tree node.  This item is both
  ///   the key and the value of the item being stored.  In alternative
  ///   implementations we might want to split the key and value into two
  ///   separate fields.
  int item;
  /// @brief Pointer to the left child of this node.  This is NULL if there
  ///   is currently no left child of this node.  If both left and right
  ///   are NULL then this node is a leaf node.
  BinaryTreeNode* left;
  /// @brief Pointer to the right child node of this node.  This is NULL if
  ///   there is currently no right child of this node.  If both left and right
  ///   are NULL then this node is a leaf node.
  BinaryTreeNode* right;
};


/** Binary Tree
 * A binary search tree implementation, using pointers/linked list, based
 * on Shaffer example implementation pg. 171.  This is the class that
 * actually manages/implements the tree.  It contains a single
 * pointer to the root node at the top (or bottom depending on how you
 * view it) of the tree.  We also maintain a count of the number of nodes
 * currently in the tree.  This class will support insertion
 * and searching for new nodes.
 */
class BinaryTree
{
private:
  /// @brief A pointer to the root node at the top of the tree.  When the
  ///   tree is initially created and/or when the tree is empty then root
  ///   will be null.
  BinaryTreeNode* root;
  /// @brief The count of the number of nodes/items currently in this
  ///   binary tree.
  int nodeCount;

  // private helper methods, do actual work usually using recursion
  string tostring(BinaryTreeNode* node) const;

public:
  // constructors and destructors
  BinaryTree();
  ~BinaryTree();

  // accessor methods
  int size() const;

  // insertion, height, clear, add function prototypes here

  // tree traversal and display
  string tostring() const;
  friend ostream& operator<<(ostream& out, const BinaryTree& aTree);
};


#endif // BINARYTREE_HPP
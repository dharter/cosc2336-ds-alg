/** @file assg11-main.cpp
 * @brief main/debug executable for Assignment 11 binary trees
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 11
 * @date   June 1, 2020
 *
 * Assignment 11 BinaryTrees.  Implement some missing member functions for a
 * BinaryTree ADT data structure. This file contains a main() function we can
 * use to build a debug target for debugging.
 */
#include <iostream>
#include "BinaryTree.hpp"
using namespace std;


/** main
 * The main entry point for this program.  Execution of this program
 * will begin with this main function.
 *
 * @param argc The command line argument count which is the number of
 *     command line arguments provided by user when they started
 *     the program.
 * @param argv The command line arguments, an array of character
 *     arrays.
 *
 * @returns An int value indicating program exit status.  Usually 0
 *     is returned to indicate normal exit and a non-zero value
 *     is returned to indicate an error condition.
 */
int main(int argc, char** argv)
{
  // example of invoking some of the functions to debug them
  BinaryTree tree;
  /*
     tree.insert(10);
     tree.insert(5);
     tree.insert(15);
     tree.insert(20);
     tree.insert(30);
     tree.insert(40);
     tree.insert(50);
     tree.insert(4);
     tree.insert(3);
     tree.insert(2);
     tree.insert(1);
     tree.insert(0);
   */

  cout << "Tree contents: " << tree <<endl;

  // return 0 to indicate successful completion of program
  return 0;
}
/** @file LinkedList.hpp
 * @brief Header file for Assignment 08, Linked Lists data types.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 08
 * @date   June 1, 2020
 *
 * Assignment 08 Linked Lists data type. Implementation of a LinkedList ADT
 * container to maintain a list of integers.  This code is based on example code
 * from Malik chapter 17 LinkedList template class. This header file contains
 * the declaration of the ListType class and prototypes for all of its member
 * functions.
 */
#include <iostream>
using namespace std;

#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_

/** Node
 * A simple Node structure type to be used as items
 * in our linked list ADT implementations.
 */
template <class T>
struct Node
{
  /// @brief The actual data of type T this node contains.
  T info;
  /// @brief A pointer to the next node of the linked list.
  Node<T>* link;
};


/** LinkedList iterator
 * A class that provides common iteration over a LinkedList
 * data type.  Malik 6th ed. pg. 1073
 */
template <class T>
class LinkedListIterator
{
private:
  /// @brief current pointer to the point current node in linked list we
  ///   are iterating through
  Node<T>* current;

public:
  // constructors
  LinkedListIterator();
  LinkedListIterator(Node<T>* ptr);

  // overloaded operators
  T operator*();
  LinkedListIterator<T> operator++();
  bool operator==(const LinkedListIterator<T>& right) const;
  bool operator!=(const LinkedListIterator<T>& right) const;
};


/** LinkedList ADT
 * This is an implementation of some of the functionality of the
 * Malik 6th ed. pg. 1072 LinkedList data type
 */
template <class T>
class LinkedList
{
private:
  /// @brief An int that keeps the current count of the number of items
  ///   in this list.
  int count;
  /// @brief pointer to the first node of the linked list.
  Node<T>* first;
  /// @brief pointer to the last node of the linked list.
  Node<T>* last;

public:
  // constructors and destructors
  LinkedList();
  LinkedList(const LinkedList<T>& otherList);
  ~LinkedList();

  // simple accessor and information methods
  bool isEmpty() const;
  int length() const;
  T front() const;
  T back() const;

  // general member functions
  void copyList(const LinkedList<T>& otherList);
  void destroyList();

  // functions for insertion, deletion, searching
  void insertFirst(const T& newItem);
  void insertLast(const T& newItem);

  // functions for iterating over the list items
  LinkedListIterator<T> begin();
  LinkedListIterator<T> end();

  // overloaded operators
  string tostring() const;

  template <typename U>
  friend ostream& operator<<(ostream& out, const LinkedList<U>& list);

  // add your member function prototypes for assignment here
};


/** Exception item not found
 * Exception for our LinkedList class to throw when searching for
 * an item and it is not found.
 */
class LinkedListItemNotFoundException
{
private:
  /// @brief An error message with additonal information about this item
  ///   not found exception.
  string message;

public:
  /** default constructor
   * Default constructor for the not found exception.
   */
  LinkedListItemNotFoundException()
  {
    message = "Error: LinkedList item not found";
  }


  /** constructor with string
   * Constructor for not found exception with string message.
   *
   * @param str An additional message for the not found exception.
   */
  LinkedListItemNotFoundException(string str)
  {
    message = "Error: LinkedList item not found" + str;
  }


  /** what accessor
   * Accessor method to find out what the exception message is.
   *
   * @returns string Returns the not found exception message.
   */
  string what()
  {
    return message;
  }


};


// include template implementations so they are included with header
#include "LinkedList.cpp"

#endif // _LINKEDLIST_H_
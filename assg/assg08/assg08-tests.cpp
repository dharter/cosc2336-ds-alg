/** @file assg08-tests.cpp
 * @brief Unit tests for Assignment 08, Linked Lists data types.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 08
 * @date   June 1, 2020
 *
 * Assignment 08 Linked Lists data type. Implementation of a LinkedList ADT
 * container to maintain a list of integers.  This code is based on example code
 * from Malik chapter 17 LinkedList template class. This file has catch2 unit
 * tests you need to test and implement the functions for your assignment.
 */
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "LinkedList.hpp"
using namespace std;


/** test LinkedList() member functions.  Not an exhaustive set of tests,
 * but example of using some of the basic linked list functions given
 * to you in this assignment.
 */
TEST_CASE("<basic functions> test LinkedList basic member function",
          "[member]")
{
  LinkedList<string> list;

  // list constructed with default constructor should be empty initially
  CHECK( list.isEmpty() );
  CHECK( list.length() == 0 );
  CHECK_THROWS( list.front(), LinkedListItemNotFoundException() );
  CHECK_THROWS( list.back(), LinkedListItemNotFoundException() );

  // after insert a single item, list is no longer empty, and the item is
  // both the front and the back item.
  list.insertFirst("first");
  CHECK_FALSE( list.isEmpty() );
  CHECK( list.length() == 1 );
  CHECK( list.front() == "first" );
  CHECK( list.back() == "first" );

  // insert an item on the front (of non-empty list)
  list.insertFirst("second");
  CHECK_FALSE( list.isEmpty() );
  CHECK( list.length() == 2 );
  CHECK( list.front() == "second" );
  CHECK( list.back() == "first" );
  CHECK( list.tostring() ==
         "List count: 2\n"
         "      info: [second, first]\n"
         );

  // insert an item on the back (of non-empty list)
  list.insertLast("third");
  CHECK_FALSE( list.isEmpty() );
  CHECK( list.length() == 3 );
  CHECK( list.front() == "second" );
  CHECK( list.back() == "third" );
  CHECK( list.tostring() ==
         "List count: 3\n"
         "      info: [second, first, third]\n"
         );

  // test linked list iterator (Malik 6th ed pg. 1073 )
  // it might be nice if LinkedList had a method to return a regular
  // c array of the list, though probably not useful for testing as we
  // should not rely on what the LinkedList things is its current
  // contents but instead what we expect the contents to be
  string expected[] = {"second", "first", "third"};
  LinkedListIterator<string> itr = list.begin();
  int index = 0;
  for ( ; itr != list.end(); ++itr)
  {
    // c++ iterators, dereference iterator produces next item in iteration
    string item = *itr;

    // test the item is what we expect
    CHECK( item == expected[index] );

    // move to next expected item in next iteration
    index++;
  }

  // if you haven't discovered new C++ range based iteration yet, they allow
  // using class iterators to write loops but avoid a lot of the cruft of
  // using these iterators
  index = 0;
  for (string item : list) // for each string item in the list
  {
    CHECK( item == expected[index] );

    // move to next expected item in next iteration
    index++;
  }

  // test copy list member function
  LinkedList<string> copy1;
  copy1.copyList(list);
  CHECK_FALSE( copy1.isEmpty() );
  CHECK( copy1.length() == 3 );
  CHECK( copy1.front() == "second" );
  CHECK( copy1.back() == "third" );
  CHECK( copy1.tostring() ==
         "List count: 3\n"
         "      info: [second, first, third]\n"
         );


  // test copy constructor
  LinkedList<string> copy2(list);
  CHECK_FALSE( copy2.isEmpty() );
  CHECK( copy2.length() == 3 );
  CHECK( copy2.front() == "second" );
  CHECK( copy2.back() == "third" );
  CHECK( copy2.tostring() ==
         "List count: 3\n"
         "      info: [second, first, third]\n"
         );


  // test destroyList empties the list.  Also make sure it does not
  // actually change any copy
  list.destroyList();
  CHECK( list.length() == 0 );
  CHECK_THROWS( list.front(), LinkedListItemNotFoundException() );
  CHECK_THROWS( list.back(), LinkedListItemNotFoundException() );
  CHECK( list.tostring() ==
         "List count: 0\n"
         "      info: []\n"
         );

  // copy1 and copy2 still same as before copy, make sure if add something
  // to copy1 not effecting copy2
  copy1.insertLast("last");
  CHECK( copy1.length() == 4 );
  CHECK( copy1.front() == "second" );
  CHECK( copy1.back() == "last" );
  CHECK( copy1.tostring() ==
         "List count: 4\n"
         "      info: [second, first, third, last]\n"
         );
  CHECK( copy2.length() == 3 );
  CHECK( copy2.front() == "second" );
  CHECK( copy2.back() == "third" );
  CHECK( copy2.tostring() ==
         "List count: 3\n"
         "      info: [second, first, third]\n"
         );
}


/** A test fixture is a common Unit Test concept.  Basically catch2 will
 * create a new instance of this class for each TEST_CASE_METHOD where we
 * use this fixture.  We create instances of some LinkedList objects, and
 * initialize them so that the test cases have a known initial set of lists
 * with content in them each time.
 */
struct LinkedListTestFixture
{
public:
  /// @brief A LinkedList of strings for testing
  LinkedList<string> slist;
  /// @brief A LinkedList of integers for testing
  LinkedList<int> ilist;
  /// @brief A LinkedList (of strings) with a single value (initially)
  ///   for testing
  LinkedList<string> onelist;
  /// @brief A LinkedList (of integers) that is initially empty for testion
  LinkedList<int> emptylist;

  // constructor to intialize the lists with a set of values for our fixtures
  LinkedListTestFixture()
  {
    // set up string list with 4 values in it [bob, frank, alice, jane]
    slist.insertLast("bob");
    slist.insertLast("frank");
    slist.insertLast("alice");
    slist.insertLast("jane");

    // set up integer list with 6 values in it [9, 7, 14, 2, 8, 12]
    ilist.insertFirst(12);
    ilist.insertFirst(8);
    ilist.insertFirst(2);
    ilist.insertFirst(14);
    ilist.insertFirst(7);
    ilist.insertFirst(9);

    // set up onelist with only a single value in it
    onelist.insertFirst("one");

    // empty list is constructued empty with no values in it by
    // the default constructor
  }


};


/** test search() member functions.  This is the first membr function
 * you are to implement for this assignment.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some search() member function.
   TEST_CASE_METHOD(LinkedListTestFixture,
                 "<search()> test LinkedList search member function",
                 "[member]")
   {
   // this test is using LinkedListTextFixture, so should have public instances
   // of all of the LinkedList objects already set up with expected list of
   // values in them

   // test if find front item on list, should return true for successful search
   CHECK( slist.search("bob") );
   CHECK( ilist.search(9) );
   CHECK( onelist.search("one") );

   // test if last item is found correctly
   CHECK( slist.search("jane") );
   CHECK( ilist.search(12) );

   // test if item in middle of list found
   CHECK( slist.search("frank") );
   CHECK( ilist.search(2) );
   CHECK( slist.search("alice") );
   CHECK( ilist.search(7) );

   // test unsuccessful search
   CHECK_FALSE( slist.search("chris") );
   CHECK_FALSE( ilist.search(42) );
   CHECK_FALSE( onelist.search("two") );

   // empty list if we search should return false
   CHECK_FALSE( emptylist.search(0) );
   }
 */


/** test deleteNode() member function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some deleteNode() member function.
   TEST_CASE_METHOD(LinkedListTestFixture,
                 "<deleteNode()> test LinkedList deleteNode member function",
                 "[member]")
   {
   // delete the first node from list
   ilist.deleteNode(9);
   CHECK_FALSE( ilist.isEmpty() );
   CHECK( ilist.length() == 5);
   CHECK( ilist.front() == 7 );
   CHECK( ilist.tostring() ==
         "List count: 5\n"
         "      info: [7, 14, 2, 8, 12]\n"
         );

   // delete back node from list
   ilist.deleteNode(12);
   CHECK_FALSE( ilist.isEmpty() );
   CHECK( ilist.length() == 4);
   CHECK( ilist.back() == 8 );
   CHECK( ilist.tostring() ==
         "List count: 4\n"
         "      info: [7, 14, 2, 8]\n"
         );

   // delete middle node from list
   ilist.deleteNode(14);
   CHECK_FALSE( ilist.isEmpty() );
   CHECK( ilist.length() == 3);
   CHECK( ilist.front() == 7 );
   CHECK( ilist.back() == 8 );
   CHECK( ilist.tostring() ==
         "List count: 3\n"
         "      info: [7, 2, 8]\n"
         );

   // try and delete nonexistent node from nonempty list,
   // should get an exception
   CHECK_THROWS( ilist.deleteNode(1), LinkedListItemNotFoundException() );

   // test again with the string list
   // delete the first node from list
   slist.deleteNode("bob");
   CHECK_FALSE( ilist.isEmpty() );
   CHECK( slist.length() == 3);
   CHECK( slist.front() == "frank" );
   CHECK( slist.tostring() ==
         "List count: 3\n"
         "      info: [frank, alice, jane]\n"
         );

   // delete back node from list
   slist.deleteNode("jane");
   CHECK_FALSE( slist.isEmpty() );
   CHECK( slist.length() == 2);
   CHECK( slist.back() == "alice" );
   CHECK( slist.tostring() ==
         "List count: 2\n"
         "      info: [frank, alice]\n"
         );

   // delete current front node
   slist.deleteNode("frank");
   CHECK_FALSE( slist.isEmpty() );
   CHECK( slist.length() == 1);
   CHECK( slist.front() == "alice" );
   CHECK( slist.back() == "alice" );
   CHECK( slist.tostring() ==
         "List count: 1\n"
         "      info: [alice]\n"
         );

   // delete last node from list, make sure becomes empty correctly
   slist.deleteNode("alice");
   CHECK( slist.isEmpty() );
   CHECK( slist.length() == 0);
   CHECK_THROWS( slist.front(), LinkedListItemNotFoundException() );
   CHECK_THROWS( slist.back(), LinkedListItemNotFoundException() );
   CHECK( slist.tostring() ==
         "List count: 0\n"
         "      info: []\n"
         );

   // try and delete nonexistent node from empty list
   CHECK_THROWS( slist.deleteNode("andy"), LinkedListItemNotFoundException() );
   }
 */


/** test findItemAtIndex() member function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some findItemAtIndex() member function.
   TEST_CASE_METHOD(LinkedListTestFixture,
                 "<findItemAtIndex()> test LinkedList findItemAtIndex member function",
                 "[member]")
   {
   // test access first item from list
   CHECK( ilist.findItemAtIndex(0) == 9 );
   CHECK( slist.findItemAtIndex(0) == "bob" );

   // access when only item
   CHECK( onelist.findItemAtIndex(0) == "one" );

   // test access of last item in list
   CHECK( ilist.findItemAtIndex(5) == 12 );
   CHECK( slist.findItemAtIndex(3) == "jane" );

   // test access of items in middle somewhere of lists
   CHECK( ilist.findItemAtIndex(2) == 14 );
   CHECK( slist.findItemAtIndex(1) == "frank" );
   CHECK( ilist.findItemAtIndex(4) == 8 );
   CHECK( slist.findItemAtIndex(2) == "alice" );

   // findItemAtIndex should trhow item not found exception if try and
   // access invalid index beyond bounds of the list
   // negative indexes are alwasy invalid
   CHECK_THROWS( ilist.findItemAtIndex(-1), LinkedListItemNotFoundException() );
   CHECK_THROWS( slist.findItemAtIndex(-5), LinkedListItemNotFoundException() );

   // indexes beyond bounds are also illegal
   CHECK_THROWS( ilist.findItemAtIndex(6), LinkedListItemNotFoundException() );
   CHECK_THROWS( slist.findItemAtIndex(55), LinkedListItemNotFoundException() );
   CHECK_THROWS( onelist.findItemAtIndex(1), LinkedListItemNotFoundException() );
   CHECK_THROWS( emptylist.findItemAtIndex(0), LinkedListItemNotFoundException() );
   }
 */


/** test deleteItemAtIndex() member function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some deleteItemAtIndex() member function.
   TEST_CASE_METHOD(LinkedListTestFixture,
                 "<deleteItemAtIndex()> test LinkedList findItemAtIndex member function",
                 "[member]")
   {
   // delete first item in list
   ilist.deleteItemAtIndex(0);
   CHECK( ilist.length() == 5);
   CHECK( ilist.front() == 7 );
   CHECK( ilist.tostring() ==
         "List count: 5\n"
         "      info: [7, 14, 2, 8, 12]\n"
         );

   // delete last item from list
   ilist.deleteItemAtIndex(4);
   CHECK_FALSE( ilist.isEmpty() );
   CHECK( ilist.length() == 4);
   CHECK( ilist.back() == 8 );
   CHECK( ilist.tostring() ==
         "List count: 4\n"
         "      info: [7, 14, 2, 8]\n"
         );

   // delete middle node from list
   ilist.deleteItemAtIndex(1);
   CHECK_FALSE( ilist.isEmpty() );
   CHECK( ilist.length() == 3);
   CHECK( ilist.front() == 7 );
   CHECK( ilist.back() == 8 );
   CHECK( ilist.tostring() ==
         "List count: 3\n"
         "      info: [7, 2, 8]\n"
         );

   // try and delete nonexistent node from nonempty list,
   // should get an exception
   CHECK_THROWS( ilist.deleteItemAtIndex(-1), LinkedListItemNotFoundException() );
   CHECK_THROWS( ilist.deleteItemAtIndex(3), LinkedListItemNotFoundException() );

   // test again with the string list, this time deleting items until nothing left
   // delete the first node from list
   slist.deleteItemAtIndex(0);
   CHECK_FALSE( ilist.isEmpty() );
   CHECK( slist.length() == 3);
   CHECK( slist.front() == "frank" );
   CHECK( slist.tostring() ==
         "List count: 3\n"
         "      info: [frank, alice, jane]\n"
         );

   // delete back node from list
   slist.deleteItemAtIndex(2);
   CHECK_FALSE( slist.isEmpty() );
   CHECK( slist.length() == 2);
   CHECK( slist.back() == "alice" );
   CHECK( slist.tostring() ==
         "List count: 2\n"
         "      info: [frank, alice]\n"
         );

   // delete current front node
   slist.deleteItemAtIndex(0);
   CHECK_FALSE( slist.isEmpty() );
   CHECK( slist.length() == 1);
   CHECK( slist.front() == "alice" );
   CHECK( slist.back() == "alice" );
   CHECK( slist.tostring() ==
         "List count: 1\n"
         "      info: [alice]\n"
         );

   // delete last node from list, make sure becomes empty correctly
   slist.deleteItemAtIndex(0);
   CHECK( slist.isEmpty() );
   CHECK( slist.length() == 0);
   CHECK_THROWS( slist.front(), LinkedListItemNotFoundException() );
   CHECK_THROWS( slist.back(), LinkedListItemNotFoundException() );
   CHECK( slist.tostring() ==
         "List count: 0\n"
         "      info: []\n"
         );

   // try and delete nonexistent node from empty list
   CHECK_THROWS( slist.deleteItemAtIndex(0), LinkedListItemNotFoundException() );
   }
 */


/** test operator[]() overloaded operator member function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some overloaded operator[]() member function.  Only uncomment these
 * tests if you implement the operator[] extra credit for the assignment.
   TEST_CASE_METHOD(LinkedListTestFixture,
                 "<operator[]()> test LinkedList overloaded operator[] member function",
                 "[operator]")
   {
   // perform all the same tests but using operator[] to begin with
   // test access first item from list
   CHECK( ilist[0] == 9 );
   CHECK( slist[0] == "bob" );

   // access when only item
   CHECK( onelist[0] == "one" );

   // test access of last item in list
   CHECK( ilist[5] == 12 );
   CHECK( slist[3] == "jane" );

   // test access of items in middle somewhere of lists
   CHECK( ilist[2] == 14 );
   CHECK( slist[1] == "frank" );
   CHECK( ilist[4] == 8 );
   CHECK( slist[2] == "alice" );

   // findItemAtIndex should trhow item not found exception if try and
   // access invalid index beyond bounds of the list
   // negative indexes are alwasy invalid
   CHECK_THROWS( ilist[-1], LinkedListItemNotFoundException() );
   CHECK_THROWS( slist[-5], LinkedListItemNotFoundException() );

   // indexes beyond bounds are also illegal
   CHECK_THROWS( ilist[6], LinkedListItemNotFoundException() );
   CHECK_THROWS( slist[55], LinkedListItemNotFoundException() );
   CHECK_THROWS( onelist[1], LinkedListItemNotFoundException() );
   CHECK_THROWS( emptylist[0], LinkedListItemNotFoundException() );

   // to get full extra credit for the operator[] you need to have correctly
   // defined to returne a T& (a reference to a T) so we can also assign
   // value to list using this member function/operator
   onelist[0] = "another one";
   CHECK( onelist[0] == "another one" );

   ilist[0] = -5;
   CHECK( ilist[0] == -5 );
   ilist[5] = -12;
   CHECK( ilist.findItemAtIndex(5) == -12 );
   ilist[2] = -42;
   CHECK( ilist[2] == -42 );
   CHECK( ilist.tostring() ==
         "List count: 6\n"
         "      info: [-5, 7, -42, 2, 8, -12]\n"
         );

   slist[0] = "betty";
   CHECK( slist[0] == "betty" );
   slist[3] = "jack";
   CHECK( slist.findItemAtIndex(3) == "jack" );
   slist[1] = "fanny";
   CHECK( slist[1] == "fanny" );
   CHECK( slist.tostring() ==
         "List count: 4\n"
         "      info: [betty, fanny, alice, jack]\n"
         );
   }
 */


/** test toReverseString() member function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the toReverseString() member function.  Only uncomment these
 * tests if you implement the toReverseString() member function extra credit
 * for the assignment.
   TEST_CASE_METHOD(LinkedListTestFixture,
                 "<toReverseString()> test LinkedList toReverseString member function",
                 "[member]")
   {
   CHECK( emptylist.toReverseString() ==
         "List count: 0\n"
         "      info: []\n"
         );

   CHECK( onelist.toReverseString() ==
         "List count: 1\n"
         "      info: [one]\n"
         );

   CHECK( ilist.toReverseString() ==
         "List count: 6\n"
         "      info: [12, 8, 2, 14, 7, 9]\n"
         );

   CHECK( slist.toReverseString() ==
         "List count: 4\n"
         "      info: [jane, alice, frank, bob]\n"
         );
   }
 */
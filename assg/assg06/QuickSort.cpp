/** @file QuickSort.cpp
 * @brief Implementation file for Assignment 06 the Quick sort algorithm.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 06
 * @date   June 1, 2020
 *
 * Assignment 06 implementing the Quick sort algorithm. This implementation file
 * should contian the actual implementation ouf the functions you write to
 * implement the quick sort.
 */
 #include "QuickSort.hpp"


// function implementations go here
// do not forget to create and include function documentation for
// your functions
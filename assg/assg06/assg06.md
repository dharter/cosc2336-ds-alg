---
title: 'Assignment 06: Quicksort'
author: 'COSC 2336: Data Structures and Algorithms'
date: 'Fall 2020'
---

# Objectives
- Practice writing functions
- Practice writing recursive functions.
- Learn about Analysis of algorithms and $\mathcal{O}(n \; \log \; n)$ sorts

# Description

In this assignment you will be implementing one of the most popular
sorting algorithms used in libraries (like the C++ STL library, and
the UNIX qsort function) to provide basic sorting abilities, the
Quicksort algorithm.  I would recommend that you at least read section
7.5 from our supplemental Shaffer textbook on Quicksort, if not
sections 7.1-7.5 talking about three well known $\mathcal{O}(n \; \log
\; n)$ sorting algorithms, and the 3 $\mathcal{O}(n^2)$ algorithms we
discussed for this unit.

Quicksort, when properly implemented, is very attractive because it
provides a way to do a fast sort completely in-place (without having to
allocate additional memory to do the sort, beyond a single value
needed when swapping two values in the list being sorted).  In the
worst case, Quicksort is actually $\mathcal{O}(n^2)$, no better than
bubble sort.  But this worse case only occurs when every pivot selected
is the wort possible, and does not divide the list at all.  This is very
unlikely to happen, unless you know how the pivot is selected, and specifically
design the input list to always choose the worst possible pivot.
On average the cost of Quicksort is $\mathcal{O}(n \; \log \; n)$, and it is
usually very likely that average case performance will result when lists
to be sorted are relatively random.

The most direct implementation of Quicksort is as a recursive
algorithm.  Quicksort is an example of a divide and conquer approach
to solving the problem of sorting a list.  We are given a list of
items, `list` and indexes `left` and `right` that indicate a sub-portion of the
list to be sorted.  `left` and `right` indicate the actual indexes, so if
the list is a regular C array of integers, and the array is of size 10

```c++
int left;
int right;
const inst SIZE = 10;
int list[size];
```

Then to sort the whole list we set `left = 0` and `right = 9` to initially call the
Quicksort function:

```c++
left = 0;
right = size-1;
quicksort(list, left, right);
```

Conceptually the steps of the Quicksort algorithm are as follows:

1. if list size is 0 or 1 (`left <= right`) return (lists of this size are sorted by definition).
2. Choose a pivot value and swap the pivot value to the
   end of the list `swap(pivotIndex, right)`
3. Partition the list.  Partitioning means all values less than the
   pivot value should end up on the left of the list, and all values
   greater will be on the right.  The first index `k` where a value >= to
   the pivot value is at indicates the new left and right side sub-lists.
4. Swap the pivot value to its correct position `k` `swap(k, right)`
5. Recursively call Quicksort on the new left and right sub-lists
   - `quicksort(list, left, k-1)`
   - `quicksort(list, k+1, right)`

Most of the real work happens in the function/code to partition the
list.  The partitioning of the list, for Quicksort to be an in-place
sort, must work by swapping values in-place in the list of items.  All
values smaller than the pivot value must end up on the left side, and
all values greater on the right.  The algorithm to partition the list
is traditionally done with these steps:

1. do a linear search from the left of list, stop at first value on left that
   is >= pivot (notice the >= here, that is important)
2. do a linear search from the right of list, stop at first value that is <
   than the pivot (notice the strictly < here, that is important)
3. `swap(left, right`) assuming you were incrementing left and decrementing
   right to point to indexes of values that are on wrong sides
4. if left < right goto 1

Conceptually we search from both ends of the list, and when we find
values that are on wrong sides with respect to the pivot value, we
swap them.  Eventually the search from both ends will meet somewhere.
The location where they meet should be the index of the first value
that is >= to the pivot (going from the left side of the list).  This
is the index where the pivot value should actually go in the list,
because all values before this are smaller than the pivot, and all
values at or after are greater than or equal to the pivot.  Thus at
the end of partitioning the list on some pivot value, we are able to
swap 1 value each time to its correct final position in the list.  But
the values to the left and right will not be sorted, thus we call
Quicksort recursively on these sub-lists to get them sorted.

In order to help you implement your own version of Quicksort, we have
broken the problem down into useful sub-functions.  If you implement
the sub-functions as specified, in the order given, the final
implementation of the Quicksort algorithm is relatively straight
forward using these smaller functions.

# Setup

For this assignment you will be given the following files:

| File Name          | Description                            |
|--------------------|----------------------------------------|
| `assg06-tests.cpp` | Unit tests for the quicksort and other |
|                    | functions you are to write.            |
| `QuickSort.hpp`    | Header file declaring function         |
|                    | prototypes of the functions you are    |
|                    | to write to implement a quick sort.    |
| `QuickSort.cpp`    | Implementation file of the             |
|                    | functions you are to implement         |
|                    | for this assignment.                   |

Set up a multi-file project to compile the two `.cpp` source files
together and run them as shown for the class.  The Makefile you were
given should be usable to create a build project using the Atom editor
as required in this class.  The general approach you should take for
this assignment, and all assignment is:

1. Set up your project with the given starting templates.  The files
   should compile and run, but either no tests will be run, or tests
   will run but be failing.
2. For this project, start by uncommenting the first `TEST_CASE` in
   the `assg06-tests.cpp` file.  These are the unit tests to test the
   functionality of your `swapListValues()`.
3. Add the correct function prototype for the `swapListValues()`
   function to the `QuickSort.hpp` header file.  The prototype
   consists of the name of the function, its input parameters and
   their types, and the return value of the function (the swap
   function is a void function in this case).
4. Add a stub/empty implementation of `swapListValues()` to the
   `QuickSort.cpp` implementation file.  The function should
   have the same signature as the prototype you gave in the header
   file. Documentation for the function has not been given for you
   this time, so add documentation of your function first.  The
   function should initially just return a 0 result so you can test
   your project is now compiling and running.
5. Your code should compile and run now.  Make sure after adding the
   function prototype and stub your code compiles and runs.  However,
   your unit tests will be failing initially.
6. Incrementally implement the functionality of your
   `swapListValues()` function.  You should try to add no more than 2
   or 3 lines of code, and then make sure your program still compiles
   and runs.  Start by adding code to get the first failing test to
   pass.  Then once that test passes, move on to the next failing
   tests until you have all tests passing.  If you write something
   that causes a previously passing test to fail, you should stop and
   figure out why, and either fix it so that the original test still
   passes, or remove what you did and try a new approach.
7. Once you have the `swapListValues()` function implemented and all
   unit tests passing, you should then move on to the other functions
   in the order suggested.  Some member functions use previous ones in
   this assignment, so do them in the order given for you in the tasks
   below.

# Tasks

You should set up your project/code as described in the previous
section.  In this section we give some more details on implementing
the member functions for this assignment.  You should perform the following
tasks for this assignment:

1. Write a function called `swapListValues()`.  This functions takes
   an array of integers as its first parameter, and two indexes (the
   left and right indexes).  This function does not return a value
   explicitly (it is a void function).  Recall arrays are passed by
   reference.  As the name implies, the two values in the array at the
   indicated left and right indexes are to be swapped, and since the
   array is passed by reference, after returning they will be swapped
   for the caller of this function.
2. Write a function called `findAndSwapPivot()`.  This function
   takes the same 3 parameters, an array of integers, and two indexes
   indicating the left and right sides of a sub-portion of the
   list.  The function should find the value in the middle of the
   left and right ends, which will be chosen as the pivot.  The
   function should use the previous `swapListValues()` function
   to swap the chosen pivot value to the end of the list
   of integers.  This function returns a value.  This is different
   from how the textbook implements the find pivot function.  Our
   function should return the actual pivot value that was chosen
   (not the pivotIndex, which we know should be the last index
   of the sub-list after calling this function).
3. Write a function called `partitionList()`.  This will implement the
   algorithm described preciously.  This functions takes the 3 same
   parameters for the previous functions, an integer array, and left
   and right indexes for the sub-portion of the list we are currently
   partitioning.  In addition, this function takes a fourth parameter,
   the pivot value).  This function should make use of the
   `swapListValues()` function defined previously when swapping values
   in-place in the list of integers.  When this function is called,
   the pivot has been swapped to the end of the sub-portion of the
   list, so the right index will be one less than this.  This function
   needs to correctly return the index, described as `k` above, where
   the pivot value should actually go.  At the end, the location where
   the left search and right search meet will be this index, the final
   location found for the pivot value.
4. Finally write a function called `quickSort()` using the described
   algorithm above.  This function will use all of the 3 previous
   functions to do its work.  If implemented correctly, there is
   almost nothing to be done in this function besides calling the
   other 3 functions, and recursively calling itself (except to check
   for the base case of your recursion).

# Example Output
Here is the correct output you should get from your program
if you correctly implement all the class functions and successfully
pass all of the unit tests given for this assignment.  If you
invoke your function with no command line arguments, only failing
tests are usually shown by default.  In the second example, we use the
-s command line option to have the unit test framework show both
successful and failing tests, and thus we get reports of all of the
successfully passing tests as well on the output.


```
$ ./test
===============================================================================
All tests passed (50 assertions in 5 test cases)



$ ./test -s

-------------------------------------------------------------------------------
test is a Catch v2.7.2 host application.
Run with -? for options

-------------------------------------------------------------------------------
<swapListValues()> test swapping in list
-------------------------------------------------------------------------------
assg06-tests.cpp:65
...............................................................................

assg06-tests.cpp:75: PASSED:
  CHECK( tostring(test1, length) == tostring(expt1, length) )
with expansion:
  "List length: 2 [10, 5]"
  ==
  "List length: 2 [10, 5]"

... output snipped ...

===============================================================================
All tests passed (50 assertions in 5 test cases)
```

# Assignment Submission

A MyLeoOnline submission folder has been created for this assignment.
There is a target named `submit` that will create a tared and gziped
file named `assg02.tar.gz`. You should do a `make submit` when finished
and upload your resulting gzip file to the MyLeoOnline Submission
folder for this assignment.

```
$ make submit
tar cvfz assg06.tar.gz assg06-tests.cpp assg06-main.cpp
  QuickSort.hpp QuickSort.cpp
assg06-tests.cpp
assg06-main.cpp
QuickSort.hpp
QuickSort.cpp
```

# Requirements and Grading Rubrics

## Program Execution, Output and Functional Requirements

1. Your program must compile, run and produce some sort of output to
   be graded.  0 if not satisfied.
1. (15 pts.) `swapListValues()` function implemented as specified and
   working.
1. (15 pts.) `findAndSwapPivot()` function implemented as specified and
   working.
1. (30 pts.) `partitionList()` function implemented as specified and
   working.
1. (30 pts.) `quickSort()` function implemented as specified and working.
1. (5 pts.) All output is correct and matches the correct example output.
1. (5 pts.) Followed class style guidelines, especially those mentioned
   below.

## Program Style

Your programs must conform to the style and formatting guidelines
given for this class.  The following is a list of the guidelines that
are required for the assignment to be submitted this week.

1. Most importantly, make sure you figure out how to set your
   indentation settings correctly.  All programs must use 2 spaces for
   all indentation levels, and all indentation levels must be
   correctly indented.  Also all tabs must be removed from files, and
   only 2 spaces used for indentation.
1. A function header must be present for member functions you define.
   You must give a short description of the function, and document
   all of the input parameters to the function, as well as the return
   value and data type of the function if it returns a value for the
   member functions, just like for regular functions.  However, setter
   and getter methods do not require function headers.
1. Do not include any statements (such as `system("pause")` or
   inputting a key from the user to continue) that are meant to keep
   the terminal from going away.  Do not include any code that is
   specific to a single operating system, such as the
   `system("pause")` which is Microsoft Windows specific.

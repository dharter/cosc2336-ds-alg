/** @file assg06-main.cpp
 * @brief main/debug executable for Assignment 06 the Quick sort algorithm.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 06
 * @date   June 1, 2020
 *
 * Assignment 06 implementing the Quick sort algorithm. This file contains a
 * main() function we can use to build a debug target for debugging.
 */
#include <sstream>
#include <iostream>
#include <string>
#include "QuickSort.hpp"
using namespace std;


/** list to string
 * Represent array as a string time, useful for debugging output.
 * We could just write a function to test if 2 lists are equal, but by instead
 * converting lists to string and comparing the strings, we get nice output
 * in our unit tests so we can see how lists differ if we have errors.
 *
 * @param list[] The list, an array of integers, to be converted to
 *   a string.
 * @param length The length of the list.
 *
 * @returns string Returns a string with a representation of the list
 *   state and it contents.
 */
string tostring(int list[], int length)
{
  ostringstream out;

  out << "List length: " << length << " [";

  // output first value, so we can remove , at end
  if (length >= 1)
  {
    out << list[0];
  }

  // output each follow with a preceeding comma,
  // which allows us to end list without trailing ,
  for (int index = 1; index < length; index++)
  {
    out << ", " << list[index];
  }

  out << "]";

  return out.str();
}


/** main
 * The main entry point for this program.  Execution of this program
 * will begin with this main function.
 *
 * @param argc The command line argument count which is the number of
 *     command line arguments provided by user when they started
 *     the program.
 * @param argv The command line arguments, an array of character
 *     arrays.
 *
 * @returns An int value indicating program exit status.  Usually 0
 *     is returned to indicate normal exit and a non-zero value
 *     is returned to indicate an error condition.
 */
int main(int argc, char** argv)
{
  // example of invoking some of the functions to debug them
  int length = 12;
  int list[] = {4, 7, 9, 3, 8, 2, 2, 9, 5, 8, 6, 1};
  //swapListValues(list, 0, length-1);
  cout << "After swapping index 0 and 11" << endl;
  cout << tostring(list, length) << endl;

  //quickSort(list, 0, length-1);
  cout << "After calling quickSort on whole list" << endl;
  cout << tostring(list, length) << endl;


  // return 0 to indicate successful completion of program
  return 0;
}
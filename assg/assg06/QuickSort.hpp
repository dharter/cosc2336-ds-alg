/** @file QuickSort.hpp
 * @brief Header file for Assignment 06 the Quick sort algorithm.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 06
 * @date   June 1, 2020
 *
 * Assignment 06 implementing the Quick sort algorithm. This header file should
 * contian the function prototypes of the functions you implement for this
 * assignment.
 */
 #ifndef _QUICKSORT_H_
 #define _QUICKSORT_H_


// function prototypes go here


 #endif // _QUICKSORT_H_
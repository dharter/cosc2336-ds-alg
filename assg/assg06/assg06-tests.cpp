/** @file assg06-tests.cpp
 * @brief Unit tests for Assignment 06, the Quick sort algorithm.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 06
 * @date   June 1, 2020
 *
 * Assignment 06 implementing the Quick sort algorithm. This file has catch2
 * unit tests you need to test and implement the functions for your assignment.
 */
#include <iostream>
#include <sstream>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "QuickSort.hpp"
using namespace std;


/** list to string
 * Represent array as a string time, useful for debugging output.
 * We could just write a function to test if 2 lists are equal, but by instead
 * converting lists to string and comparing the strings, we get nice output
 * in our unit tests so we can see how lists differ if we have errors.
 *
 * @param list[] The list, an array of integers, to be converted to
 *   a string.
 * @param length The length of the list.
 *
 * @returns string Returns a string with a representation of the list
 *   state and it contents.
 */
string tostring(int list[], int length)
{
  ostringstream out;

  out << "List length: " << length << " [";

  // output first value, so we can remove , at end
  if (length >= 1)
  {
    out << list[0];
  }

  // output each follow with a preceeding comma,
  // which allows us to end list without trailing ,
  for (int index = 1; index < length; index++)
  {
    out << ", " << list[index];
  }

  out << "]";

  return out.str();
}


/** test swapListValues() function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the swapListValues() function.
   TEST_CASE("<swapListValues()> test swapping in list",
          "[swapListValues]")
   {
   int length;

   // basic test
   length = 2;
   int test1[] = {5, 10};
   int expt1[] = {10, 5};
   swapListValues(test1, 0, 1);
   CHECK( tostring(test1, length) == tostring(expt1, length) );

   // test if index is equal, should not cause a change in list but is valid
   length = 2;
   int test2[] = {8, 6};
   int expt2[] = {8, 6};
   swapListValues(test2, 0, 0);
   CHECK( tostring(test2, length) == tostring(expt2, length) );

   swapListValues(test2, 1, 1);
   CHECK( tostring(test2, length) == tostring(expt2, length) );

   // more general cases, swapping in middle of list
   length = 12;
   int test3[] = {2, 7, 9, 3, 8, 4, 2, 9, 5, 8, 6, 1};
   int expt3[] = {2, 7, 6, 3, 8, 4, 2, 9, 5, 8, 9, 1};
   swapListValues(test3, 2, 10);
   CHECK( tostring(test3, length) == tostring(expt3, length) );

   // swap them back
   int expt4[] = {2, 7, 9, 3, 8, 4, 2, 9, 5, 8, 6, 1};
   swapListValues(test3, 10, 2);
   CHECK( tostring(test3, length) == tostring(expt4, length) );

   // swap with index 0 on a bigger list
   int expt5[] = {4, 7, 9, 3, 8, 2, 2, 9, 5, 8, 6, 1};
   swapListValues(test3, 5, 0);
   CHECK( tostring(test3, length) == tostring(expt5, length) );

   // swap with last index on a bigger list
   int expt6[] = {4, 7, 9, 3, 8, 2, 1, 9, 5, 8, 6, 2};
   swapListValues(test3, 6, 11);
   CHECK( tostring(test3, length) == tostring(expt6, length) );

   // swap first and last indexes on the bigger list
   int expt7[] = {2, 7, 9, 3, 8, 2, 1, 9, 5, 8, 6, 4};
   swapListValues(test3, 11, 0);
   CHECK( tostring(test3, length) == tostring(expt7, length) );
   }
 */


/** test findAndSwapPivot() function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the findAndSwapPivot() function.

   TEST_CASE("<findAndSwapPivot()> test pivot selection",
          "[findAndSwapPivot]")
   {
   int length;
   int actualPivotValue;

   // basic test on a small list
   length = 3;
   int test1[] = {5, 3, 8};
   int expt1[] = {5, 8, 3};
   actualPivotValue = findAndSwapPivot(test1, 0, length-1);
   CHECK( tostring(test1, length) == tostring(expt1, length) );
   CHECK( actualPivotValue == 3 );

   // test on list of length 1, should work nothing will be done
   length = 1;
   int test2[] = {5};
   int expt2[] = {5};
   actualPivotValue = findAndSwapPivot(test2, 0, length-1);
   CHECK( tostring(test2, length) == tostring(expt2, length) );
   CHECK( actualPivotValue == 5 );

   // test on a bigger list, even number of values, should select
   // pivot at index 4
   length = 10;
   int test3[] = {5, 2, 8, 7, 3, 9, 1, 4, 5, 8};
   int expt3[] = {5, 2, 8, 7, 8, 9, 1, 4, 5, 3};
   actualPivotValue = findAndSwapPivot(test3, 0, length-1);
   CHECK( tostring(test3, length) == tostring(expt3, length) );
   CHECK( actualPivotValue == 3 );

   // test on a bigger list, od number of values, should select
   // pivot at index 7
   length = 15;
   int test4[] = {5, 2, 8, 7, 3, 9, 1, 42, 5, 8, 10, 11, 15, 22, 18};
   int expt4[] = {5, 2, 8, 7, 3, 9, 1, 18, 5, 8, 10, 11, 15, 22, 42};
   actualPivotValue = findAndSwapPivot(test4, 0, length-1);
   CHECK( tostring(test4, length) == tostring(expt4, length) );
   CHECK( actualPivotValue == 42 );
   }
 */


/** test partitionList() function.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the partitionList() function.
   TEST_CASE("<partitionList()> test list partitioning",
          "[partitionList]")
   {
   int length;
   int actualPivotIndex;

   // work from more general to more specific stress tests in this case
   // most general test, partition list approximately in middle.
   // NOTE: in all of these tests the pivot is assumed to be the value at
   // the end of the list (findAndSwapPivot() was already run), so normally
   // valid indexes for list of size n are from 0 to n-1, but we are Partitioning
   // on the list now from 0 to n-2 with the pivot value at index n-1
   length = 10;
   int test1[] = {8, 3, 7, 6, 9, 2, 4, 8, 1, 5};
   int expt1[] = {1, 3, 4, 2, 9, 6, 7, 8, 8, 5};
   actualPivotIndex = partitionList(test1, 0, length-2, test1[length-1]);
   CHECK( tostring(test1, length) == tostring(expt1, length) );
   CHECK( actualPivotIndex == 4 );

   // another general test, also we have several values equal to pivot
   // in this test, so check correctly testing for < or <= when determining
   // which values to swap
   length = 15;
   int test2[] = {12, 7, 10, 19, 6, 18, 12, 15, 6, 10, 4, 13, 11, 17, 10};
   int expt2[] = {4, 7, 6, 6, 19, 18, 12, 15, 10, 10, 12, 13, 11, 17, 10};
   actualPivotIndex = partitionList(test2, 0, length-2, test2[length-1]);
   CHECK( tostring(test2, length) == tostring(expt2, length) );
   CHECK( actualPivotIndex == 4 );

   // test when everything is to the left of pivot
   length = 6;
   int test3[] = {4, 3, 7, 6, 5, 10};
   int expt3[] = {4, 3, 7, 6, 5, 10};
   actualPivotIndex = partitionList(test3, 0, length-2, test3[length-1]);
   CHECK( tostring(test3, length) == tostring(expt3, length) );
   CHECK( actualPivotIndex == 5 );

   // test when everything is to the right of the pivot
   length = 6;
   int test4[] = {4, 3, 7, 6, 5, 2};
   int expt4[] = {4, 3, 7, 6, 5, 2};
   actualPivotIndex = partitionList(test4, 0, length-2, test4[length-1]);
   CHECK( tostring(test4, length) == tostring(expt4, length) );
   CHECK( actualPivotIndex == 0 );

   // test small list that needs to be swapped
   length = 3;
   int test5[] = {8, 2, 4};
   int expt5[] = {2, 8, 4};
   actualPivotIndex = partitionList(test5, 0, length-2, test5[length-1]);
   CHECK( tostring(test5, length) == tostring(expt5, length) );
   CHECK( actualPivotIndex == 1 );

   // list of only 1 item (besides pivot value), still needs to work
   length = 2;
   int test6[] = {8, 4};
   int expt6[] = {8, 4};
   actualPivotIndex = partitionList(test6, 0, length-2, test6[length-1]);
   CHECK( tostring(test6, length) == tostring(expt6, length) );
   CHECK( actualPivotIndex == 0 );

   // list of only 1 item, but the pivot is bigger.  Notice the difference,
   // the returned pivot index needs to be index of first value >= to pivot
   length = 2;
   int test7[] = {8, 12};
   int expt7[] = {8, 12};
   actualPivotIndex = partitionList(test7, 0, length-2, test7[length-1]);
   CHECK( tostring(test7, length) == tostring(expt7, length) );
   CHECK( actualPivotIndex == 1 );
   }
 */


/** test sort by hand.  This is an example where we are beginning to do some
 * more system level testing.  We test using the 3 functions we have so far
 * to sort a small list by hand.  We repeatedily find pivot and partition on
 * sublists until we are finished sorting the original list.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of using swapListValues(), findAndSwapPartition() and parritionList()
 * together
   TEST_CASE("<sort by hand> test 3 functions sorting by hand",
          "[sort by hand]")
   {
   // sort an array of size 5
   int list[] = {7, 8, 6, 5, 3};
   int length = 5;
   int left = 0;
   int right = length - 1;

   // first pivot vlaue will be at index 2 (value 6)
   int pivotValue;
   pivotValue = findAndSwapPivot(list, left, right);
   CHECK( pivotValue == 6 );

   // we expect first pivotIndex should be 2, and 6 should be at correct index 2
   // after swap to the partition index we found
   int pivotIndex;
   pivotIndex = partitionList(list, left, right-1,  pivotValue);
   CHECK( pivotIndex == 2 );
   swapListValues(list, pivotIndex, right);
   CHECK( list[pivotIndex] == 6 );

   // now partition left sublist, we expect 3 as pivot value, and after we swap
   // the left list will be sorted from index 0 to 1 (index 2 already in place
   // as well)
   right = pivotIndex - 1;
   pivotValue = findAndSwapPivot(list, left, right);
   CHECK( pivotValue == 5 );

   pivotIndex = partitionList(list, left, right-1, pivotValue);
   CHECK( pivotIndex == 1 );
   swapListValues(list, pivotIndex, right);
   CHECK( list[pivotIndex] == 5 );
   CHECK( list[left] == 3 );

   // now partition right sublist (from indexes 3 to 4).  After this the right
   // sublist will be sorted
   left = 3;
   right = 4;
   pivotValue = findAndSwapPivot(list, left, right);
   CHECK( pivotValue == 7 );

   pivotIndex = partitionList(list, left, right-1, pivotValue);
   CHECK( pivotIndex == 3 );
   swapListValues(list, pivotIndex, right);
   CHECK( list[pivotIndex] == 7 );
   CHECK( list[right] == 8 );

   // we expect the list to be fully sorted in the end
   int expt[] = {3, 5, 6, 7, 8};
   CHECK( tostring(list, length) == tostring(expt, length) );
   }
 */


/** test quickSort() function
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the full quickSort() function which should use your 3 previous functions
 * recursively to sort a list of integer values.
   TEST_CASE("<quickSort()> test quickSort() implementation",
          "[quickSort]")
   {
   int length;

   // sort a list of length 1, should work without crashing
   length = 1;
   int test1[] = {8};
   int expt1[] = {8};
   quickSort(test1, 0, length-1);
   CHECK( tostring(test1, length) == tostring(expt1, length) );

   // sort a list of length 2 that is already sorted
   length = 2;
   int test2[] = {3, 9};
   int expt2[] = {3, 9};
   quickSort(test2, 0, length-1);
   CHECK( tostring(test2, length) == tostring(expt2, length) );

   // sort a list of length 2 that is not sorted
   length = 2;
   int test3[] = {12, 6};
   int expt3[] = {6, 12};
   quickSort(test3, 0, length-1);
   CHECK( tostring(test3, length) == tostring(expt3, length) );

   // sort an odd sized list
   length = 9;
   int test4[] = {9, 3, 2, 7, 5, 8, 6, 5, 4};
   int expt4[] = {2, 3, 4, 5, 5, 6, 7, 8, 9};
   quickSort(test4, 0, length-1);
   CHECK( tostring(test4, length) == tostring(expt4, length) );

   // sort an even sized list, also duplicate values in the list
   length = 14;
   int test5[] = {9, 3, 2, 7, 5, 8, 6, 5, 4, 2, 10, 8, 11, 5};
   int expt5[] = {2, 2, 3, 4, 5, 5, 5, 6, 7, 8, 8, 9, 10, 11};
   quickSort(test5, 0, length-1);
   CHECK( tostring(test5, length) == tostring(expt5, length) );

   // sort an already sorted lists
   length = 17;
   int test6[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};
   int expt6[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};
   quickSort(test6, 0, length-1);
   CHECK( tostring(test6, length) == tostring(expt6, length) );

   // sort a reversed list
   length = 17;
   int test7[] = {17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
   int expt7[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};
   quickSort(test7, 0, length-1);
   CHECK( tostring(test7, length) == tostring(expt7, length) );

   // sort a list with lots of duplicates
   length = 14;
   int test8[] = {4, 8, 4, 8, 4, 8, 4, 8, 4, 8, 4, 8, 4, 8};
   int expt8[] = {4, 4, 4, 4, 4, 4, 4, 8, 8, 8, 8, 8, 8, 8};
   quickSort(test8, 0, length-1);
   CHECK( tostring(test8, length) == tostring(expt8, length) );
   }
 */
/** @file ex01-main.cpp
 * @brief main/debug executable for Example 01, practice on writing functions.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 02
 * @date   June 1, 2020
 *
 * Example Assignment, practice on writing functions.  This is a stub for a
 * separate main() function that can be used to build a debug executable if
 * needed.
 */
#include <cmath>
#include <iomanip>
#include <iostream>
#include "ex01-functions.hpp"
using namespace std;


/** main entry point
 * Main entry point for debugging functions.
 *
 * @param argc The command line argument count, the number of arguments
 *   provided by user on the command line.
 * @param argv An array of char* old style c-strings.  Each argv[x]
 *   that is passed in holds one of the command line arguments provided
 *   by the user to the program when started.
 *
 * @returns int Returns 0 to indicate successfull completion of program,
 *   and a non-zero value to indicate an error code.
 */
int main(int argc, char** argv)
{
  bool res;
  res = isPrime(15);

  cout << "Result is: " << boolalpha << res << endl;

  // return 0 to indicate successful completion
  return 0;
}
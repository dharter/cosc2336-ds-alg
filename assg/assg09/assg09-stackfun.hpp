/** @file assg09-stackfun.hpp
 * @brief header file for function prototypes for Assignment 09 using stacks.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 09
 * @date   June 1, 2020
 *
 * Assignment 09 using stacks.  Use the given Stack ADT to implement a set of
 * functions/algorithms. This header file is for the function prototypes you are
 * to write for this assignment.  You should modify this file by adding
 * prototypes/signatures of your 4 functions where indicated below.
 */
#include <string>
#include "Stack.hpp" // only use the Stack ADT given, do not use STL stacks
using namespace std;


#ifndef _ASSG09_STACKFUN_H_
#define _ASSG09_STACKFUN_H_

// put your function prototypes for the assignment here


// because you are defining template functions, we have to treat
// this header/library as a template library, so we include
// the implementation file, and we do not separately compile
// these files in the build
#include "assg09-stackfun.cpp"


#endif // _ASSG09_STACKFUN_H_
/** @file assg09-stackfun.cpp
 * @brief implementation file for function prototypes for
 *   Assignment 09 using stacks.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 09
 * @date   June 1, 2020
 *
 * Assignment 09 using stacks.  Use the given Stack ADT to implement a set of
 * functions/algorithms. This implementation file is for the functions you are
 * to write for this assignment.  You should modify this file by adding
 * implementations of the 4 functions you are to write.  You need to use the
 * Stack ADT given to you, and included for you, for the Stack instances for
 * your functions.  It is incorrect to include STL stacks or other containers
 * here to use for this assignment.
 */
#include "Stack.hpp"  // only use the Stack ADT given, do not use STL stacks
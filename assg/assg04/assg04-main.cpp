/** @file assg04-main.cpp
 * @brief main/debug executable for Assignment 04, practice defining
 *   recursive functions.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 04
 * @date   June 1, 2020
 *
 * Assignment 04 Practice defining recursive functions.  We implement factorial
 * and counting combinations functions using the binomial coefficient in this
 * assignment.  This file contains a main() function we can use to build a debug
 * target for debugging.
 */
#include <cmath>
#include <iostream>
#include <string>
#include "BinomialFunctions.hpp"
using namespace std;


/** main
 * The main entry point for this program.  Execution of this program
 * will begin with this main function.
 *
 * @param argc The command line argument count which is the number of
 *     command line arguments provided by user when they started
 *     the program.
 * @param argv The command line arguments, an array of character
 *     arrays.
 *
 * @returns An int value indicating program exit status.  Usually 0
 *     is returned to indicate normal exit and a non-zero value
 *     is returned to indicate an error condition.
 */
int main(int argc, char** argv)
{

  // example call factorialIterative
  bigint res = 0;

  //res = factorialIterative(18);
  cout << "18! = " << res << endl;

  // example call countCombinationsRecursive
  //res = countCombinationsRecursive(6, 3);
  cout << "(6 choose 3) = " << res << endl;

  // return 0 to indicate successful completion of program
  return 0;
}
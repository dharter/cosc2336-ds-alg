/** @file BinomialFunctions.cpp
 * @brief Implementation file for Assignment 04 Binomial Coefficient functions
 *   using recursion and iteration.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 04
 * @date   June 1, 2020
 *
 * Implementation file for the Binomial Coefficient functions. We have functions
 * for computing the factorial of an integer and for counting the number of
 * combinations of n choose i items in this library. Your implementations of the
 * functions for the assignment should go here. Do not forget to document your
 * functions.
 */
#include "BinomialFunctions.hpp"

using namespace std;


// your function implementations go here
// don't forget to document your functions with block comments
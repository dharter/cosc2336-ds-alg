/** @file assg04-tests.cpp
 * @brief Unit tests for Assignment 04, practice defining recursive functions.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 04
 * @date   June 1, 2020
 *
 * Assignment 04 Practice defining recursive functions.  We implement factorial
 * and counting combinations functions using the binomial coefficient in this
 * assignment.  This file has catch2 unit tests you need to test and implement
 * the functions for your assignment.
 */
#include <iostream>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "BinomialFunctions.hpp"
using namespace std;


/** test factorialIterative() function.  These tests cannot test how
 * you implement the function.  So the tests for iterative and recursive
 * versions have the same set of tests.  However I will of course look at your
 * code and remove points if you don't implement the functions using recursion
 * or iteration as asked for in each one.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the factorialIterative() function.
   TEST_CASE("<factorialIterative()> iterative factorial implementation",
          "[factorialIterative]")
   {
   // base case, 0! should be 1
   CHECK( factorialIterative(0) == 1 );

   // possible edge case, 1! should also be 1
   CHECK( factorialIterative(1) == 1 );

   // test all general cases up to factorial 10
   CHECK( factorialIterative(2) == 2 * 1 );
   CHECK( factorialIterative(3) == 3 * 2 * 1 );
   CHECK( factorialIterative(4) == 4 * 3 * 2 * 1 );
   CHECK( factorialIterative(5) == 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialIterative(6) == 6 * 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialIterative(7) == 7 * 6 * 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialIterative(8) == 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialIterative(9) == 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialIterative(10) == 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1 );

   // test correctly using the bigint in calculations (unsigned long int)
   CHECK( factorialIterative(20) == 2432902008176640000 );
   }
 */


/** test factorialRecursive() function.  These tests cannot test how
 * you implement the factorial function.  So the tests for iterative
 * and recursive versions have the same set of tests.  However I will of
 * course look at your code and remove points if you don't implement the
 * functions using recursion or iteration as asked for in each one.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the factorialRecursive() function.
   TEST_CASE("<factorialRecursive()> iterative factorial implementation",
          "[factorialRecursive]")
   {
   // base case, 0! should be 1
   CHECK( factorialRecursive(0) == 1 );

   // possible edge case, 1! should also be 1
   CHECK( factorialRecursive(1) == 1 );

   // test all general cases up to factorial 10
   CHECK( factorialRecursive(2) == 2 * 1 );
   CHECK( factorialRecursive(3) == 3 * 2 * 1 );
   CHECK( factorialRecursive(4) == 4 * 3 * 2 * 1 );
   CHECK( factorialRecursive(5) == 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialRecursive(6) == 6 * 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialRecursive(7) == 7 * 6 * 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialRecursive(8) == 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialRecursive(9) == 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1 );
   CHECK( factorialRecursive(10) == 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1 );

   // test correctly using the bigint in calculations (unsigned long int)
   CHECK( factorialRecursive(20) == 2432902008176640000 );
   }
 */


/** test countCombinationsDirectly() function.  These tests cannot test how
 * you implement the binomial function.  So the tests for direct
 * and recursive versions have the same set of tests.  However I will of
 * course look at your code and remove points if you don't implement the
 * functions using recursion or directly using the binomial formula
 * as asked for in each one.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the countCombinationsDirectly() function.
   TEST_CASE("<countCombinationsDirectly()> direct binomial formula implementation",
          "[countCombinationsDirectly]")
   {
   // test base cases, choose 0 and choose n
   CHECK( countCombinationsDirectly(5, 0) == 1 );
   CHECK( countCombinationsDirectly(10, 0) == 1 );
   CHECK( countCombinationsDirectly(8, 8) == 1 );
   CHECK( countCombinationsDirectly(4, 4) == 1 );

   // edge case, what if there are 0 items?
   CHECK( countCombinationsDirectly(0, 0) == 1 );

   // edge case, 1 items
   CHECK( countCombinationsDirectly(1, 0) == 1 );
   CHECK( countCombinationsDirectly(1, 1) == 1 );

   // test all cases of choosing from 6 items
   CHECK( countCombinationsDirectly(6, 0) == 1 );
   CHECK( countCombinationsDirectly(6, 1) == 6 );
   CHECK( countCombinationsDirectly(6, 2) == 15 );
   CHECK( countCombinationsDirectly(6, 3) == 20 );
   CHECK( countCombinationsDirectly(6, 4) == 15 );
   CHECK( countCombinationsDirectly(6, 5) == 6 );
   CHECK( countCombinationsDirectly(6, 6) == 1 );

   // some other bigger general cases
   CHECK( countCombinationsDirectly(15, 6) == 5005 );
   CHECK( countCombinationsDirectly(15, 14) == 15 );
   CHECK( countCombinationsDirectly(20, 10) == 184756 );
   }
 */


/** test countCombinationsRecursive() function.  These tests cannot test how
 * you implement the binomial function.  So the tests for direct
 * and recursive versions have the same set of tests.  However I will of
 * course look at your code and remove points if you don't implement the
 * functions using recursion or directly using the binomial formula
 * as asked for in each one.
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the countCombinationsDirectly() function.
   TEST_CASE("<countCombinationsRecursive()> recursive binomial function implementation",
          "[countCombinationsRecursive]")
   {
   // test base cases, choose 0 and choose n
   CHECK( countCombinationsRecursive(5, 0) == 1 );
   CHECK( countCombinationsRecursive(10, 0) == 1 );
   CHECK( countCombinationsRecursive(8, 8) == 1 );
   CHECK( countCombinationsRecursive(4, 4) == 1 );

   // edge case, what if there are 0 items?
   CHECK( countCombinationsRecursive(0, 0) == 1 );

   // edge case, 1 items
   CHECK( countCombinationsRecursive(1, 0) == 1 );
   CHECK( countCombinationsRecursive(1, 1) == 1 );

   // test all cases of choosing from 6 items
   CHECK( countCombinationsRecursive(6, 0) == 1 );
   CHECK( countCombinationsRecursive(6, 1) == 6 );
   CHECK( countCombinationsRecursive(6, 2) == 15 );
   CHECK( countCombinationsRecursive(6, 3) == 20 );
   CHECK( countCombinationsRecursive(6, 4) == 15 );
   CHECK( countCombinationsRecursive(6, 5) == 6 );
   CHECK( countCombinationsRecursive(6, 6) == 1 );

   // some other bigger general cases
   CHECK( countCombinationsRecursive(15, 6) == 5005 );
   CHECK( countCombinationsRecursive(15, 14) == 15 );
   CHECK( countCombinationsRecursive(20, 10) == 184756 );
   }
 */
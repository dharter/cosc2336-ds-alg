/** @file BinomialFunctions.hpp
 * @brief Header file for Assignment 04 implementing the Binomial
 *   Coefficient functions using recursion and iteration.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 04
 * @date   June 1, 2020
 *
 * Header file of definitions for implementing the Binomial Coefficient
 * functions.  We have functions for computing the factorial of an integer and
 * for counting the number of combinations of n choose i items in this library.
 * You should pur the function prototypes of the functions you implement in this
 * file.
 */
#ifndef _BINOMIALFUNCTIONS_H_
#define _BINOMIALFUNCTIONS_H_

#include <iostream>

using namespace std;

// global definitions
typedef unsigned long long int bigint;  // give unsigned long int an alias name of bigint

// Function prototypes for our BinomialFunctions library go here

#endif // _BINOMIALFUNCTIONS_H_
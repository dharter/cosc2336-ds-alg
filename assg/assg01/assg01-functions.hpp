/** @file assg01-functions.hpp
 * @brief Header file for Assignment 01 practice on functions.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 01
 * @date   June 1, 2020
 *
 * Assignment 01, practice on functions, user defined types and arrays.  This is
 * the header file for the assignment 01 functions.  The function prototypes for
 * the functions you create should be defined in this file.
 */


// function prototypes for assignment 01
// you should put in the function prototypes for the two functions
// you implement in the .cpp file here so they can be included where needed
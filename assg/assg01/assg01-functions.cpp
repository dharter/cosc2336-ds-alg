/** @file assg01-functions.cpp
 * @brief Implementation file for Assignment 01 practice on functions.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 01
 * @date   June 1, 2020
 *
 * Assignment 01, practice on functions, user defined types and arrays.  This is
 * the file where you should implement the functions calculateMean() and
 * calculateStandardDeviation() as described in the assignment.
 */
#include <iostream>
#include <cmath>
using namespace std;


/** calculateMean
 * Calculate the mean (or average) of the given list of values.  This
 * functions works on a list of integers, but since the mean can be a
 * real valued number, it returns a double result.  The values are
 * passed in as an array, along with the number of values in the array
 * as parameters.
 *
 * @param n The number of values being input to the
 *   function.
 * @param x An array of integers, the values to calculate the
 *   mean of.  This is a const parameter because we will not
 *   change the original values in any way, only calculate their
 *   mean.
 *
 * @returns A double result, the calculated mean or average of the
 *   list of values.
 */
// write your implementation of the calculateMean() function here


/** calculateStandardDeviation
 * Calculate the standard deviation of the given list of values.
 * This function works on a list of integers, but since the
 * standard deviation can be a real valued number, it returns
 * a double result.  The values are passed in as an array, along
 * with the number of values in the array as parameters.
 *
 * @param n The number of values being input to the
 *   function.
 * @param x An array of integers, the values to calculate the
 *   standard deviation of.  This is a const parameter because we will
 *   not change the original values in any way, only calculate their
 *   mean.
 *
 * @returns A double result, the calculated mean or average of the
 *   list of values.
 */
// write your implementation of the calculateStandardDeviation() function here
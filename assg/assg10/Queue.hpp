/** @file Queue.hpp
 * @brief Header file containing a Queue class for Assignment 10
 *   queues and priority queues.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 10
 * @date   June 1, 2020
 *
 * Assignment 10 queues and priority queues.  Use the given Queue ADT to create
 * a new PriorityQueue ADT, then use queues and your priority queue to create a
 * random job processing simulation. This header file contains the declaration
 * of the Queue class container that defines the ADT abstraction for queues to
 * use for this assignment.
 */
#include <iostream>
#include <string>
#include <sstream>
using namespace std;


#ifndef _QUEUE_H_
#define _QUEUE_H_


//-------------------------------------------------------------------------
/** queue (base class)
 * The basic definition of the Queue Abstract Data Type (ADT)
 * and queue operations.  All declared functions here are
 * virtual, they must be implemented by concrete derived
 * classes.
 */
template <class T>
class Queue
{
public:
  /** clear
   * Method to clear out or empty any items on queue,
   * put queue back to empty state.
   * Postcondition: Queue is empty.
   *
   * @returns void Returns nothing
   */
  virtual void clear() = 0;

  /** isEmpty
   * Function to determine whether the queue is empty.  Needed
   * because it is undefined to remove from empty queue.  This
   * function will not change the state of the queue (const).
   *
   * @returns bool true if queue is empty, false otherwise.
   */
  virtual bool isEmpty() const = 0;

  /** enqueue
   * Add a new item onto back of queue.
   *
   * @param newItem The item of template type T to add on back of
   *   the current queue.
   *
   * @returns void Returns nothing
   */
  virtual void enqueue(const T& newItem) = 0;

  /** front
   * Return the front item from the queue.  Note in this ADT, peeking
   * at the front item does not remove the front item.  Some ADT combine
   * front() and dequeue() as one operation.  It is undefined to try and
   * peek at the front item of an empty queue.  Derived classes should
   * throw an exception if this is attempted.
   *
   * @returns T Returns the front item from queue.
   */
  virtual T front() const = 0;

  /** dequeue
   * Remove the item from the front of the queue.  It is undefined what
   * it means to try and dequeue from an empty queue.  Derived classes should
   * throw an exception if dequeue() from empty is attempted.
   *
   * @returns void Returns nothing
   */
  virtual void dequeue() = 0;

  /** length
   * Return the current length or number of item son the queue.
   *
   * @returns int The current length of this queue.
   */
  virtual int length() const = 0;

  /** tostring
   * Represent queue as a string
   *
   * @returns string Returns a string representation of the given queue
   */
  virtual string tostring() const = 0;


  // overload operators, mostly to support boolean comparison betwen
  // two queues for testing
  bool operator==(const Queue<T>& rhs) const;

  /** indexing operator
   * A virtual method to provide an indexing operator into the queue.
   * All subclasses must implement this method.  This is not a normal
   * queue method, we use it for debugging.
   *
   * @param index The integer index of the item to be accessed and returned
   *   from this queue.
   *
   * @returns T& Returns a reference to the indexed item on the queue.
   */
  virtual const T& operator[](int index) const = 0;

  // overload output stream operator for all queues using tostring()
  template <typename U>
  friend ostream& operator<<(ostream& out, const Queue<U>& aQueue);
};




//-------------------------------------------------------------------------
/** Empty queue exception
 * Class for empty queue exceptions
 */
class EmptyQueueException
{
private:
  /// @brief The error message associated with this empty queue exctption
  string message;

public:
  /** default constructor
   * The default constructor for an empty queue exception.  Construct a
   * generic error/exception message to be returned.
   */
  EmptyQueueException()
  {
    message = "Error: operation on empty queue";
  }


  /** string constructor
   * Alternate constructor for empty queue exception.  Use the given string
   * to construct an error message with more information about the exception.
   *
   * @param str A string with additional information about this exception.
   */
  EmptyQueueException(string str)
  {
    message = "Error: " + str + " attempted on emtpy queue";
  }


  /** what accessor
   * Access and return what the error message associated with this exception.
   *
   * @returns string Returns the error message associated with this exception.
   */
  string what()
  {
    return message;
  }


};


/** InvalidIndex queue exception
 * Class to be thrown when an invalid index is asked for when indexing
 * into a queue object.
 */
class InvalidIndexQueueException
{
private:
  /// @brief The error message associated with this invalid index queue exctption
  string message;

public:
  /** default constructor
   * The default constructor for an invalid index queue exception.  Construct a
   * generic error/exception message to be returned.
   */
  InvalidIndexQueueException()
  {
    message = "Error: invalid index request for queue";
  }


  /** string constructor
   * Alternate constructor for invalid index queue exception.  Use the given string
   * to construct an error message with more information about the exception.
   *
   * @param str A string with additional information about this exception.
   */
  InvalidIndexQueueException(string str)
  {
    message = "Error: " + str + " invalid index request for queue";
  }


  /** what accessor
   * Access and return what the error message associated with this exception.
   *
   * @returns string Returns the error message associated with this exception.
   */
  string what()
  {
    return message;
  }


};



//-------------------------------------------------------------------------
/** queue (array implementation)
 * Implementation of the queue ADT as a fixed array.  This
 * implementation combines a circular buffer implementation, to make
 * sure that both enqueue() and dequeue() operations are O(1) constant
 * time.  However, it also uses dynamic memory allocation, and
 * demonstrates doubling the size of the allocated space as needed to
 * grow queue if/when the queue becomes full.
 */
template <class T>
class AQueue : public Queue<T>
{
private:
  /// @brief The amount of memory currently allocated for this queue.
  int allocSize;
  /// @brief The current length or number of items on the queue.
  int numitems;
  /// @brief A pointer to the index of the front item on the queue.
  int frontIndex;
  /// @brief A pointer to the back or last item on the queu.
  int backIndex;
  /// @brief The items on the queue.  This is a dynamically allocated array
  ///   that can grow if needed when queue exceeds current allocation.
  T* items;

public:
  AQueue(int initialAlloc = 100); // constructor
  AQueue(int initItems[], int numItems);
  ~AQueue(); // destructor
  void clear();
  bool isEmpty() const;
  bool isFull() const;
  void enqueue(const T& newItem);
  T front() const;
  void dequeue();
  int length() const;
  string tostring() const;
  const T& operator[](int index) const;
};




//-------------------------------------------------------------------------
/** Node
 * A basic node contaning an item and a link to the next node in
 * the linked list.
 */
template <class T>
struct Node
{
  /// @brief The item held by this linked list node
  T item;
  /// @brief link A pointer to the next node in a linked list
  Node<T>* link;
};



//-------------------------------------------------------------------------
/** queue (linked list implementation)
 * Implementation of the queue ADT as a dynamic linked list.  This implementation
 * uses link nodes and grows (and shrinks) the nodes as items enqueued and dequeued
 * onto queue.
 */
template <class T>
class LQueue : public Queue<T>
{
protected:
  /// @brief A pointer to the front node of our linked list queue
  Node<T>* queueFront;
  /// @brief A pointer to the node holding the back item of the queue
  Node<T>* queueBack;
  /// @brief The length or number of items currently on the queue.
  int numitems; // the queue length

public:
  LQueue(); // default constructor
  ~LQueue(); // destructor
  void clear();
  bool isEmpty() const;
  void enqueue(const T& newItem);
  T front() const;
  void dequeue();
  int length() const;
  string tostring() const;
  const T& operator[](int index) const;
};


//-------------------------------------------------------------------------
// You need to add your PriorityQueue class declaration
// here.  Your PriorityQueue should be derived from (a child
// class of) the LQueue linked list queue class.  Your
// class will only have/override a single method, the
// enqueue() member function.


// include template implementations so they are included with header
#include "Queue.cpp"

#endif // _STACK_H_
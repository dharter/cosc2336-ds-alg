/** @file assg10-main.cpp
 * @brief main/debug executable for Assignment 10 queues and priority queues.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 10
 * @date   June 1, 2020
 *
 * Assignment 10 queues and priority queues.  Use the given Queue ADT to create
 * a new PriorityQueue ADT, then use queues and your priority queue to create a
 * random job processing simulation. This file contains a main() function we can
 * use to build a debug target for debugging.
 */
#include <iostream>
#include "Queue.hpp"
#include "JobSimulator.hpp"
using namespace std;


/** main
 * The main entry point for this program.  Execution of this program
 * will begin with this main function.
 *
 * @param argc The command line argument count which is the number of
 *     command line arguments provided by user when they started
 *     the program.
 * @param argv The command line arguments, an array of character
 *     arrays.
 *
 * @returns An int value indicating program exit status.  Usually 0
 *     is returned to indicate normal exit and a non-zero value
 *     is returned to indicate an error condition.
 */
int main(int argc, char** argv)
{
  // example of invoking some of the functions to debug them
  JobSchedulerSimulator sim;

  // seed random number generator so we can recreate sequence of jobs and
  // priorities that are randomly generated
  int seed = 32;
  srand(seed);

  // test with a non priority queue first
  LQueue<Job> jobQueue;
  //sim.runSimulation(jobQueue, "Normal (non-priority based) queueing discipline");
  cout << sim;


  // reseed with same seed, in theory we should end up with the same sequence
  // of random jobs, but just handled with a priorty queue now
  srand(seed);

  // test with a priority queue
  //PriorityQueue<Job> jobPriorityQueue;
  //sim.runSimulation(jobPriorityQueue, "Priority queueing discipline");
  cout << sim;


  // return 0 to indicate successful completion of program
  return 0;
}
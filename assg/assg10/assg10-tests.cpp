/** @file assg10-tests.cpp
 * @brief Unit tests for Assignment 10 queues and priority queues.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Summer 2020
 * @note   ide  : Atom Text Editor 1.46.0 / build package / GNU gcc tools
 * @note   assg : Assignment 10
 * @date   June 1, 2020
 *
 * Assignment 10 queues and priority queues.  Use the given Queue ADT to create
 * a new PriorityQueue ADT, then use queues and your priority queue to create a
 * random job processing simulation. This file has catch2 unit tests you need to
 * test and implement the functions for your assignment.
 */
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Queue.hpp"
#include "JobSimulator.hpp"
using namespace std;



/** test basic queue functions() functions.  Just some general tests
 * of the Queue implementation given to you, to make sure nothing
 * breaks while trying to add/implement new functionality to the Queue.
 */
TEST_CASE("<basic Queue> test basic Queue functionality",
          "[class]")
{
  //--------------------------------------------------------------------
  // use a linked list queue implementation of ints
  LQueue<int> ilq;

  // initial queue should be empty
  CHECK( ilq.isEmpty() );
  CHECK( ilq.length() == 0 );
  CHECK( ilq.tostring() == "Front: :Back\n" );
  CHECK_THROWS( ilq.front(), EmptyQueueException() );
  CHECK_THROWS( ilq.dequeue(), EmptyQueueException() );

  // add item to empty queue
  ilq.enqueue(5);
  CHECK_FALSE( ilq.isEmpty() );
  CHECK( ilq.length() == 1 );
  CHECK( ilq.tostring() == "Front: 5 :Back\n" );
  CHECK( ilq.front() == 5 );

  // add item to non empty queue
  ilq.enqueue(7);
  CHECK_FALSE( ilq.isEmpty() );
  CHECK( ilq.length() == 2 );
  CHECK( ilq.tostring() == "Front: 5 7 :Back\n" );
  CHECK( ilq.front() == 5 );

  // add another item to non empty queue
  ilq.enqueue(3);
  CHECK_FALSE( ilq.isEmpty() );
  CHECK( ilq.length() == 3 );
  CHECK( ilq.tostring() == "Front: 5 7 3 :Back\n" );
  CHECK( ilq.front() == 5 );

  // test dequeu
  ilq.dequeue();
  CHECK_FALSE( ilq.isEmpty() );
  CHECK( ilq.length() == 2 );
  CHECK( ilq.tostring() == "Front: 7 3 :Back\n" );
  CHECK( ilq.front() == 7 );

  ilq.dequeue();
  CHECK_FALSE( ilq.isEmpty() );
  CHECK( ilq.length() == 1 );
  CHECK( ilq.tostring() == "Front: 3 :Back\n" );
  CHECK( ilq.front() == 3 );

  // test dequeue to make queue become empty again
  ilq.dequeue();
  CHECK( ilq.isEmpty() );
  CHECK( ilq.length() == 0 );
  CHECK( ilq.tostring() == "Front: :Back\n" );
  CHECK_THROWS( ilq.front(), EmptyQueueException() );

  // test clear of queue
  ilq.enqueue(1);
  ilq.enqueue(2);
  CHECK_FALSE( ilq.isEmpty() );
  CHECK( ilq.length() == 2 );
  CHECK( ilq.tostring() == "Front: 1 2 :Back\n" );

  ilq.clear();
  CHECK( ilq.isEmpty() );
  CHECK( ilq.length() == 0 );
  CHECK( ilq.tostring() == "Front: :Back\n" );

  //--------------------------------------------------------------------
  // use a linked list queue implementation of strings
  LQueue<string> slq;

  // initial queue should be empty
  CHECK( slq.isEmpty() );
  CHECK( slq.length() == 0 );
  CHECK( slq.tostring() == "Front: :Back\n" );
  CHECK_THROWS( slq.front(), EmptyQueueException() );
  CHECK_THROWS( slq.dequeue(), EmptyQueueException() );

  // add item to empty queue
  slq.enqueue("echo");
  CHECK_FALSE( slq.isEmpty() );
  CHECK( slq.length() == 1 );
  CHECK( slq.tostring() == "Front: echo :Back\n" );
  CHECK( slq.front() == "echo" );

  // add item to non empty queue
  slq.enqueue("hotel");
  CHECK_FALSE( slq.isEmpty() );
  CHECK( slq.length() == 2 );
  CHECK( slq.tostring() == "Front: echo hotel :Back\n" );
  CHECK( slq.front() == "echo" );

  // add another item to non empty queue
  slq.enqueue("bravo");
  CHECK_FALSE( slq.isEmpty() );
  CHECK( slq.length() == 3 );
  CHECK( slq.tostring() == "Front: echo hotel bravo :Back\n" );
  CHECK( slq.front() == "echo" );

  // test dequeu
  slq.dequeue();
  CHECK_FALSE( slq.isEmpty() );
  CHECK( slq.length() == 2 );
  CHECK( slq.tostring() == "Front: hotel bravo :Back\n" );
  CHECK( slq.front() == "hotel" );

  slq.dequeue();
  CHECK_FALSE( slq.isEmpty() );
  CHECK( slq.length() == 1 );
  CHECK( slq.tostring() == "Front: bravo :Back\n" );
  CHECK( slq.front() == "bravo" );

  // test dequeue to make queue become empty again
  slq.dequeue();
  CHECK( slq.isEmpty() );
  CHECK( slq.length() == 0 );
  CHECK( slq.tostring() == "Front: :Back\n" );
  CHECK_THROWS( slq.front(), EmptyQueueException() );

  // test clear of queue
  slq.enqueue("alpha");
  slq.enqueue("bravo");
  CHECK_FALSE( slq.isEmpty() );
  CHECK( slq.length() == 2 );
  CHECK( slq.tostring() == "Front: alpha bravo :Back\n" );

  slq.clear();
  CHECK( slq.isEmpty() );
  CHECK( slq.length() == 0 );
  CHECK( slq.tostring() == "Front: :Back\n" );


  //--------------------------------------------------------------------
  // use an array queue implementation of ints
  AQueue<int> iaq;

  // initial queue should be empty
  CHECK( iaq.isEmpty() );
  CHECK( iaq.length() == 0 );
  CHECK( iaq.tostring() == "Front: :Back\n" );
  CHECK_THROWS( iaq.front(), EmptyQueueException() );
  CHECK_THROWS( iaq.dequeue(), EmptyQueueException() );

  // add item to empty queue
  iaq.enqueue(5);
  CHECK_FALSE( iaq.isEmpty() );
  CHECK( iaq.length() == 1 );
  CHECK( iaq.tostring() == "Front: 5 :Back\n" );
  CHECK( iaq.front() == 5 );

  // add item to non empty queue
  iaq.enqueue(7);
  CHECK_FALSE( iaq.isEmpty() );
  CHECK( iaq.length() == 2 );
  CHECK( iaq.tostring() == "Front: 5 7 :Back\n" );
  CHECK( iaq.front() == 5 );

  // add another item to non empty queue
  iaq.enqueue(3);
  CHECK_FALSE( iaq.isEmpty() );
  CHECK( iaq.length() == 3 );
  CHECK( iaq.tostring() == "Front: 5 7 3 :Back\n" );
  CHECK( iaq.front() == 5 );

  // test dequeu
  iaq.dequeue();
  CHECK_FALSE( iaq.isEmpty() );
  CHECK( iaq.length() == 2 );
  CHECK( iaq.tostring() == "Front: 7 3 :Back\n" );
  CHECK( iaq.front() == 7 );

  iaq.dequeue();
  CHECK_FALSE( iaq.isEmpty() );
  CHECK( iaq.length() == 1 );
  CHECK( iaq.tostring() == "Front: 3 :Back\n" );
  CHECK( iaq.front() == 3 );

  // test dequeue to make queue become empty again
  iaq.dequeue();
  CHECK( iaq.isEmpty() );
  CHECK( iaq.length() == 0 );
  CHECK( iaq.tostring() == "Front: :Back\n" );
  CHECK_THROWS( iaq.front(), EmptyQueueException() );

  // test clear of queue
  iaq.enqueue(1);
  iaq.enqueue(2);
  CHECK_FALSE( iaq.isEmpty() );
  CHECK( iaq.length() == 2 );
  CHECK( iaq.tostring() == "Front: 1 2 :Back\n" );

  iaq.clear();
  CHECK( iaq.isEmpty() );
  CHECK( iaq.length() == 0 );
  CHECK( iaq.tostring() == "Front: :Back\n" );

  //--------------------------------------------------------------------
  // use a linked list queue implementation of strings
  LQueue<string> saq;

  // initial queue should be empty
  CHECK( saq.isEmpty() );
  CHECK( saq.length() == 0 );
  CHECK( saq.tostring() == "Front: :Back\n" );
  CHECK_THROWS( saq.front(), EmptyQueueException() );
  CHECK_THROWS( saq.dequeue(), EmptyQueueException() );

  // add item to empty queue
  saq.enqueue("echo");
  CHECK_FALSE( saq.isEmpty() );
  CHECK( saq.length() == 1 );
  CHECK( saq.tostring() == "Front: echo :Back\n" );
  CHECK( saq.front() == "echo" );

  // add item to non empty queue
  saq.enqueue("hotel");
  CHECK_FALSE( saq.isEmpty() );
  CHECK( saq.length() == 2 );
  CHECK( saq.tostring() == "Front: echo hotel :Back\n" );
  CHECK( saq.front() == "echo" );

  // add another item to non empty queue
  saq.enqueue("bravo");
  CHECK_FALSE( saq.isEmpty() );
  CHECK( saq.length() == 3 );
  CHECK( saq.tostring() == "Front: echo hotel bravo :Back\n" );
  CHECK( saq.front() == "echo" );

  // test dequeu
  saq.dequeue();
  CHECK_FALSE( saq.isEmpty() );
  CHECK( saq.length() == 2 );
  CHECK( saq.tostring() == "Front: hotel bravo :Back\n" );
  CHECK( saq.front() == "hotel" );

  saq.dequeue();
  CHECK_FALSE( saq.isEmpty() );
  CHECK( saq.length() == 1 );
  CHECK( saq.tostring() == "Front: bravo :Back\n" );
  CHECK( saq.front() == "bravo" );

  // test dequeue to make queue become empty again
  saq.dequeue();
  CHECK( saq.isEmpty() );
  CHECK( saq.length() == 0 );
  CHECK( saq.tostring() == "Front: :Back\n" );
  CHECK_THROWS( saq.front(), EmptyQueueException() );

  // test clear of queue
  saq.enqueue("alpha");
  saq.enqueue("bravo");
  CHECK_FALSE( saq.isEmpty() );
  CHECK( saq.length() == 2 );
  CHECK( saq.tostring() == "Front: alpha bravo :Back\n" );

  saq.clear();
  CHECK( saq.isEmpty() );
  CHECK( saq.length() == 0 );
  CHECK( saq.tostring() == "Front: :Back\n" );
}


/** test PriorityQueue class using integers
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some PriorityQueue enqueue() and other functions.
   TEST_CASE("<PriorityQueue enqueue int> Test PriorityQueue using int items",
          "[class]")
   {
   PriorityQueue<int> pq;

   // priority queue is initially empty
   CHECK( pq.isEmpty() );
   CHECK( pq.length() == 0 );
   CHECK( pq.tostring() == "Front: :Back\n" );
   CHECK_THROWS( pq.front(), EmptyQueueException() );
   CHECK_THROWS( pq.dequeue(), EmptyQueueException() );

   // test case 1 insertion into empty priority queue
   pq.enqueue(5);
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 1 );
   CHECK( pq.tostring() == "Front: 5 :Back\n" );
   CHECK( pq.front() == 5 );

   // test case 2 new node is highest priority and should end up on front
   pq.enqueue(10);
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 2 );
   CHECK( pq.tostring() == "Front: 10 5 :Back\n" );
   CHECK( pq.front() == 10 );

   // Test case 3 new node ends up somewhere in middle of the queue
   pq.enqueue(7);
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 3 );
   CHECK( pq.tostring() == "Front: 10 7 5 :Back\n" );
   CHECK( pq.front() == 10 );

   // Test case 3+/4 need to correctl handle when new node ends up on the end
   pq.enqueue(3);
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 4 );
   CHECK( pq.tostring() == "Front: 10 7 5 3 :Back\n" );
   CHECK( pq.front() == 10 );

   pq.enqueue(1);
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 5 );
   CHECK( pq.tostring() == "Front: 10 7 5 3 1 :Back\n" );
   CHECK( pq.front() == 10 );

   // equal priority, we can't see if this works on ints, but when
   // items of equal priority are put on queue, the first item should be ahead
   // of the new item in the resulting queue
   pq.enqueue(5);
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 6 );
   CHECK( pq.tostring() == "Front: 10 7 5 5 3 1 :Back\n" );
   CHECK( pq.front() == 10 );

   // also test dequeue and clear still work.
   pq.dequeue();
   CHECK( pq.length() == 5 );
   CHECK( pq.front() == 7 );
   CHECK( pq.tostring() == "Front: 7 5 5 3 1 :Back\n" );

   pq.dequeue();
   pq.dequeue();
   CHECK( pq.length() == 3 );
   CHECK( pq.front() == 5 );
   CHECK( pq.tostring() == "Front: 5 3 1 :Back\n" );

   pq.clear();
   CHECK( pq.isEmpty() );
   CHECK( pq.length() == 0 );
   CHECK( pq.tostring() == "Front: :Back\n" );
   CHECK_THROWS( pq.front(), EmptyQueueException() );
   CHECK_THROWS( pq.dequeue(), EmptyQueueException() );
   }
 */



/** test PriorityQueue class using strings
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the some PriorityQueue enqueue() and other functions on string types.
   TEST_CASE("<PriorityQueue enqueue string> Test PriorityQueue using string items",
          "[class]")
   {
   PriorityQueue<string> pq;

   // priority queue is initially empty
   CHECK( pq.isEmpty() );
   CHECK( pq.length() == 0 );
   CHECK( pq.tostring() == "Front: :Back\n" );
   CHECK_THROWS( pq.front(), EmptyQueueException() );
   CHECK_THROWS( pq.dequeue(), EmptyQueueException() );

   // test case 1 insertion into empty priority queue
   pq.enqueue("mike");
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 1 );
   CHECK( pq.tostring() == "Front: mike :Back\n" );
   CHECK( pq.front() == "mike" );

   // test case 2 new node is highest priority and should end up on front
   pq.enqueue("zulu");
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 2 );
   CHECK( pq.tostring() == "Front: zulu mike :Back\n" );
   CHECK( pq.front() == "zulu" );

   // Test case 3 new node ends up somewhere in middle of the queue
   pq.enqueue("tango");
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 3 );
   CHECK( pq.tostring() == "Front: zulu tango mike :Back\n" );
   CHECK( pq.front() == "zulu" );

   // Test case 3+/4 need to correctl handle when new node ends up on the end
   pq.enqueue("hotel");
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 4 );
   CHECK( pq.tostring() == "Front: zulu tango mike hotel :Back\n" );
   CHECK( pq.front() == "zulu" );

   pq.enqueue("echo");
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 5 );
   CHECK( pq.tostring() == "Front: zulu tango mike hotel echo :Back\n" );
   CHECK( pq.front() == "zulu" );

   // equal priority, we can't see if this works on ints, but when
   // items of equal priority are put on queue, the first item should be ahead
   // of the new item in the resulting queue
   pq.enqueue("mike");
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 6 );
   CHECK( pq.tostring() == "Front: zulu tango mike mike hotel echo :Back\n" );
   CHECK( pq.front() == "zulu" );

   // also test dequeue and clear still work.
   pq.dequeue();
   CHECK( pq.length() == 5 );
   CHECK( pq.front() == "tango" );
   CHECK( pq.tostring() == "Front: tango mike mike hotel echo :Back\n" );

   pq.dequeue();
   pq.dequeue();
   CHECK( pq.length() == 3 );
   CHECK( pq.front() == "mike" );
   CHECK( pq.tostring() == "Front: mike hotel echo :Back\n" );

   pq.clear();
   CHECK( pq.isEmpty() );
   CHECK( pq.length() == 0 );
   CHECK( pq.tostring() == "Front: :Back\n" );
   CHECK_THROWS( pq.front(), EmptyQueueException() );
   CHECK_THROWS( pq.dequeue(), EmptyQueueException() );
   }
 */


/** test PriorityQueue class using Job objects
 */
/* uncomment the test cases 1 at a time.  This test case tests implementation
 * of the PriorityQueue enqueue() and other functions on Job types.
   TEST_CASE("<PriorityQueue enqueue Job> Test PriorityQueue using Job items",
          "[class]")
   {
   PriorityQueue<Job> pq;

   // priority queue is initially empty
   CHECK( pq.isEmpty() );
   CHECK( pq.length() == 0 );
   CHECK( pq.tostring() == "Front: :Back\n" );
   CHECK_THROWS( pq.front(), EmptyQueueException() );
   CHECK_THROWS( pq.dequeue(), EmptyQueueException() );

   // test case 1 insertion into empty priority queue
   // look in JobSimulator.hpp for Job constructor
   // Job(5, 0, 0) is Job(priority, serviceTime, startTime), we only use priority
   // for these tests
   pq.enqueue( Job(5, 0, 0) );
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 1 );
   CHECK( pq.tostring() == "Front: [id: 1 priority: 5] :Back\n" );
   CHECK( pq.front().tostring() == "[id: 1 priority: 5]" );

   // test case 2 new Job is highest priority and should end up on front
   pq.enqueue( Job(10, 5, 5) );
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 2 );
   CHECK( pq.tostring() == "Front: [id: 2 priority: 10] [id: 1 priority: 5] :Back\n" );
   CHECK( pq.front().tostring() == "[id: 2 priority: 10]" );

   // Test case 3 new Job ends up somewhere in middle of the queue
   pq.enqueue( Job(7, 1, 1) );
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 3 );
   CHECK( pq.tostring() == "Front: [id: 2 priority: 10] [id: 3 priority: 7] [id: 1 priority: 5] :Back\n" );
   CHECK( pq.front().tostring() == "[id: 2 priority: 10]" );

   // Test case 3+/4 need to correctl handle when new node ends up on the end
   pq.enqueue( Job(3, 4, 5) );
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 4 );
   CHECK( pq.tostring() == "Front: [id: 2 priority: 10] [id: 3 priority: 7] [id: 1 priority: 5] [id: 4 priority: 3] :Back\n" );
   CHECK( pq.front().tostring() == "[id: 2 priority: 10]" );

   pq.enqueue( Job(1, 2, 3) );
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 5 );
   CHECK( pq.tostring() == "Front: [id: 2 priority: 10] [id: 3 priority: 7] [id: 1 priority: 5] [id: 4 priority: 3] [id: 5 priority: 1] :Back\n" );
   CHECK( pq.front().tostring() == "[id: 2 priority: 10]" );

   // equal priority, we insert another Job with priority 5, it should end up on
   // the queu after the existing Job with same priority if you implement your
   // enqueue function correctly
   pq.enqueue( Job(5, 9, 9) );
   CHECK_FALSE( pq.isEmpty() );
   CHECK( pq.length() == 6 );
   CHECK( pq.tostring() == "Front: [id: 2 priority: 10] [id: 3 priority: 7] [id: 1 priority: 5] [id: 6 priority: 5] [id: 4 priority: 3] [id: 5 priority: 1] :Back\n" );
   CHECK( pq.front().tostring() == "[id: 2 priority: 10]" );
   CHECK( pq[2].getId() == 1 );
   CHECK( pq[3].getId() == 6 );
   }
 */
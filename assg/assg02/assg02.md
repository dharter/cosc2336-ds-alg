---
title: 'Assignment 02: Classes, User Defined Data Types'
author: 'COSC 2336: Data Structures and Algorithms'
date: 'Fall 2020'
---


# Objectives
- Learn how to define classes over a multi-file project, with
  class declarations in the class header file and member function
  implementations in the implementation file.
- Practice defining classes and class member functions.
- Review of writing functions and C/C++ expressions.

# Description

In this assignment you will implement your own version of a `Set` 
data type, in the mathematical sense of a set of objects.  A `Set`
is like a list of items, except all items are unique in the set, we
never have duplicates of items in the set.  We will implement a simple
`Set` data type using a C++ `class`.  To keep things relatively simple,
your set will only keep track of a set of integer values, and we will
declare a maximum number of items that can be in your set so that we can
use a statically defined array of integers to represent the items 
currently in the set (we will look at dynamic memory allocation next 
week to solve this issue of allowing sets of an arbitray size to be 
represented).


# Setup

For this assignment you will be given the following files:

| File Name                 | Description                         |
|---------------------------|-------------------------------------|
| `assg02-tests.cpp`        | Unit tests for the Set class you    |
|                           | are to implement.                   |
| `Set.hpp`                 | Header file for the declarations    |
|                           | of the `Set` class and its members. |
| `Set.cpp`                 | Implementation file for the member  |
|                           | functions of your `Set` class.      |

These files are set up as a multi-file project to compile the
two `.cpp` source files together and run them as shown for the
class.  The `Makefile` you were given should be usable to build
and test the assignment using the Visual Code editor as required
in this class.  For this assignment you have been given the 
constructor for the `Set` class.  Also the function documentation
for all of the member functions has been left in the `Set.cpp`
implementation file.  In future you will be required to write 
the documentation as well as the function, but for this assignment make
sure your function appears directly after the documentation
that documents each member function.

The general approach you should take for this assignment, and
all assignment is:

1. Open your repository folder with the given starting templates.  The files
   should compile and run, but either no tests will be run, or tests
   will run but be failing.
2. For this project, start by uncommenting the first `TEST_CASE` in
   the `assg02-tests.cpp` file.  This case performs a single check
   of your `isEmpty()` function.  Once this first test case is uncommented,
   your program will no long be able to compile because you have to 
   implement the `isEmpty()` member function.
3. Add the correct member function prototype for your `isEmpty()`
   member function to the `Set.hpp` class header file.  The
   prototype consists of the name of the function, its input
   parameters and their types (there are no input parameters 
   for `isEmpty()`), and the return value of the function, which
   should return a `bool` boolean result in this case..
4. Add a stub/empty implementation of `isEmpty()` member function to the
   `Set.cpp` implementation file.  The function should
   have the same signature as the prototype you gave in the header
   file.  The function should initially just return a result of `true`
   since this is a value returning function that returns a `bool`
   value as a result.  Make sure you place the implementation of this
   function under the function documentation meant for the `isEmpty()`
   member function, and also don't forget the `Set::` to indicate this
   function is a member of the `Set` class.
5. Your code should compile and run now.  Make sure after adding the
   function prototype and stub your code compiles and runs.  And your first
   unit test should pass since it is true that we expect the set `s`
   to be empty at this point.
6. Incrementally implement the functionality of your `Set`
   class.  You should try to add no more than 2 or 3 lines of code,
   and then make sure you program still compiles and runs.  Start by
   uncommenting the next test case and working on that function.  
   When a test is failing add code to get the first failing test to pass.  Then once that
   test passes, move on to the next failing tests until you have all
   tests passing.  If you write something that causes a previously
   passing test to fail, you should stop and figure out why, and
   either fix it so that the original test still passes, or remove
   what you did and try a new approach.

# Tasks
You should set up your project/code as described in the previous
section.  In this section we give some more details on implementing
the class member functions you need for your `Set` class:

1. Start with your stub implementation of the `isEmpty()` member function
   that always returns `true`.  This should allow the first unit test to
   pass.
2. The next unit test tests the `getSetSize()` accessor method.  Again use
   a stub implementation that just returns 0 to get this test to pass.
3. Uncomment the third test case and write a stub implementation of the
   `containsItem()` member function.  If this function just returns 
   `false` it will be able to pass all of the tests in the third test
   case.
4. Uncomment the next text case of the `str()` member function.  Write 
   a stub function that simply returns the string "[ ]" to get this
   test to compile and run.
5. Now the real work begins.  If you followed directions to this point, you
   have functions named `isEmpty()`, `getSetSize()`, `containsItem()`
   and `str()` that  return default values. Now we will implement
   a function that will need you to do some real work to implement it
   and the previous stubbed out functions.  Uncomment the next test
   case that test the `addItem()` member function.  This is a `void`
   function, it does not return a result.  Write a stub function that
   does nothing, and make sure your code still compiles.  If your code compiles
   it should still be passing the previous tests, but it will fail now on
   the first test of `isEmpty()` since we no longer expect the
   set to be empty.  You need to add code into `addItem()` first.
   For example, you could simply increment the `setSize` member variable
   by 1 each time an item is added to the set.  This would allow you to
   correctly implement the `isEmpty()` function now because you can
   return `true` from `isEmpty()` when the `setSize` is 0, and you 
   can return `false` when the `setSize` is a non zero value.  Do this
   to get the first test in this case of your `isEmpty()` to correctly
   pass.
6. You should now be able to correctly implement your `getSetSize()` method,
   because you can return the `setSize` value instead of a hardcoded 0, which
   should allow the next test to pass.
7. But to get `containsItem()` to work we need to actually remember the items
   that are currently in the set.  So you should start by saving the
   item in your `addItem()` method to index 0 of the `setItem` member array.
   You can think ahead about how to correctly add the next item to index 1
   of the array, e.g. you could use the `setSize` variable which will allow
   you to know which index should be used to store new items that are added.
   Once you store the item in the array somehow, work on your
   `containsItem()` function.  Your implementation should simply search through
   your `setItem` array member variable, and return true if you find the
   item is currently in your set, and false if not.  Implement your 
   `containsItem()` so that it passes the unit test here.  It should also
   still correctly pass the next test that if we ask if the set contains
   the item 0, which it does not, the answer is `false`.
8. Implement the `str()` member function.  You should use a `ostringstream`
   string stream object to construct a string stream as expected to
   represent the items currently in the set.  Watch the lecture videos from
   this week where we cover using string streams if you have not used them
   before.
9. At this point you should be passing the tests in the first section
   for the `addItem()` member function.  Check that all of the tests
   in the next section pass as well after adding 3 more items, and fix any
   issues/bugs if these tests are not passing before moving on.
10. Uncomment the next test case.  This test case is still testing the
    `addItem()` member function.  But here we test that if you add 
    a duplicate item to the set, nothing happens and only 1 unique value of
    each integer is kept and represented in the set.  You should implement
    this behavior using the following algorithm.  You should reuse your
    hopefully now working `containsItem()` member function.  So before
    you actually add the item into your `setItem` array, first check whether
    or not the seat already contains the item by calling `containsItem()`.
    If you already have the item, you can just return immediately and do
    nothing.  Only if the item is not already in the set should you then proceed
    to add it to the end of the array of your items in the set.  
    If you implement this correctly, all of the tests of adding duplicate
    items should now be passing in this test case.
11. Uncomment the next test case to test the `removeItem()` member functions.
    As usual first create a stub function that does nothing, and make sure
    your code still compiles and still passes all of the unit tests up to
    this point.  The `removeItem()` should simply silently do nothing if
    asked to remove an item that is not in the set.  You can do this again by
    first checking using your `containsItem()` member function, and just
    returning if the set doesn't contain the item you were asked to remove.
    If the item is in the set, you need to search for and remove the
    item from your array of set items.  You need to do this using the
    following algorithm.  First find the location of the item in the array.
    Then you should shift down all of the items at higher indexes by 1
    value.  This effectively removes the item, and ensures there are no
    holes left in your array of set items.  Make sure you correctly update
    your `setSize` count by decreasing it by 1.  The tests in this unit test
    check that you correctly handle cases like when you try and remove the
    last item of the set of items, or when you try to remove the
    item at index 0 in the array, etc.  If you correctly shift the items
    and update the `setSize` you should be able to pass all of the unit 
    tests of `removeItem()` here.
12. Finally you will implement operations to perform union and intersection
    on your set.  A test fixture is used in our unit test file at this point.
    Uncomment the test fixture first.  This test fixture creates 4 or 5 
    sets that are used to test your union and intersection operators.
13. Uncomment the first test case below the fixture class to test your
   `operatorUnion()` member function.  As usual you should create a stub function
   and make sure you code compiles, runs and passes all of the tests you
   had been passing previously.  The union operation will take a `otherSet`
   as a parameter and the result will be the union of the two sets ends
   up in the set you call the `operatorUnion()` member function on.
   The algorithm for the `operatorUnion()` is relatively simple.  You
   should iterate over all of the items in the `otherSet` and call your
   own `addItem()` to add each item of the `otherSet`.  If your `addItem()`
   is implemented correctly, it will do nothing if the item is already
   in the set, but it will add the item that it doesn't have from the
   other set if it is missing.  The result of doing this is the union
   of the two sets.  You should be able to pass all of the tests in the
   first unit test with this algorithm.  Also if you have done things
   correctly in your `operatorUnion()` function, you can uncomment all
   of the next 3 unit tests that test the union operator on empty sets
   and other edge conditions.  Hopefully if your implementation is
   complete and correct, all of these will be passing.  If not you will
   need to debug your algorithm and fix your union operator.
14. Finally you can uncomment the first test case for the
    `operatorIntersect()` member function.  Again start by creating a stub
    function and making sure your code still compiles, runs and is still passing
    all of the tests up to this point.  The intersection operation is a bit
    more tricky than the union operation.  To implement intersection you
    will have to remove any item in the set that is not also in the other
    set.  There are many different ways you could accomplish this.  My
    recommendation is that you implement this in the following way.
    Iterate backwards through your items in your `setItem` array, e.g. start
    with the last item and work backwards to item at index 0.  For each item
    test if it is in the `otherSet` by calling the `containsItem()` member
    function of the other set.  If the item is not also in the other set,
    you need to remove it from your set.  Do this by calling your own
    `removeItem()` member function to remove the item.  The reason why you
    work backwards through your items is because, if you do so, you will
    not get messed by by your `removeItem()` implementation shifting items
    in the array.  Only items you have already checked will get shifted down.
    Once you can pass the tests in the first unit test of the
    `operatorIntersect()` uncomment the next 3 tests and make sure you 
    are passing all of them as well.  If any tests are failing,  you will
    need to debug your `operatorIntersect()` implementation to get them
    to pass.





# Example Output
Here is the correct output you get from your program
if you correctly implement the two functions and successfully
pass all of the unit tests given for this assignment.  If you
invoke your function with no command line arguments, only failing
tests are usually shown by default.  In the second example, we use the
`-s` command line option to have the unit test framework show both
successful and failing tests, and thus we get reports of all of the
successfully passing tests as well on the output.

```
$ ./test
===============================================================================
All tests passed (195 assertions in 17 test cases)

$ ./test -s

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
test is a Catch v2.12.2 host application.
Run with -? for options

-------------------------------------------------------------------------------
<Set> default constructor and isEmpty
-------------------------------------------------------------------------------
assg02-tests.cpp:34
...............................................................................

assg02-tests.cpp:36: PASSED:
  CHECK( s.isEmpty() )
with expansion:
  true

-------------------------------------------------------------------------------
<Set> default constructor and get setSize accessor
-------------------------------------------------------------------------------
assg02-tests.cpp:44
...............................................................................

assg02-tests.cpp:46: PASSED:
  CHECK( s.getSetSize() == 0 )
with expansion:
  0 == 0

... snip output removed for length ...

-------------------------------------------------------------------------------
<Set> operatorIntersect() test intersection of end values
  test again but with ends which also tests intersection doesn't change when
  set is a subset of the other set
-------------------------------------------------------------------------------
assg02-tests.cpp:621
...............................................................................

assg02-tests.cpp:625: PASSED:
  CHECK_FALSE( s2ends.isEmpty() )
with expansion:
  !false

assg02-tests.cpp:626: PASSED:
  CHECK( s2ends.getSetSize() == 2 )
with expansion:
  2 == 2

assg02-tests.cpp:628: PASSED:
  CHECK( s2ends.containsItem(8) )
with expansion:
  true

assg02-tests.cpp:629: PASSED:
  CHECK_FALSE( s2ends.containsItem(4) )
with expansion:
  !false

assg02-tests.cpp:630: PASSED:
  CHECK_FALSE( s2ends.containsItem(-6) )
with expansion:
  !false

assg02-tests.cpp:631: PASSED:
  CHECK( s2ends.containsItem(18) )
with expansion:
  true

assg02-tests.cpp:633: PASSED:
  CHECK( s2ends.str() == "[ 8 18 ]" )
with expansion:
  "[ 8 18 ]" == "[ 8 18 ]"

===============================================================================
All tests passed (195 assertions in 17 test cases)

```



# Assignment Submission

A MyLeoOnline submission folder has been created for this assignment.
There is a target named `submit` that will create a tared and gziped
file named `assg0w.tar.gz`. You should do a `make submit` when finished
and upload your resulting gzip file to the MyLeoOnline Submission
folder for this assignment.

```
$ make submit
uncrustify -c ../../config/uncrustify.cfg --replace --no-backup assg02-tests.cpp assg02-main.cpp Set.hpp Set.cpp
Parsing: assg02-tests.cpp as language CPP
Parsing: assg02-main.cpp as language CPP
Parsing: Set.hpp as language CPP
Parsing: Set.cpp as language CPP
tar cvfz assg02.tar.gz assg02-tests.cpp assg02-main.cpp Set.hpp Set.cpp
assg02-tests.cpp
assg02-main.cpp
Set.hpp
Set.cpp
```

# Requirements and Grading Rubrics

## Program Execution, Output and Functional Requirements

1. 50 pts. Your program must compile, run and produce some sort of output to
   be graded.  0 if not satisfied.
2. 5 pts. `isEmpty()` member function implementation works and passes
   all unit tests.
3. 5 pts. `getSetSize()` member function implementation works and passes
   all unit tests.
4. 5 pts. `containsItem()` member function implementation works and passes
   all unit tests.
5. 5 pts. `str()` member function implementation works and passes all unit
   tests.
6. 5 pts. `addItem()` member function implementation works and passes all 
   unit tests.
7. 5 pts. `removeItem()` member function implementation works and passes all
   unit tests.
8. 10 pts. `operatorUnion()` member function implementation works and passes
   all unit tests.
9. 10 pts. `operatorIntersect()` member function implementation works and
   passes all unit tests. 


## Program Style

Your programs must conform to the style and formatting guidelines
given for this class.  The following is a list of the guidelines that
are required for the assignment to be submitted this week.

1. Most importantly, make sure you figure out how to set your
   indentation settings correctly.  All programs must use 2 spaces for
   all indentation levels, and all indentation levels must be
   correctly indented.  Also all tabs must be removed from files, and
   only 2 spaces used for indentation.
1. A function header must be present for all functions you define.
   You must give a short description of the function, and document
   all of the input parameters to the function, as well as the return
   value and data type of the function if it returns a value.
1. Do not include any statements (such as `system("pause")` or
   inputting a key from the user to continue) that are meant to keep
   the terminal from going away.  Do not include any code that is
   specific to a single operating system, such as the
   `system("pause")` which is Microsoft Windows specific.

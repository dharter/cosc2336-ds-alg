/** @file assg02-tests.cpp
 * @brief Unit tests for Assignment 02, practice on classes and user
 *   defined types.
 *
 * @author Jane Programmer
 * @note   cwid : 123 45 678
 * @note   class: COSC 2336, Fall 2020
 * @note   ide  : VS Code 1.48 / build package / GNU gcc tools
 * @note   assg : Assignment 02
 * @date   June 1, 2020
 *
 * Assignment 02, classes and user defined data types.  This is
 * the file containing the unit tests for this assignment.  It uses the catch2
 * unit test framework.  Tests are given (commented out) for the member functions
 * you need to implement for the two classes for this assignment.
 */
#include <cmath>
#include <iostream>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Set.hpp"
using namespace std;


/// @brief s is an initially empty set that we will perform some basic tests of.
Set s;


/** Set default constructor, needs implement of isEmpty()
 * to test.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Set> default constructor and isEmpty", "[class]")
   {
   CHECK(s.isEmpty());
   }
 */

/** Set getSetSize accessor method
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Set> default constructor and get setSize accessor", "[class]")
   {
   CHECK(s.getSetSize() == 0);
   }
 */

/** Set containsItem accessor method.  The set is still empty,
 * So any item we check should give a false result as it should
 * not be in the set yet.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Set> default constructor and contains item", "[class]")
   {
   CHECK_FALSE(s.containsItem(5));
   CHECK_FALSE(s.containsItem(3));
   CHECK_FALSE(s.containsItem(9));
   CHECK_FALSE(s.containsItem(0));
   }
 */

/** Set str method.  The set is still empty,
 * So we should get an empty string as a result
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Set> default constructor and str representation", "[class]")
   {
   CHECK(s.str() == "[ ]");
   }
 */

/** addItem tests
 * Our first real test of the set.  Lets add some items and
 * see if the set is correctly being maintained.  Adding a new item
 * will cause all of the methods we have tested to now give new
 * results, so we will be testing all of them again as well here.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Set> addItem() tests", "[class]")
   {
   SECTION("add our first item to the set")
   {
    s.addItem(5);
    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 1);
    CHECK(s.containsItem(5));
    CHECK_FALSE(s.containsItem(0));
    CHECK(s.str() == "[ 5 ]");
   }

   SECTION("add 3 more items to the set")
   {
    s.addItem(9);
    s.addItem(-1);
    s.addItem(42);
    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 4);

    CHECK(s.containsItem(5)); // still got it, right?
    CHECK(s.containsItem(9));
    CHECK(s.containsItem(-1));
    CHECK(s.containsItem(42));
    CHECK_FALSE(s.containsItem(0));
    CHECK_FALSE(s.containsItem(15));

    CHECK(s.str() == "[ 5 9 -1 42 ]"); // we are expecting an unsorted set of items here
   }
   }
 */

/** addItem duplicate items
 * This is a set, so adding existing items should not cause the set to
 * grow.  We test adding a duplicate of the first, last and a middle
 * value of the set.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Set> addItem() duplicate items", "[class]")
   {
   SECTION("test adding duplicate that is at index 0 of the list")
   {
    s.addItem(5);

    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 4);

    CHECK(s.containsItem(5));
    CHECK(s.containsItem(9));
    CHECK(s.containsItem(-1));
    CHECK(s.containsItem(42));

    CHECK(s.str() == "[ 5 9 -1 42 ]");
   }

   SECTION("test adding duplicate of the last item in the set/list")
   {
    s.addItem(42);

    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 4);

    CHECK(s.containsItem(5));
    CHECK(s.containsItem(9));
    CHECK(s.containsItem(-1));
    CHECK(s.containsItem(42));

    CHECK(s.str() == "[ 5 9 -1 42 ]");
   }

   SECTION("test adding duplicate of item somewhere inside of the list")
   {
    s.addItem(-1);

    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 4);

    CHECK(s.containsItem(5));
    CHECK(s.containsItem(9));
    CHECK(s.containsItem(-1));
    CHECK(s.containsItem(42));

    CHECK(s.str() == "[ 5 9 -1 42 ]");
   }
   }
 */

/** removeItems tests
 * Test that removeItems is working.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE("<Set> removeItem() tests", "[class]")
   {
   SECTION("first test that request to remove nonexistent item does nothing")
   {
    s.removeItem(-5);

    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 4);

    CHECK(s.containsItem(5));
    CHECK(s.containsItem(9));
    CHECK(s.containsItem(-1));
    CHECK(s.containsItem(42));

    CHECK(s.str() == "[ 5 9 -1 42 ]");
   }

   SECTION("start with easiest case, remove middle item")
   {
    s.removeItem(9);

    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 3);

    CHECK(s.containsItem(5));
    CHECK_FALSE(s.containsItem(9));
    CHECK(s.containsItem(-1));
    CHECK(s.containsItem(42));

    CHECK(s.str() == "[ 5 -1 42 ]");
   }

   SECTION("test that first item removal handled correctly")
   {
    s.removeItem(5);

    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 2);

    CHECK_FALSE(s.containsItem(5));
    CHECK_FALSE(s.containsItem(9));
    CHECK(s.containsItem(-1));
    CHECK(s.containsItem(42));

    CHECK(s.str() == "[ -1 42 ]");
   }

   SECTION("test that last item removal handled correctly")
   {
    s.removeItem(42);

    CHECK_FALSE(s.isEmpty());
    CHECK(s.getSetSize() == 1);

    CHECK_FALSE(s.containsItem(5));
    CHECK_FALSE(s.containsItem(9));
    CHECK(s.containsItem(-1));
    CHECK_FALSE(s.containsItem(42));

    CHECK(s.str() == "[ -1 ]");
   }

   SECTION("final boundary case, set of 1 item becomes empty set when item is removed")
   {
    s.removeItem(-1);

    CHECK(s.isEmpty());
    CHECK(s.getSetSize() == 0);

    CHECK_FALSE(s.containsItem(5));
    CHECK_FALSE(s.containsItem(9));
    CHECK_FALSE(s.containsItem(-1));
    CHECK_FALSE(s.containsItem(42));

    CHECK(s.str() == "[ ]");
   }
   }
 */


/** A test fixture is a common Unit Test concept.  Basically catch2 will
 * create a new instance of this class for each TEST_CASE_METHOD where we
 * use this fixture.  We create instances of sets we can use to test
 * intersection and union operations in this fixture.
 */
/*
   struct SetTestFixture
   {
   public:
   /// @brief set 1
   Set s1;
   /// @brief another set the same as s1
   Set sameAsS1;
   /// @brief set 2
   Set s2;
   /// @brief set 2 ends, the first and last item of set 2
   Set s2ends;
   /// @brief an empty set
   Set empty;

   // constructor to initialize the sets with some values
   SetTestFixture()
   {
    s1.addItem(5);
    s1.addItem(8);
    s1.addItem(-6);
    s1.addItem(12);

    sameAsS1.addItem(5);
    sameAsS1.addItem(8);
    sameAsS1.addItem(-6);
    sameAsS1.addItem(12);

    s2.addItem(8);
    s2.addItem(4);
    s2.addItem(-6);
    s2.addItem(18);

    s2ends.addItem(8);
    s2ends.addItem(18);
   }


   };
 */


/** operatorUnion tests
 * Test that the union operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorUnion() basic test", "[class]")
   {
   SECTION("first test basic union operation on 2 sets with more than 1 item in union")
   {
    s1.operatorUnion(s2);

    CHECK_FALSE(s1.isEmpty());
    CHECK(s1.getSetSize() == 6);

    CHECK(s1.containsItem(5));
    CHECK(s1.containsItem(8));
    CHECK(s1.containsItem(-6));
    CHECK(s1.containsItem(12));
    CHECK(s1.containsItem(4));
    CHECK(s1.containsItem(18));

    CHECK(s1.str() == "[ 5 8 -6 12 4 18 ]");

    // also make sure s2 wasn't changed
    CHECK(s2.getSetSize() == 4);
    CHECK(s2.str() == "[ 8 4 -6 18 ]");
   }
   }
 */


/** operatorUnion tests
 * Test that the union operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorUnion() test the other way", "[class]")
   {
   SECTION("should get the same result if we use same 2 sets but have s2 perform the union on s1")
   {
    s2.operatorUnion(s1);

    CHECK_FALSE(s2.isEmpty());
    CHECK(s2.getSetSize() == 6);

    CHECK(s2.containsItem(5));
    CHECK(s2.containsItem(8));
    CHECK(s2.containsItem(-6));
    CHECK(s2.containsItem(12));
    CHECK(s2.containsItem(4));
    CHECK(s2.containsItem(18));

    CHECK(s2.str() == "[ 8 4 -6 18 5 12 ]");

    // also make sure s2 wasn't changed
    CHECK(s1.getSetSize() == 4);
    CHECK(s1.str() == "[ 5 8 -6 12 ]");
   }
   }
 */


/** operatorUnion tests
 * Test that the union operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorUnion() test union empty set", "[class]")
   {
   SECTION("The union of a set with an empty set is the original set")
   {
    s1.operatorUnion(empty);
    CHECK_FALSE(s1.isEmpty());
    CHECK(s1.getSetSize() == 4);
    CHECK(s1.containsItem(5));
    CHECK(s1.containsItem(8));
    CHECK(s1.containsItem(-6));
    CHECK(s1.containsItem(12));
    CHECK(s1.str() == "[ 5 8 -6 12 ]");

    s2.operatorUnion(empty);
    CHECK_FALSE(s2.isEmpty());
    CHECK(s2.getSetSize() == 4);
    CHECK(s2.containsItem(8));
    CHECK(s2.containsItem(4));
    CHECK(s2.containsItem(-6));
    CHECK(s2.containsItem(18));
    CHECK(s2.str() == "[ 8 4 -6 18 ]");

    // empty set with empty set is still empty
    empty.operatorUnion(empty);
    CHECK(empty.isEmpty());
    CHECK(empty.getSetSize() == 0);
   }
   }
 */


/** operatorUnion tests
 * Test that the union operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorUnion test union with same set", "[class]")
   {
   SECTION("The union of two equal sets is still the original set")
   {
    s1.operatorUnion(sameAsS1);

    CHECK_FALSE(s1.isEmpty());
    CHECK(s1.getSetSize() == 4);

    CHECK(s1.containsItem(5));
    CHECK(s1.containsItem(8));
    CHECK(s1.containsItem(-6));
    CHECK(s1.containsItem(12));

    CHECK(s1.str() == "[ 5 8 -6 12 ]");

    // we should be able to do sets and unions with ourself and in both cases
    // there will be no change
    sameAsS1.operatorUnion(sameAsS1);

    CHECK_FALSE(sameAsS1.isEmpty());
    CHECK(sameAsS1.getSetSize() == 4);

    CHECK(sameAsS1.containsItem(5));
    CHECK(sameAsS1.containsItem(8));
    CHECK(sameAsS1.containsItem(-6));
    CHECK(sameAsS1.containsItem(12));

    CHECK(sameAsS1.str() == "[ 5 8 -6 12 ]");
   }
   }
 */


/** operatorUnion tests
 * Test that the union operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorUnion() test union of end values", "[class]")
   {
   SECTION("Look out for problems with index 0 and last index still on the sets, union of only first and last item of set")
   {
    s2.operatorUnion(s2ends);

    CHECK_FALSE(s2.isEmpty());
    CHECK(s2.getSetSize() == 4);

    CHECK(s2.containsItem(8));
    CHECK(s2.containsItem(4));
    CHECK(s2.containsItem(-6));
    CHECK(s2.containsItem(18));

    CHECK(s2.str() == "[ 8 4 -6 18 ]");
   }

   SECTION("test again but with ends which also tests union doesn't change when set is a subset of the other set")
   {
    s2ends.operatorUnion(s2);

    CHECK_FALSE(s2ends.isEmpty());
    CHECK(s2ends.getSetSize() == 4);

    CHECK(s2ends.containsItem(8));
    CHECK(s2ends.containsItem(4));
    CHECK(s2ends.containsItem(-6));
    CHECK(s2ends.containsItem(18));

    CHECK(s2ends.str() == "[ 8 18 4 -6 ]");
   }
   }
 */


/** operatorIntersect tests
 * Test that the intersection operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorIntersect() basic test", "[class]")
   {
   SECTION("first test basic intersection operation on 2 sets with more than 1 item in intersection")
   {
    s1.operatorIntersect(s2);

    CHECK_FALSE(s1.isEmpty());
    CHECK(s1.getSetSize() == 2);

    CHECK_FALSE(s1.containsItem(5));
    CHECK(s1.containsItem(8));
    CHECK(s1.containsItem(-6));
    CHECK_FALSE(s1.containsItem(12));

    CHECK(s1.str() == "[ 8 -6 ]");

    // also make sure s2 wasn't changed
    CHECK(s2.getSetSize() == 4);
    CHECK(s2.str() == "[ 8 4 -6 18 ]");
   }
   }
 */


/** operatorIntersect tests
 * Test that the intersection operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorIntersect() test the other way", "[class]")
   {
   SECTION("should get the same result if we use same 2 sets but have s2 perform the intersection")
   {
    s2.operatorIntersect(s1);

    CHECK_FALSE(s2.isEmpty());
    CHECK(s2.getSetSize() == 2);

    CHECK(s2.containsItem(8));
    CHECK_FALSE(s2.containsItem(4));
    CHECK(s2.containsItem(-6));
    CHECK_FALSE(s2.containsItem(18));

    CHECK(s2.str() == "[ 8 -6 ]");

    // also make sure s1 wasn't changed
    CHECK(s1.getSetSize() == 4);
    CHECK(s1.str() == "[ 5 8 -6 12 ]");
   }
   }
 */


/** operatorIntersect tests
 * Test that the intersection operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorIntersect() test intersection empty set", "[class]")
   {
   SECTION("The intersection of a set with an empty set is always the empty set")
   {
    s1.operatorIntersect(empty);
    CHECK(s1.isEmpty());
    CHECK(s1.getSetSize() == 0);

    s2.operatorIntersect(empty);
    CHECK(s2.isEmpty());
    CHECK(s2.getSetSize() == 0);

    // empty set with empty set is still empty
    empty.operatorIntersect(empty);
    CHECK(empty.isEmpty());
    CHECK(empty.getSetSize() == 0);
   }
   }
 */


/** operatorIntersect tests
 * Test that the intersection operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorIntersect() test intersection with same set", "[class]")
   {
   SECTION("The intersection of two equal sets is still the original set")
   {
    s1.operatorIntersect(sameAsS1);

    CHECK_FALSE(s1.isEmpty());
    CHECK(s1.getSetSize() == 4);

    CHECK(s1.containsItem(5));
    CHECK(s1.containsItem(8));
    CHECK(s1.containsItem(-6));
    CHECK(s1.containsItem(12));

    CHECK(s1.str() == "[ 5 8 -6 12 ]");

    // we should be able to do sets and unions with ourself and in both cases
    // there will be no change
    sameAsS1.operatorIntersect(sameAsS1);

    CHECK_FALSE(sameAsS1.isEmpty());
    CHECK(sameAsS1.getSetSize() == 4);

    CHECK(sameAsS1.containsItem(5));
    CHECK(sameAsS1.containsItem(8));
    CHECK(sameAsS1.containsItem(-6));
    CHECK(sameAsS1.containsItem(12));

    CHECK(sameAsS1.str() == "[ 5 8 -6 12 ]");
   }
   }
 */


/** operatorIntersect tests
 * Test that the intersection operation on sets works correctly.
 * Uncomment the following test case block and write your code
 * to pass these tests for these parts of the assignment.
 */
/*
   TEST_CASE_METHOD(SetTestFixture,
                 "<Set> operatorIntersect() test intersection of end values", "[class]")
   {
   SECTION("Look out for problems with index 0 and last index still on the sets, intersection of only first and last item of set")
   {
    s2.operatorIntersect(s2ends);

    CHECK_FALSE(s2.isEmpty());
    CHECK(s2.getSetSize() == 2);

    CHECK(s2.containsItem(8));
    CHECK_FALSE(s2.containsItem(4));
    CHECK_FALSE(s2.containsItem(-6));
    CHECK(s2.containsItem(18));

    CHECK(s2.str() == "[ 8 18 ]");
   }

   SECTION("test again but with ends which also tests intersection doesn't change when set is a subset of the other set")
   {
    s2ends.operatorIntersect(s2);

    CHECK_FALSE(s2ends.isEmpty());
    CHECK(s2ends.getSetSize() == 2);

    CHECK(s2ends.containsItem(8));
    CHECK_FALSE(s2ends.containsItem(4));
    CHECK_FALSE(s2ends.containsItem(-6));
    CHECK(s2ends.containsItem(18));

    CHECK(s2ends.str() == "[ 8 18 ]");
   }
   }
 */